package comicviewer.display.settings;

import comicviewer.display.items.ImageItem;
import comicviewer.display.items.TextItem;
import comicviewer.display.Page.PageOptions;
import comicviewer.components.button.Radio;
import comicviewer.utils.parser.Types;
import comicviewer.utils.Utils.defNull;
import libgoa.Artean;
import comicviewer.display.settings.Tab.TabId;
import h3d.Vector;
import h3d.Vector4;

/**
 * ...
 * @author josu
 */
class Settings extends Page {
	var currentTab(default, set):TabId;
	var panels:Map<TabId, SettingsPanel>;
	var index:Index;
	var glossary:Glossary;
	var general:General;
	var bgItem:ImageItem;

	static inline var BG_LAYER:Int = 1;
	static inline var SETTINGS_LAYER:Int = 2;
	static inline var TAB_LAYER:Int = 3;

	public function new(?opt:PageOptions, hasGlossary:Bool = true) {
		defNull(opt.defaultFontName, CV.GENERAL_VALUES.uiFontName);

		defNull(opt.pos, new Vector(0, CV.s2d.height));
		defNull(opt.thumbTile, null);

		super(opt);
		layer = CV.GENERAL_VALUES.settingsLayer;

		var w = hxd.Window.getInstance();
		bgItem = cast addItem(new ImageItem({
			id: "settingsBg",
			color: Vector4.fromColor(0xccffffff),
			initPos: new Vector(),
			absoluteSize: new Vector(w.width, w.height),
			layer: BG_LAYER,
			parent: this
		}));

		panels = new Map();

		index = new Index(this, SETTINGS_LAYER);
		addPanel(index);
		if (hasGlossary) {
			glossary = new Glossary(this, SETTINGS_LAYER);
			addPanel(glossary);
		}
		general = new General(this, SETTINGS_LAYER);
		addPanel(general);

		CV.signals.itemClicked.handle(item -> show(null, item.glossaryLink));
	}

	public function addPanel(panel:SettingsPanel) {
		var tab = new Tab({
			id: panel.id,
			layer: TAB_LAYER,
			parent: this,
			color: Vector4.fromColor(0xFFBBBBBB)
		});
		tab.onSelected = onTabSelected;
		addItem(tab);
		panel.tab = tab;
		panels.set(panel.id, panel);
		resizeTabs();
	}

	public function resizeTabs() {
		var w = CV.s2d.width;
		var h = CV.s2d.height;
		var tabW = w / Lambda.count(panels);
		var tabX = 0.;
		var tabY = h - Tab.HEIGHT;
		var s = new h3d.Vector(tabW, Tab.HEIGHT);

		var tab;
		for (p in panels) {
			tab = p.tab;
			tab.setPosition(tabX, tabY);
			tab.size = s;
			tabX += tabW;
		}
	}

	public function addGlossaryItem(gitem:GlossaryItem) {
		if (glossary != null) {
			glossary.addItem(gitem);
		}
	}

	public function addLanguage(code:String, name:String) {
		general.addLanguage(code, name);
	}

	public function show(_tab:TabId = null, _glossaryLink:String = null) {
		if (_glossaryLink != null && glossary == null) {
			return;
		}

		load();
		start();

		// @:privateAccess bgItem.bitmap.tile.scaleToSize(hxd.Window.getInstance().width, hxd.Window.getInstance().height);
		// @:privateAccess {
		// 	trace(bgItem.bitmap.tile.width + ' / ' + bgItem.bitmap.tile.height);
		// }

		#if !comtor
		CV.NAV_BUTTONS.settings(true);
		#end

		var startY = y;

		Artean.simple(.2, startY, 0, (currY) -> {
			y = currY;
			if (y == 0)
				CV.IS_SETTINGS_SHOWN = true;
		});

		if (glossary != null && _glossaryLink != null) {
			onTabSelected(glossary.tab, true);
			glossary.showWithLink(_glossaryLink);
		} else {
			onTabSelected(index.tab, true);
			index.tab.forceSelected(true);
		}
	}

	public function visibilizeGlossaryLinks(links:Array<String>) {
		if (glossary == null || links == null || links.length == 0) {
			return;
		}

		glossary.visibilizeLinks(links);
	}

	public function hide() {
		#if !comtor
		CV.NAV_BUTTONS.settings(false);
		#end

		var startY = y;
		var newY = CV.s2d.height;
		Artean.simple(.2, startY, newY, (currY) -> {
			y = currY;
			if (y == newY) {
				stop();
				unload();
				currentTab = null;
				CV.IS_SETTINGS_SHOWN = false;
			}
		});
	}

	function onTabSelected(v:Tab, force = false) {
		if (!CV.IS_SETTINGS_SHOWN && !force)
			return;

		currentTab = v.tabId;
	}

	override public function update(dt:Float) {
		if (index != null)
			index.update(dt);
		if (glossary != null)
			glossary.update(dt);
		if (general != null)
			general.update(dt);
	}

	// GETTER & SETTER
	function set_currentTab(_t:TabId):TabId {
		if (currentTab != null) {
			var p = panels.get(currentTab);
			if (p != null)
				p.hide();
		}

		if (_t != null) {
			var p = panels.get(_t);
			if (p != null)
				p.show();
		}

		return currentTab = _t;
	}
}
