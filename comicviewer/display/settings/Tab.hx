package comicviewer.display.settings;

import comicviewer.components.button.Radio;
import comicviewer.display.items.TextItem;
import comicviewer.display.items.TextItem.TextItemOptions;
import h3d.Vector4;

/**
 * ...
 * @author josu
 */
abstract TabId(String) from String to String {}

class Tab extends TextItem {
	public var tabId:TabId;

	static public var HEIGHT:Float;

	var radioComp:Radio;

	public var onSelected:Tab->?Bool->Void;

	public function new(opt:TextItemOptions) {
		opt.hasBg = true;
		opt.layer = 5;
		opt.align = h2d.Text.Align.Center;
		opt.alignVertical = Top;
		opt.fontName = CV.GENERAL_VALUES.uiFontName;
		opt.fontSize = CV.GENERAL_VALUES.uiFontSize;
		super(opt);

		tabId = id;
		name = '${tabId}_tab';
		HEIGHT = CV.s2d.height * .15;

		radioComp = new Radio('tabs', Vector4.fromColor(0xFF151515), Vector4.fromColor(0xFF00ccff));
		radioComp.onSelectionChanged = onSelectionChanged;
		radioComp.amount = .1;
		addComponent(radioComp);

		bgColor = radioComp.unselectedColor;
	}

	function onSelectionChanged(isSelected:Bool) {
		forceSelected(isSelected);
		if (onSelected != null && isSelected)
			onSelected(this);
	}

	public function forceSelected(isSelected:Bool) {
		bgColor = (isSelected) ? radioComp.selectedColor : radioComp.unselectedColor;
		updateBg();
	}

	override function updateBg() {
		if (bgColor == null || text == null)
			return;

		super.updateBg();

		if (background == null) {
			background = new h2d.Graphics(this);
			addChild(text);
		}

		background.clear();
		background.beginFill(bgColor.toColor(), bgColor.a);
		background.drawRect(0, 0, size.x, size.y);
		background.endFill();
	}

	override function getFont() {
		return comicviewer.utils.FontPool.get(fontName, Std.int(fontSize * CV.s2d.width));
	}

	override public function updateLocalization() {
		if (!inited)
			return;

		super.updateLocalization();
		if (text != null)
			text.x = 0;
	}

	override public function set_size(s:h3d.Vector) {
		super.set_size(s);
		updateBg();
		return s;
	}
}
