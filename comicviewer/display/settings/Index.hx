package comicviewer.display.settings;

import comicviewer.CV;
import comicviewer.display.ui.Scroll;
import comicviewer.display.items.ImageItem;
import comicviewer.utils.Defaults;
import comicviewer.utils.FontPool;
import libgoa.Artean;

#if comtor
@:publicFields
#end
class Index extends SettingsPanel {
	var thumbs:Array<h2d.Object>;
	var indexScroll:comicviewer.display.ui.Scroll;

	public function new(parent:h2d.Layers = null, layer:Int = 0) {
		super('index', parent, layer);
	}

	function loadThumbs() {
		indexScroll = new comicviewer.display.ui.Scroll();
		indexScroll.scrollHeight = CV.s2d.height * .85;
		indexScroll.horizontalSpacing = 10;
		indexScroll.layout = Horizontal;
		indexScroll.padding = 10;
		indexScroll.multiline = true;
		indexScroll.maxWidth = CV.s2d.width - 20;
		indexScroll.onChildClicked = function(c) {
			var t = Std.instance(c, h2d.Object);
			trace('Clicked $t in indexScroll');
			if (t != null) {
				var pid = StringTools.replace(t.name, '_thumb', '');
				if (pid != null) {
					CV.instance.gotoPage(Std.parseInt(pid));
					@:privateAccess CV.instance.settings.hide();
				}
			}
		}
		thumbs = [];

		var pages = CV.instance.pages;
		var numberFont = FontPool.get(CV.GENERAL_VALUES.uiFontName, Std.int(CV.GENERAL_VALUES.uiFontSize * CV.GENERAL_VALUES.pageWidth));
		var th:h2d.Object, numberText, numberColor = new h3d.Vector4();
		var numberBgTile = h2d.Tile.fromColor(0xFFFFFF, 10, 10, .8);
		for (p in pages) {
			if (p.thumbTile != null) {
				th = new h2d.Object();
				new h2d.Bitmap(p.thumbTile, th);
				new h2d.Bitmap(numberBgTile, th);
				numberText = new h2d.Text(numberFont, th);
				numberText.color = numberColor;
				numberText.smooth = true;
				numberText.x = 5;
				numberText.y = 5;
				numberText.text = Std.string(p.pid);
				numberBgTile.scaleToSize(numberText.textWidth + 10, numberText.textHeight + 10);
				th.name = p.pid + '_thumb';
				th.setScale(CV.COMIC_SCALE);
				thumbs.push(th);
			}
		}
	}

	override public function show() {
		super.show();

		if (thumbs == null) {
			loadThumbs();
		}

		for (t in thumbs) {
			indexScroll.addChild(t);

			t.alpha = 0;
			var a = 1;
			Artean.simple(.2, 0, a, (currA) -> {
				t.alpha = currA;
			});
		}

		indexScroll.x = (CV.s2d.width - indexScroll.outerWidth) * .5;
		addChild(indexScroll);
	}

	override public function hide() {
		super.hide();

		if (thumbs == null) {
			return;
		}
		for (t in thumbs) {
			t.remove();
		}
	}

	override public function update(dt:Float) {
		super.update(dt);

		if (indexScroll != null)
			indexScroll.update(dt);
	}
}
