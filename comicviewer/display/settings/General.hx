package comicviewer.display.settings;

import comicviewer.utils.Syspecial;
import comicviewer.CV;
import comicviewer.display.ui.Scroll;
import comicviewer.utils.Defaults;
import comicviewer.utils.FontPool;
import comicviewer.display.items.TextItem;
import comicviewer.components.button.Radio;
import libgoa.Artean;
import h3d.Vector;
import h3d.Vector4;
import hxd.Event;
import h2d.Interactive;
import h2d.Text;
import h2d.Tile;
import h2d.Object;
import h2d.RenderContext;

#if comtor
@:publicFields
#end
@:access(h2d.Dropdown)
class General extends SettingsPanel {
	var localesDropdown:h2d.Dropdown;
	var localesScrollItem:GeneralScrollItem;
	var layoutContainer:Scroll;
	var effectsScrollItem:GeneralScrollItem;
	var effectsCheckbox:Checkbox;

	public function new(parent:h2d.Layers = null, layer:Int = 0) {
		super('general', parent, layer);

		layoutContainer = new Scroll(this);
		layoutContainer.scrollHeight = CV.s2d.height * .85;
		layoutContainer.layout = Vertical;
		layoutContainer.verticalSpacing = 10;
		layoutContainer.padding = 5;
		layoutContainer.maxWidth = CV.s2d.width - 20;

		var font = FontPool.get(CV.GENERAL_VALUES.uiFontName, Std.int(.04 * CV.s2d.width));

		localesScrollItem = new GeneralScrollItem(layoutContainer);
		var t = new h2d.Text(font, localesScrollItem);
		t.letterSpacing = 2;
		t.smooth = true;
		t.text = 'LANGUAGE';
		t.setPosition(10, (GeneralScrollItem.HEIGHT - t.textHeight) * .5);
		localesDropdown = new h2d.Dropdown(localesScrollItem);
		var h = localesDropdown.maxHeight;
		localesDropdown.dropdownList.backgroundTile = h2d.Tile.fromColor(0);
		localesDropdown.setPosition(0, (GeneralScrollItem.HEIGHT - h) * .5);
		var h25 = Std.int(h * .25);
		var h5 = Std.int(h * .5);
		var d:hxd.BitmapData = new hxd.BitmapData(h, h);
		d.fill(0, 0, h, h, 0x404040);
		d.line(h25, h5, h - h25, h5, 0xffCCCCCC);
		localesDropdown.tileArrow = Tile.fromBitmap(d);
		d = new hxd.BitmapData(h, h);
		d.line(h5, h25, h5, h - h25, 0xffCCCCCC);
		localesDropdown.tileArrowOpen = Tile.fromBitmap(d);
		localesDropdown.tileOverItem = Tile.fromColor(0x000000, 0, 0);
		localesDropdown.onClose = () -> {
			var item = localesDropdown.getItem(localesDropdown.selectedItem);
			if (item != null) {
				var langCode = item.name;
				CV.instance.setLocale(langCode);
				@:privateAccess CV.signals.triggers.languageSelected.trigger(langCode);
			}
			return;
		};

		effectsScrollItem = new GeneralScrollItem(layoutContainer);
		t = new h2d.Text(font, effectsScrollItem);
		t.letterSpacing = 2;
		t.smooth = true;
		t.text = 'EFFECTS';
		t.setPosition(10, (GeneralScrollItem.HEIGHT - t.textHeight) * .5);
		effectsCheckbox = new Checkbox(effectsScrollItem);
		localesDropdown.x = CV.s2d.width - GeneralScrollItem.HEIGHT * 2;
		effectsCheckbox.x = localesDropdown.x + effectsCheckbox.width * .75;
		effectsCheckbox.onChange = (v) -> {
			@:privateAccess CV.signals.triggers.effectsEnabled.trigger(v);
		};
	}

	override public function show() {
		super.show();

		if (effectsCheckbox != null)
			effectsCheckbox.checked = CV.EFFECTS_ENABLED;

		var items = localesDropdown.getItems();
		for (i in 0...items.length) {
			if (libgoa.Itzul.currentLocale == items[i].name) {
				localesDropdown.selectedItem = i;
				return;
			}
		}
	}

	public function addLanguage(code:String, name:String) {
		var font = FontPool.get(CV.GENERAL_VALUES.uiFontName, Std.int(.05 * CV.s2d.width));
		var ti = new h2d.Text(font);
		ti.color = new Vector4(1, 1, 1, 1);
		ti.smooth = true;
		ti.text = name.toLowerCase();
		ti.name = code;

		localesDropdown.addItem(ti);
		localesDropdown.selectedItem = 0;
	}

	override public function update(dt:Float) {
		if (layoutContainer != null)
			layoutContainer.update(dt);
	}
}

private class GeneralScrollItem extends h2d.Object {
	static public inline var HEIGHT = 120;

	public function new(?parent) {
		super(parent);

		new h2d.Bitmap(h2d.Tile.fromColor(0x333333, Std.int(CV.s2d.width - 10), HEIGHT, .7), this);
	}
}

/**
	Primitive checkbox.
	Can be used for fast UI creation for debugging purposes.
**/
private class Checkbox extends Interactive {
	private static var bg:Tile;
	private static var check:Tile;

	/**
		Current checkbox state. Does not trigger `onChange` when changed, use `setChecked` function instead if even is required.
	**/
	public var checked:Bool;

	public var label:Text;

	public function new(?parent:Object, ?_label:String) {
		var size = Std.int(GeneralScrollItem.HEIGHT * .5);
		super(size, size, parent);
		if (bg == null) {
			bg = Tile.fromColor(0x808080, size, size);
			check = Tile.fromColor(0xAA0033, Std.int(size * .9), Std.int(size * .9));
			check.dx = check.dy = size * .05;
			setPosition(0, (GeneralScrollItem.HEIGHT - size) * .5);
		}

		if (_label != null) {
			label = new Text(hxd.res.DefaultFont.get(), this);
			label.text = _label;
			label.x = 11;
			this.width = 11 + label.textWidth;
		}
	}

	/**
		Sets `checked` flag and triggers `onChange` if value changes.
	**/
	public function setChecked(v:Bool):Void {
		if (checked != v) {
			checked = v;
			onChange(v);
		}
	}

	override private function draw(ctx:RenderContext) {
		emitTile(ctx, bg);
		if (checked)
			emitTile(ctx, check);
	}

	override public function handleEvent(e:Event) {
		var mdown = mouseDownButton;
		super.handleEvent(e);
		if (e.cancel)
			return;
		switch (e.kind) {
			case ERelease:
				if (mdown == e.button) {
					setChecked(!checked);
				}
			default:
		}
	}

	public dynamic function onChange(value:Bool):Void {}
}
