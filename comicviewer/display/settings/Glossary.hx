package comicviewer.display.settings;

import comicviewer.CV;
import comicviewer.display.ui.Scroll;
import comicviewer.utils.parser.Types.GlossaryItem;
import comicviewer.utils.Defaults;
import comicviewer.utils.FontPool;

#if comtor
@:publicFields
#end
class Glossary extends SettingsPanel {
	var detailWindow:DetailWindow;
	var items:Map<String, GlossaryItem>;
	var layoutContainer:Scroll;

	public function new(parent:h2d.Layers = null, layer:Int = 0) {
		super('glossary', parent, layer);

		items = new Map();
		layoutContainer = new Scroll(this);
		layoutContainer.scrollHeight = CV.s2d.height * .85;
		layoutContainer.layout = Vertical;
		layoutContainer.verticalSpacing = 10;
		layoutContainer.padding = 5;
		layoutContainer.onChildClicked = c -> {
			showWithLink(c.name);
		};

		detailWindow = new DetailWindow(show);
	}

	function createLayout(i:GlossaryItem) {
		var path = Defaults.GET_GLOSSARY_TEXTURE_PATH() + i.image;
		var tile = try {
			hxd.Res.load(path).toTile();
		} catch (e:hxd.res.NotFound) {
			// trace('texture $path not found');
			h2d.Tile.fromColor(Std.int(hxd.Math.random(0xffffff)));
		}
		var l = new GlossaryItemLayout(tile, getLocalizedText(i.title), layoutContainer);
		l.name = i.link;
		l.onClick = showWithLink;
	}

	function getLocalizedText(key:String) {
		return libgoa.Itzul.getLocalized(key, 'eu_ES');
	}

	public function addItem(i:GlossaryItem) {
		if (items.exists(i.link))
			return;

		i.visible = false;
		items.set(i.link, i);
	}

	public function visibilizeLinks(links:Array<String>) {
		var i;
		for (l in links) {
			i = items.get(l);
			if (i == null || i.visible)
				continue;
			i.visible = true;
			createLayout(i);
		}
	}

	public function showWithLink(glink:String = null) {
		super.show();

		if (glink != null && items.exists(glink)) {
			layoutContainer.remove();
			var gitem = items.get(glink);
			detailWindow.title = getLocalizedText(gitem.title);
			detailWindow.body = getLocalizedText(gitem.body);
			var path = Defaults.GET_GLOSSARY_TEXTURE_PATH() + gitem.detailImage;
			detailWindow.image = try {
				hxd.Res.load(path).toTile();
			} catch (e:hxd.res.NotFound) {
				trace('texture $path not found');
				h2d.Tile.fromColor(Std.int(hxd.Math.random(0xffffff)));
			}
			addChild(detailWindow);
		} else {
			addChild(layoutContainer);
		}
	}

	override public function show() {
		showWithLink();
	}

	override public function hide() {
		super.remove();

		if (detailWindow != null)
			detailWindow.remove();
	}

	override public function update(dt:Float) {
		if (layoutContainer != null)
			layoutContainer.update(dt);
	}

	#if comtor
	public function getDataJson() {
		var ret = '"glossaryItems":[';
		var raw;
		for (item in items) {
			raw = StringTools.replace(item.link, 'gl_', '');
			ret += '"$raw",';
		}

		ret = ret.substring(0, ret.length - 1);

		ret += ']';

		return ret;
	}
	#end

	static public function GET_TITLE_FONT(size:Int) {
		var DESC = hxd.res.Embed.getResource('comicviewer/utils/default_font/rustic_capitals_sdf.fnt');
		var BYTES = hxd.res.Embed.getResource('comicviewer/utils/default_font/rustic_capitals_sdf.png');
		var fontBmp = new hxd.res.BitmapFont(DESC.entry);
		@:privateAccess fontBmp.loader = BYTES.loader;
		var titleFont = fontBmp.toSdfFont(size, h2d.Font.SDFChannel.Alpha, .45);
		DESC = null;
		BYTES = null;

		return titleFont;
	}
}

class GlossaryItemLayout extends h2d.Flow {
	var titleTxt:h2d.Text;
	var imageBmp:h2d.Bitmap;

	public function new(tile:h2d.Tile, title:String, ?parent) {
		super(parent);

		horizontalSpacing = 10;
		verticalAlign = Middle;
		padding = 5;
		minWidth = Std.int(CV.s2d.width - 10);
		imageBmp = new h2d.Bitmap(tile.sub(0, 0, tile.width, tile.width), this);
		var scale = minWidth * .25 / tile.width;
		imageBmp.setScale(scale);
		titleTxt = new h2d.Text(Glossary.GET_TITLE_FONT(Std.int(CV.GENERAL_VALUES.uiFontSize * CV.s2d.width)), this);
		titleTxt.text = title.toUpperCase();
		var imageSize = imageBmp.getBounds();
		backgroundTile = h2d.Tile.fromColor(0x333333, Std.int(CV.s2d.width), Std.int(tile.width * scale), .7);
	}

	public dynamic function onClick(s:String) {}
}

class DetailWindow extends h2d.Object {
	public var title(default, set):String;
	public var body(default, set):String;
	public var image(default, set):h2d.Tile;

	var closeTxt:h2d.Text;
	var lineSingleTile:h2d.Tile;
	var featherTile:h2d.Tile;
	var topLine:h2d.Bitmap;
	var feather:h2d.Bitmap;
	var bottomLine:h2d.Bitmap;

	var titleTxt:h2d.Text;
	var bodyTxt:h2d.Text;
	var imageBmp:h2d.Bitmap;
	var textFlow:h2d.Flow;
	var imageTextContainer:h2d.Object;
	var imageTextWidth:Float;

	public function new(?parent, ?onRemove:Void->Void) {
		super(parent);

		closeTxt = new h2d.Text(FontPool.get(CV.GENERAL_VALUES.uiFontName, Std.int(CV.GENERAL_VALUES.uiFontSize * CV.s2d.width)), this);
		closeTxt.text = 'X';
		closeTxt.color = new h3d.Vector4();
		closeTxt.scale(2);
		closeTxt.x = CV.s2d.width - closeTxt.textWidth * closeTxt.scaleX * 1.5;
		closeTxt.y = 10;
		var closeInt = new h2d.Interactive(closeTxt.textWidth, closeTxt.textHeight, closeTxt);
		closeInt.onClick = function(_) {
			remove();
			if (onRemove != null)
				onRemove();
		}

		topLine = new h2d.Bitmap(getLineSingleTile(), this);
		topLine.y = CV.s2d.height * .05;
		lineSingleTile.setSize(CV.s2d.width, lineSingleTile.height);
		topLine.tileWrap = true;

		imageTextContainer = new h2d.Object(this);
		imageTextWidth = CV.s2d.width * .55;
		imageBmp = new h2d.Bitmap(imageTextContainer);
		imageBmp.x = 10;

		textFlow = new h2d.Flow(imageTextContainer);
		textFlow.backgroundTile = h2d.Tile.fromColor(0xffffff, Std.int(imageTextWidth), 10, .5);
		textFlow.layout = Vertical;
		textFlow.verticalSpacing = 15;
		textFlow.padding = 20;
		textFlow.horizontalSpacing = 10;

		titleTxt = new h2d.Text(Glossary.GET_TITLE_FONT(Std.int(CV.GENERAL_VALUES.uiFontSize * 1.5 * CV.s2d.width)), textFlow);
		bodyTxt = new h2d.Text(FontPool.get(CV.GENERAL_VALUES.uiFontName, Std.int(CV.GENERAL_VALUES.uiFontSize * CV.s2d.width)), textFlow);
		bodyTxt.lineSpacing = 3;
		bodyTxt.letterSpacing = 1;
		titleTxt.color = new h3d.Vector4();
		bodyTxt.color = new h3d.Vector4();
		bodyTxt.maxWidth = imageTextWidth;

		textFlow.x = CV.s2d.width - imageTextWidth * 1.1;
		imageTextContainer.y = topLine.y + topLine.tile.height;

		feather = new h2d.Bitmap(getFeatherTile(), this);
		feather.x = (CV.s2d.width - feather.tile.width) * .5;

		bottomLine = new h2d.Bitmap(lineSingleTile, this);
		bottomLine.y = CV.s2d.height - Tab.HEIGHT - lineSingleTile.height * .5;
		bottomLine.tileWrap = true;
		bottomLine.scaleY *= -1;

		feather.y = bottomLine.y - feather.tile.height * 1.5;
	}

	function set_title(v:String) {
		titleTxt.text = v.toUpperCase();
		return title = v;
	}

	function set_body(v:String) {
		bodyTxt.text = v;
		return body = v;
	}

	function set_image(t:h2d.Tile) {
		imageBmp.tile = t;
		var topLineBorder = topLine.y + topLine.tile.height;
		var tarteHeight = bottomLine.y - bottomLine.tile.height - topLineBorder;
		imageBmp.setScale(tarteHeight / t.height);
		textFlow.y = (imageBmp.tile.height * imageBmp.scaleY - textFlow.outerHeight) * .5;
		return image = t;
	}

	function getFeatherTile() {
		if (featherTile != null)
			return featherTile;

		var BYTES = hxd.res.Embed.getResource('comicviewer/utils/img/glossaryFeather.png');
		var loader = @:privateAccess BYTES.loader;
		featherTile = new hxd.res.Any(loader, BYTES.entry).toTile();
		return featherTile;
	}

	function getLineSingleTile() {
		if (lineSingleTile != null)
			return lineSingleTile;

		var BYTES = hxd.res.Embed.getResource('comicviewer/utils/img/glossaryLineSingleTile.png');
		var loader = @:privateAccess BYTES.loader;
		lineSingleTile = new hxd.res.Any(loader, BYTES.entry).toTile();
		return lineSingleTile;
	}
}
