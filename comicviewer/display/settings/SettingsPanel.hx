package comicviewer.display.settings;

class SettingsPanel extends h2d.Layers {
	public var tab:Tab;
	public var id(default, null):String;

	var panelParent:h2d.Layers;
	var layer:Int;

	public function new(id:String, panelParent:h2d.Layers = null, layer:Int = 0) {
		super();

		this.id = id;
		this.panelParent = panelParent;
		this.layer = layer;
	}

	public function show() {
		if (panelParent != null)
			panelParent.add(this, layer);
	}

	public function hide() {
		remove();
	}

	public function update(dt:Float) {}
}
