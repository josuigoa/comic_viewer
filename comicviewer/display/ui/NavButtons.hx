package comicviewer.display.ui;

import comicviewer.CV;
import libgoa.Artean;

class NavButtons {
	var previ:h2d.Interactive;
	var nexti:h2d.Interactive;
	var s2d:h2d.Scene;

	var showMapInteractive:h2d.Interactive;
	var showMapBMP:h2d.Bitmap;
	var showSettingsInteractive:h2d.Interactive;
	var showSettingsBMP:h2d.Bitmap;

	public function new(s2d:h2d.Scene) {
		this.s2d = s2d;

		var div = .05;
		previ = new h2d.Interactive(s2d.width * div, s2d.height * .7, s2d);
		previ.y = s2d.height * .15;
		previ.backgroundColor = #if mobile 0x00000000 #else 0x77cccccc #end;
		previ.onClick = function(_) {
			CV.instance.flipPage(-1, true);
		}
		nexti = new h2d.Interactive(s2d.width * div, s2d.height * .7, s2d);
		nexti.x = s2d.width * (1 - div);
		nexti.y = s2d.height * .15;
		nexti.backgroundColor = #if mobile 0x00000000 #else 0x77cccccc #end;
		nexti.onClick = (_) -> {
			CV.instance.flipPage(1, true);
		}

		var settingsTile = comicviewer.utils.Defaults.LOAD_SETTINGS_ICON();
		settingsTile.setCenterRatio(0, .5);
		showSettingsInteractive = new h2d.Interactive(settingsTile.width, settingsTile.height);
		showSettingsInteractive.x = CV.s2d.width * .5 - settingsTile.width * .5;
		showSettingsInteractive.y = CV.s2d.height - settingsTile.height;
		CV.instance.add(showSettingsInteractive, CV.GENERAL_VALUES.settingsLayer + 1);
		showSettingsBMP = new h2d.Bitmap(settingsTile, showSettingsInteractive);
		showSettingsBMP.y = settingsTile.height * .5;
		showSettingsInteractive.onClick = onShowSettingsClicked;
	}

	function onShowSettingsClicked(_) {
		if (CV.IS_MAP_SHOWN || CV.IS_SCALED)
			return;

		if (CV.IS_SETTINGS_SHOWN) {
			CV.instance.hideSettings();
			settings(false);
		} else {
			CV.instance.showSettings();
			settings(true);
		}
	}

	public function settings(show:Bool) {
		var startY, newY;
		if (show) {
			startY = CV.s2d.height;
			newY = 0;

			Artean.simple(.2, startY, newY, (currY) -> {
				showSettingsInteractive.y = currY;
				showSettingsBMP.scaleY = -1 + 2 * (currY / startY);
			});
		} else {
			startY = 0;
			newY = CV.s2d.height - showSettingsBMP.tile.iheight;

			Artean.simple(.2, startY, newY, (currY) -> {
				showSettingsInteractive.y = currY;
				showSettingsBMP.scaleY = -1 + 2 * (currY / newY);
			});
		}
	}
}
