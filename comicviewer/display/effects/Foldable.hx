package comicviewer.display.effects;

import comicviewer.CV;

/**
 * ...
 * @author josu
 */
private class MPoint {
	public var x:Float;
	public var y:Float;
	public var u:Float;
	public var v:Float;
	public var r:Float;
	public var g:Float;
	public var b:Float;
	public var a:Float;

	public function new(_x:Float, _y:Float, _u:Float, _v:Float) {
		x = _x;
		y = _y;
		u = _u;
		v = _v;
		r = g = b = a = 1;
	}
}

private class FoldableContent extends h3d.prim.Primitive {
	var buf:hxd.FloatBuffer;
	var index:hxd.IndexBuffer;
	var points:Array<MPoint>;

	public var x:Float;
	public var y:Float;

	public function new(w:Float, h:Float) {
		points = [];

		inline function add(x:Float, y:Float, u:Float, v:Float)
			points.push(new MPoint(x, y, u, v));

		add(0, 0, 0, 0); // 0
		add(w, 0, 1, 0); // 1
		add(0, h * .33, 0, .33); // 2
		add(w, h * .33, 1, .33); // 3
		add(0, h * .66, 0, .66); // 4
		add(w, h * .66, 1, .66); // 5
		add(0, h, 0, 1); // 6
		add(w, h, 1, 1); // 7

		inline function addIndex(i)
			index.push(i);

		index = new hxd.IndexBuffer();

		/*
			0 * * * 1
			*       *
			2 * * * 3
			*       *
			4 * * * 5
			*       *
			6 * * * 7
		 */
		addIndex(0);
		addIndex(1);
		addIndex(2);

		addIndex(2);
		addIndex(1);
		addIndex(3);

		addIndex(2);
		addIndex(3);
		addIndex(4);

		addIndex(4);
		addIndex(3);
		addIndex(5);

		addIndex(4);
		addIndex(5);
		addIndex(6);

		addIndex(6);
		addIndex(5);
		addIndex(7);
	}

	public inline function createBuffer() {
		if (buffer != null)
			buffer.dispose();

		buf = new hxd.FloatBuffer();
		for (p in points) {
			buf.push(p.x);
			buf.push(p.y);
			buf.push(p.u);
			buf.push(p.v);
			buf.push(p.r);
			buf.push(p.g);
			buf.push(p.b);
			buf.push(p.a);
		}
	}

	override function alloc(engine:h3d.Engine) {
		if (index.length <= 0)
			return;
		if (buf == null)
			createBuffer();
		buffer = h3d.Buffer.ofFloats(buf, hxd.BufferFormat.H2D); // hxd.BufferFormat.XY_UV_RGBA
		indexes = h3d.Indexes.alloc(index);
	}

	override function render(engine:h3d.Engine) {
		if (index.length <= 0)
			return;
		flush();
		engine.renderIndexed(buffer, indexes);
		super.render(engine);
	}

	public inline function flush() {
		if (buffer == null || buffer.isDisposed())
			alloc(h3d.Engine.getCurrent());
	}

	public function setVerticeX(vInd:Int, val:Float)
		points[vInd].x = val;

	public function setVerticeY(vInd:Int, val:Float)
		points[vInd].y = val;

	public function setVerticeRgb(vInd:Int, val:Float)
		points[vInd].r = points[vInd].g = points[vInd].b = val;

	public function clear() {
		dispose();
	}
}

class Foldable extends h2d.Drawable {
	var content:FoldableContent;

	public var isFolded(default, null):Bool;
	public var isUnfolded(default, null):Bool;
	public var isFolding(default, null):Bool;
	public var foldedPercentage:Float;
	public var unfoldedPercentage:Float;
	public var foldingTime:Float;

	public var foldingValue(default, set):Float; // 0:closed, 1:opened

	var targetTexture:h3d.mat.Texture;

	public function new(targetTexture:h3d.mat.Texture) {
		super(null);
		this.targetTexture = targetTexture;
		content = new FoldableContent(Std.int(targetTexture.width), Std.int(targetTexture.height));
		x = CV.PAGE_SPRITE_POS.x;
		y = CV.PAGE_SPRITE_POS.y;
		foldedPercentage = 0.;
		unfoldedPercentage = 1.;
		foldingTime = .5;
		isFolded = true;

		blendMode = h2d.BlendMode.None;

		isUnfolded = !isFolded;
		foldingValue = (isFolded) ? foldedPercentage : unfoldedPercentage;

		clear();
	}

	public function fold(animate:Bool = true, oncomplete:Foldable->Void = null, force:Bool = false) {
		if ((isFolded || isFolding) && !force) {
			if (isFolded)
				completeUnFolding(true, oncomplete);
			return;
		}

		if (animate) {
			isUnfolded = isFolded = false;
			isFolding = true;

			var t = 0.;
			var currentFoldingPercentage = (foldingValue > 0) ? foldingValue : unfoldedPercentage;
			CV.instance.waitEvent.waitUntil(function(dt) {
				t += dt;
				if (t >= foldingTime || foldingValue <= 0) {
					completeUnFolding(true, oncomplete);
					return true;
				}
				foldingValue = foldedPercentage + (currentFoldingPercentage - t / foldingTime);
				return false;
			});
		} else {
			foldingValue = foldedPercentage;
			completeUnFolding(true, oncomplete);
		}
	}

	public function unfold(animate:Bool = true, oncomplete:Foldable->Void = null, force:Bool = false) {
		if ((isUnfolded || isFolding) && !force) {
			if (isUnfolded)
				completeUnFolding(false, oncomplete);
			return;
		}

		visible = true;
		if (animate) {
			isUnfolded = isFolded = false;
			isFolding = true;
			var t = foldingTime;
			var currentFoldingPercentage = (foldingValue > 0) ? foldingValue : foldedPercentage;
			CV.instance.waitEvent.waitUntil(function(dt) {
				if (!isUnfolded) {
					t -= dt;
					if (t <= 0 || foldingValue >= 1) {
						completeUnFolding(false, oncomplete);
						return true;
					}
					foldingValue = unfoldedPercentage + (currentFoldingPercentage - t / foldingTime);
				}

				return false;
			});
		} else {
			foldingValue = unfoldedPercentage;
			completeUnFolding(false, oncomplete);
		}
	}

	function completeUnFolding(isfolded:Bool, oncomplete:Foldable->Void) {
		isFolding = false;
		isFolded = isfolded;
		isUnfolded = !isFolded;
		foldingValue = (isfolded) ? foldedPercentage : unfoldedPercentage;
		updateFolding();

		if (oncomplete != null)
			oncomplete(this);

		if (isFolded) {
			visible = false;
			remove();
		}
	}

	function updateFolding() {
		if (foldingValue > 0 && parent == null) {
			visible = true;
			CV.instance.add(this, CV.GENERAL_VALUES.mapLayer);
		}

		var w = Std.int(targetTexture.width);
		var h = Std.int(targetTexture.height);

		var val = (w * .25) * (1. - foldingValue);
		content.setVerticeX(2, val);
		content.setVerticeX(6, val);
		content.setVerticeX(4, -val);
		content.setVerticeX(3, w - val);
		content.setVerticeX(7, w - val);
		content.setVerticeX(5, w + val);

		content.setVerticeRgb(2, foldingValue * foldingValue);
		content.setVerticeRgb(3, foldingValue * foldingValue);
		content.setVerticeRgb(4, foldingValue * foldingValue);
		content.setVerticeRgb(5, foldingValue * foldingValue);

		val = h * .33 * foldingValue;
		content.setVerticeY(2, val);
		content.setVerticeY(3, val);
		val = h * .66 * foldingValue;
		content.setVerticeY(4, val);
		content.setVerticeY(5, val);
		val = h * foldingValue;
		content.setVerticeY(6, val);
		content.setVerticeY(7, val);

		content.createBuffer();
	}

	public function update(dt:Float) {
		if (!visible)
			return;
	}

	override function onRemove() {
		super.onRemove();
		clear();
	}

	public function clear() {
		content.clear();
	}

	override function draw(ctx:h2d.RenderContext) {
		if (!ctx.beginDrawObject(this, targetTexture))
			return;
		content.render(ctx.engine);
	}

	override function sync(ctx:h2d.RenderContext) {
		super.sync(ctx);
		content.flush();
	}

	public function set_foldingValue(v:Float) {
		foldingValue = v;
		// isFolded = v <= 0;
		// isUnfolded = v >= 1;
		// isFolding = !isFolded && !isUnfolded;
		updateFolding();
		return v;
	}
}
