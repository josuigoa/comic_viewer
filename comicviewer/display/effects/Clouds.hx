package comicviewer.display.effects;

import libgoa.Artean;
import comicviewer.utils.Utils.defNan;
import comicviewer.display.Page;
import h2d.Bitmap;

/**
 * ...
 * @author josu
 */
typedef CloudsOptions = {
	var parentPage:Page;
	@:optional var dirX:Null<Float>;
	@:optional var dirY:Null<Float>;
	@:optional var cloudVel:Null<Float>;
	@:optional var numClouds:Null<UInt>;
	@:optional var minAlpha:Null<Float>;
	@:optional var maxAlpha:Null<Float>;
}

typedef Cloud = {
	var bmp:Bitmap;
	var velocity:Float;
	var isInPage:Bool;
}

#if comtor
@:publicFields
#end
class Clouds {
	var clouds:Array<Cloud>;
	var dirX:Float;
	var dirY:Float;
	var cloudVel:Float;
	var numClouds:UInt;
	var minAlpha:Float;
	var maxAlpha:Float;
	var parentPage:Page;

	public function new(opt:CloudsOptions) {
		parentPage = opt.parentPage;
		dirX = defNan(opt.dirX, 0);
		dirY = defNan(opt.dirY, 0);
		cloudVel = defNan(opt.cloudVel, 1);
		numClouds = defNan(opt.numClouds, 20);
		minAlpha = defNan(opt.minAlpha, 0);
		maxAlpha = defNan(opt.maxAlpha, 1);
	}

	public function start() {
		if (clouds == null) {
			var min = 3.;
			var max = 2.;
			clouds = [
				for (i in 0...numClouds) {
					var t = hxd.Res.load(comicviewer.utils.Defaults.GET_MAP_TEXTURE_PATH() + 'cloud${i % 5}.png').toTile();
					t.scaleToSize(t.width * (min + max * Math.random()), t.height * (min + max * Math.random()));
					{
						bmp: new Bitmap(t, parentPage),
						velocity: .5 + hxd.Math.random() * .5,
						isInPage: false
					};
				}
			];
		}

		var c, col;
		for (i in 0...clouds.length) {
			c = clouds[i];
			c.bmp.visible = i < numClouds;
			col = .2 + Math.random();
			c.bmp.color.set(col, col, col);
			spawn(c, true);
		}
	}

	function spawn(c:Cloud, isInitial:Bool = false) {
		if (!c.bmp.visible)
			return;

		c.bmp.rotation = hxd.Math.degToRad(Std.random(360));
		var alpha = minAlpha + (maxAlpha - minAlpha) * Math.random();
		c.bmp.x = Std.random(CV.GENERAL_VALUES.pageWidth);
		c.bmp.y = Std.random(CV.GENERAL_VALUES.pageHeight);
		c.isInPage = false;
		if (isInitial) {
			c.bmp.alpha = alpha;
		} else {
			c.bmp.alpha = 0;
			Artean.simple(2 + Std.random(3), 0, alpha, (a) -> {
				c.bmp.alpha = a;
			});
		}
	}

	function calculateIsInPage(c:Cloud) {
		var bmp = c.bmp;
		var horizontal = (bmp.x + bmp.tile.width) > 0 && bmp.x < CV.GENERAL_VALUES.pageWidth;
		var vertical = bmp.y > 0 && (bmp.y + bmp.tile.height) < CV.GENERAL_VALUES.pageHeight;
		if (c.isInPage && !(horizontal || vertical)) {
			Artean.simple(2 + Std.random(3), c.bmp.alpha, 0, (a) -> {
				c.bmp.alpha = a;
				if (a == 0)
					spawn(c);
			});
		} else if (!c.isInPage && (horizontal || vertical)) {
			c.isInPage = true;
		}
	}

	public function update(dt:Float) {
		if (clouds != null) {
			var bmp;
			for (c in clouds) {
				bmp = c.bmp;
				if (!bmp.visible)
					continue;
				bmp.x += dirX * cloudVel * c.velocity * dt;
				bmp.y += dirY * cloudVel * c.velocity * dt;
				calculateIsInPage(c);
			}
		}
	}
}
