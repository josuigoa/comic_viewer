package comicviewer.display;

import comicviewer.display.items.ComicItem;
import comicviewer.display.items.ImageItem;
import comicviewer.utils.Utils.*;
import comicviewer.utils.parser.Types;
import comicviewer.utils.HScriptHelper;

#if comtor
using haxe.io.Path;
#end

/**
 * ...
 * @author josu
 */
typedef PageOptions = {
	@:optional var name:String;
	@:optional var pid:Null<Int>;
	@:optional var parent:h2d.Object;
	@:optional var pos:h3d.Vector;
	@:optional var size:h3d.Vector;
	@:optional var thumbTile:h2d.Tile;
	@:optional var defaultFontName:String;
	@:optional var onEnterScript:DelayedScript;
	@:optional var onLeaveScript:String;
}

#if comtor
@:publicFields
#end
class Page extends h2d.Layers {
	public var pid:Int;
	public var thumbTile:h2d.Tile;
	// public var postShader:Shader;
	public var defaultFontName:String;
	public var started:Bool;

	var layer:Int = -1;
	var hasThumb:Bool;
	var items:Array<ComicItem>;
	var onEnterScript:DelayedScript;
	var onLeaveScript:String;
	var timeInPage:Float;

	static inline var maxTimeToOnenterscript:Float = 2;
	static inline var minTimeToOnLeaveScript:Float = 2;
	#if comtor
	static public inline var itemClicked:String = 'page.item.clicked';
	#else
	static public inline var glossaryClicked:String = 'page.glossaryLink.clicked';
	#end

	public function new(?opt:PageOptions) {
		pid = defNan(opt.pid, -1);
		layer = CV.GENERAL_VALUES.pageLayer;

		if ((opt.name == null || opt.name == '') && pid != -1)
			name = 'page_$pid';
		else if (opt.name != null && opt.name != '')
			name = opt.name;

		super(opt.parent);

		items = [];
		started = false;

		thumbTile = defNull(opt.thumbTile, null);
		defaultFontName = defNull(opt.defaultFontName, CV.GENERAL_VALUES.textFontName);
		onEnterScript = opt.onEnterScript;
		onLeaveScript = opt.onLeaveScript;
		// postShader = defNull(opt.postShader, null);
		var pos = defNull(opt.pos, new h3d.Vector());
		x = pos.x;
		y = pos.y;
	}

	public function start() {
		if (items == null || items.length == 0 || started)
			return;

		started = true;
		for (i in items)
			i.start();
	}

	// TODO onComplete beharrezkoa da?
	public function enter(onComplete:Void->Void = null) {
		start();
		timeInPage = 0;
		if (onEnterScript != null) {
			onComplete();
			haxe.Timer.delay(() -> {
				if (started && CV.EFFECTS_ENABLED)
					HScriptHelper.executeScriptFile(onEnterScript.script, function() {});
			}, onEnterScript.delayMs);
		} else
			onComplete();
	}

	public function leave(onComplete:Void->Void = null) {
		if (onLeaveScript != null && timeInPage >= minTimeToOnLeaveScript && CV.EFFECTS_ENABLED)
			HScriptHelper.executeScriptFile(onLeaveScript, onComplete);
		else
			onComplete();
	}

	public function stop() {
		if (items == null || items.length == 0 || !started)
			return;

		started = false;
		for (i in items)
			i.stop();
	}

	public function setPageScale(s:Float) {
		setScale(s);

		for (i in items)
			i.setItemScale(s);
	}

	public function addItem(i:ComicItem) {
		items.push(i);
		return i;
	}

	public function getItem(id:String) {
		for (i in items)
			if (haxe.io.Path.withoutDirectory(i.id) == id)
				return i;
		return null;
	}

	#if comtor
	public function removeItem(i:ComicItem) {
		removeChild(i.drawable);
		items.remove(i);
	}
	#end

	public function load() {
		if (items == null || items.length == 0)
			return;

		if (onEnterScript != null)
			HScriptHelper.loadScript(onEnterScript.script);
		if (onLeaveScript != null)
			HScriptHelper.loadScript(onLeaveScript);

		CV.instance.add(this, layer);
		for (i in items)
			i.load();
	}

	public function unload() {
		if (items == null || items.length == 0)
			return;

		if (onEnterScript != null)
			HScriptHelper.removeScript(onEnterScript.script);
		if (onLeaveScript != null)
			HScriptHelper.removeScript(onLeaveScript);

		for (i in items)
			i.unload();
		remove();
	}

	public function updateLocalization() {
		if (items == null || items.length == 0)
			return;

		for (i in items) {
			i.updateLocalization();
		}
	}

	public function update(dt:Float) {
		timeInPage += dt;

		if (items == null || items.length == 0)
			return;

		for (i in items) {
			i.update(dt);
		}
	}

	public function dispose() {
		if (items == null || items.length == 0)
			return;

		for (i in items) {
			i.remove();
			i = null;
		}

		items = null;

		remove();
	}

	public function getGlossaryLinks()
		return [
			for (i in items) {
				if (i.glossaryLink != null) {
					i.glossaryLink;
				}
			}
		];

	/* GETTER & SETTER */
	#if comtor
	public function getDataJson(map:comicviewer.display.items.map.ComicMap = null) {
		var ret = '{"id": $pid';
		if (defaultFontName != null && defaultFontName != CV.GENERAL_VALUES.textFontName)
			ret += ',"pageDefaultFont":"${defaultFontName.withoutDirectory().withoutExtension()}"';

		if (onEnterScript != null)
			ret += ',"onEnterScript":{"script":"${onEnterScript.script}","delayMs":${onEnterScript.delayMs}}';
		if (onLeaveScript != null)
			ret += ',"onLeaveScript":"$onLeaveScript"';

		ret += ',"items":[';

		ret += getItemsDataJson();

		if (map != null) {
			var mapData = map.getCharactersJsonDataByPageIndex(pid);
			if (mapData != '') {
				ret += ',$mapData';
			}
		}

		ret += ']}';

		return ret;
	}

	function getItemsDataJson() {
		var ret = '';
		if (items.length > 0) {
			for (i in items) {
				ret += i.getDataJson() + ",";
			}
			ret = ret.substring(0, ret.length - 1); // remove the last comma
		}
		return ret;
	}
	#end
}
