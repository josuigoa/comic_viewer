package comicviewer.display.items;

import haxe.Timer;
import comicviewer.utils.Utils.*;
import comicviewer.utils.Defaults;
import libgoa.particles.ParticleSystem;
import libgoa.particles.loaders.ParticleLoader;

/**
 * ...
 * @author josu
 */
typedef ParticlesItemOptions = {
	> ComicItem.ComicItemOptions,
	var particlesConfPaths:Array<String>;
	@:optional var delayMs:Int;
}

class ParticlesItem extends ComicItem {
	var particlesConfPaths:Array<String>;
	var delayMs:Int;
	var particleSystem:ParticleSystem;

	public function new(opt:ParticlesItemOptions) {
		type = PARTICLES;
		defNull(opt.id, '');
		defNull(opt.color, h3d.Vector4.fromColor(0xFF333333));
		defNan(opt.layer, CV.GENERAL_VALUES.imageLayer);
		delayMs = defNan(opt.delayMs, 0);
		particlesConfPaths = defNull(opt.particlesConfPaths, []);

		if (particlesConfPaths.length == 0)
			return;

		super(opt);

		particleSystem = new ParticleSystem(this);
		for (c in particlesConfPaths) {
			particleSystem.addParticleGroup(ParticleLoader.load(Defaults.GET_PARTICLES_PATH() + c));
		}
	}

	override public function start() {
		super.start();
		haxe.Timer.delay(() -> {
			if (active && CV.EFFECTS_ENABLED)
				particleSystem.start();
		}, delayMs);
	}

	override function enableEffects(enable:Bool) {
		super.enableEffects(enable);
		if (particleSystem != null)
			if (enable)
				particleSystem.start();
			else
				particleSystem.stop();
	}

	override public function stop() {
		super.stop();
		particleSystem.stop();
	}

	override public function update(dt:Float) {
		super.update(dt);
		particleSystem.update(dt);
	}

	#if comtor
	override public function getDataJson() {
		var ret = super.getDataJson();

		ret = '{"type":"particles",' + ret;
		var pathsArray = [
			for (c in 0...particlesConfPaths.length)
				(c < particlesConfPaths.length - 1)
			?'"${particlesConfPaths[c]},"':'"${particlesConfPaths[c]}"'
		];
		ret += ',"particlesConfPaths":$pathsArray';
		if (delayMs != 0)
			ret += ',"delayMs":$delayMs';

		ret += '}';

		return ret;
	}
	#end
}
