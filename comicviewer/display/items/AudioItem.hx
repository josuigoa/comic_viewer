package comicviewer.display.items;

import hxd.snd.Channel;
import hxd.res.Sound;
import haxe.Timer;
import comicviewer.utils.Utils.*;
import comicviewer.utils.Defaults;

typedef AudioItemOptions = {
	> ComicItem.ComicItemOptions,
	@:optional var delayMs:Int;
}

class AudioItem extends ComicItem {
    var audio:Sound;
    var audioChannel:Channel;
	var delayMs:Int;

	public function new(opt:AudioItemOptions) {
		type = AUDIO;
		defNull(opt.id, '');
		delayMs = defNan(opt.delayMs, Std.int(CV.GENERAL_VALUES.pageFlippingTime * 1500));
		
        super(opt);
		inited = true;
	}
	
	override function updateLocalization() {
		super.updateLocalization();
		loadAudio();
	}

	function loadAudio() {
		var locAudio = comicviewer.utils.Defaults.GET_AUDIO_PATH() + getLocalizedText();
        audio = try {
                hxd.Res.load(locAudio).toSound();
            } catch (e:hxd.res.NotFound) {
                trace('audio $locAudio not found');
                null;
            }
	}

	override public function start() {
		if (audio == null)
			return;
		
        active = true;
		haxe.Timer.delay(() -> {
			if (active)
				audioChannel = audio.play();
		}, delayMs);
	}

	override public function stop() {
		active = false;
		if (audio == null)
			return;
        if (audioChannel != null) {
			audioChannel.fadeTo(0, .5, () -> audio.stop());
        }
	}

	#if comtor
	override public function getDataJson() {
		var ret = super.getDataJson();

		ret = '{"type":"audio",' + ret;
        if (delayMs != 0)
		    ret += ',"delayMs":$delayMs';
		ret += '}';

		return ret;
	}
	#end
}
