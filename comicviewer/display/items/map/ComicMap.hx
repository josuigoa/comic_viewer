package comicviewer.display.items.map;

import comicviewer.display.Page.PageOptions;
import comicviewer.display.effects.Clouds;
import comicviewer.display.items.ComicItem;
import comicviewer.CV;

/**
 * ...
 * @author josu
 */
class ComicMap extends Page {
	var characters:Array<Character>;
	var targetTexture:h3d.mat.Texture;
	var clouds:Clouds;

	public function new(?_options:PageOptions) {
		characters = [];
		_options.thumbTile = null;
		var w = Std.int(CV.GENERAL_VALUES.pageWidth * CV.COMIC_SCALE);
		var h = Std.int(CV.GENERAL_VALUES.pageHeight * CV.COMIC_SCALE);
		targetTexture = new h3d.mat.Texture(w, h, [h3d.mat.Data.TextureFlags.Target]);
		
		clouds = new Clouds({
			parentPage: this,
			dirX: -1 + Std.random(2),
			dirY: -1 + Std.random(2),
			cloudVel: 1 + Math.random() * 5,
			numClouds: 10 + Std.random(10),
			minAlpha: .7,
			maxAlpha: 1
		});

		super(_options);
	}

	override public function load() {
		super.load();
		haxe.Timer.delay(() -> remove(), 0);
	}
	
	override function start() {
		super.start();
		clouds.start();
	}

	override public function update(dt:Float) {
		if (!visible)
			return;
		super.update(dt);
		drawToTexture();
		clouds.update(dt);
	}

	override public function addItem(i:ComicItem) {
		super.addItem(i);
		if (i.type.exclusive(ComicItem.ItemType.MAP_CHAR))
			characters.push(Std.instance(i, Character));
		return i;
	}

	public function drawCharactersAtPage(pageIndex:Int) {
		var nameY = 80.;
		for (mi in characters)
			nameY += mi.drawPositionAtPage(pageIndex, nameY);
	}

	@:access(h2d.Scene)
	function drawToTexture() {
		var s = CV.s2d;
		s.ctx.engine = h3d.Engine.getCurrent();
		s.ctx.engine.begin();
		s.ctx.globalAlpha = alpha;
		s.ctx.begin();
		s.ctx.pushTarget(targetTexture);
		drawRec(s.ctx);
		s.ctx.popTarget();
	}

	#if comtor
	public function getCharactersJsonDataByPageIndex(pageId:Int) {
		var ret = [], data;
		for (mc in characters) {
			data = mc.getDataJsonByPage(pageId);
			if (data != null)
				ret.push(data);
		}

		return ret.join(',');
	}

	override function getItemsDataJson() {
		var ret = '';
		if (items.length > 0) {
			var noChars = [for (i in items) if (!i.type.exclusive(ComicItem.ItemType.MAP_CHAR)) i];
			for (i in noChars) {
				ret += i.getDataJson() + ",";
			}
			ret = ret.substring(0, ret.length - 1); // remove the last comma
		}
		return ret;
	}
	#end
}
