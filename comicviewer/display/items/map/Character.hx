package comicviewer.display.items.map;

import comicviewer.display.items.TextItem.TextItemOptions;
import comicviewer.utils.OrderedMap;
import comicviewer.utils.parser.Types;
import comicviewer.utils.Utils.defNull;
import h3d.Vector;
import h3d.Vector4;

/**
 * ...
 * @author josu
 */
@:allow(comicviewer.utils.parser.ComicParser)
class Character extends TextItem {
	var positions:OrderedMap<Int, CVector>;
	var pathGraphics:h2d.Graphics;
	var pathColor:Color;
	var firstAppearanceIndex:Int; // the first page where this item appears

	public function new(?opt:TextItemOptions) {
		positions = new OrderedMap<Int, CVector>(new haxe.ds.IntMap<CVector>());
		firstAppearanceIndex = 5000;
		opt.color = new Vector4(0, 0, 0, 1);
		opt.isUpperCase = true;
		opt.hasBg = false;
		opt.initPos = new h3d.Vector();
		opt.align = h2d.Text.Align.Left;
		opt.alignVertical = defNull(opt.alignVertical, TextItem.AlignVertical.Top);

		super(opt);

		type = ComicItem.ItemType.MAP_CHAR;
		visible = false;

		pathGraphics = new h2d.Graphics(this);
		var minCol = 0xFF432816;
		var maxCol = 0xFF735039;
		var colDiff = maxCol - minCol;

		pathColor = switch id {
			case 'mc_maansi': 0xff6d8855;
			case 'mc_alyosha': 0xff7e5a64;
			case 'mc_nevsky': 0xff59707e;
			case 'mc_mongol_exp': 0xff4e5454;
			case 'mc_golden_horde': 0xff9a1624;
			default: 0xffffffff;
		}

		// pathColor = new Vector(hxd.Math.random(0xFF) / 255, hxd.Math.random(0xEE) / 255, hxd.Math.random(0xDD) / 255, 1);
		color = pathColor; // .add(Vector.fromColor(0x222222));
	}

	public function drawPositionAtPage(pageId:Int, nameY:Float):Float {
		start();

		visible = pageId >= firstAppearanceIndex;

		if (pageId < firstAppearanceIndex)
			return 0;

		var p = null;
		var c = pathColor;
		var pathPos = new h3d.Vector();
		pathGraphics.lineStyle(6, c.toColor(), c.a);
		for (key in positions.keys()) {
			if (p == null) {
				p = positions.get(key);
				pathGraphics.moveTo(0, 0);
				pathPos = new h3d.Vector(p.x, p.y);
				p = p.sub(pathPos);
				continue;
			}

			if (key > pageId)
				break;

			p = positions.get(key).sub(pathPos);
			pathGraphics.lineTo(p.x, p.y);
			// pathGraphics.drawCircle(p.x, p.y, 4);
		}

		pathGraphics.beginFill(c.toColor(), c.a);
		pathGraphics.drawCircle(p.x, p.y, 12);
		pathGraphics.endFill();

		pathGraphics.setPosition(pathPos.x, pathPos.y);
		text.setPosition(10, nameY);

		return text.font.lineHeight;
	}

	override public function stop() {
		super.stop();
		pathGraphics.clear();
	}

	public function addPosition(pageId:Int, newPos:Vector) {
		if (pageId < firstAppearanceIndex) {
			firstAppearanceIndex = pageId;
		}

		newPos.set(newPos.x * CV.GENERAL_VALUES.pageWidth, newPos.y * CV.GENERAL_VALUES.pageHeight);
		positions.set(pageId, newPos);
	}

	#if comtor
	public function getDataJsonByPage(pageId:Int) {
		if (!positions.exists(pageId))
			return null;

		var ret;
		var p = positions.get(pageId).clone();
		if (pageId == firstAppearanceIndex) {
			// lehenengo agerraldia bada, propietate guztiak bolkatu
			initPos.set(p.x, p.y);
			ret = super.getDataJson();
			ret = ret.substring(0, ret.length - 1);
			if (pathColor != null && pathColor.toColor() != CV.GENERAL_VALUES.mapCharacterPathColor.toColor()) {
				ret += ',"pathColor":"${pathColor.toColor()}"';
			}
		} else {
			// hurrengo agerraldietan, mota, id eta posizioa bolkatzearekin nahikoa da
			ret = '{"type":"mapCharacter",';
			ret += '"id":"${id}",';
			p.x = hxd.Math.fmt(p.x / CV.GENERAL_VALUES.pageWidth);
			p.y = hxd.Math.fmt(p.y / CV.GENERAL_VALUES.pageHeight);
			ret += '"pos":[${p.x}, ${p.y}]';
		}

		ret += '}';

		// trace('characterGetJson.return: $ret');

		return ret;
	}
	#end
}
