package comicviewer.display.items;

import h3d.Vector;
import h3d.Vector4;
import h2d.Text;
import comicviewer.utils.FontPool;
import comicviewer.utils.Utils.*;
import comicviewer.utils.parser.Types;
import comicviewer.utils.Defaults;
import comicviewer.components.button.Clickable;

#if comtor
using haxe.io.Path;
#end

enum abstract AlignVertical(Int) from Int {
	var Top = 0;
	var Center;
	var Bottom;
}

/**
 * ...
 * @author josu
 */
typedef TextItemOptions = {
	> ComicItem.ComicItemOptions,
	@:optional var skin:String;
	@:optional var overrideFont:Bool;
	@:optional var isUpperCase:Bool;
	@:optional var isHtml:Bool;
	@:optional var hasBg:Bool;
	@:optional var bgLayer:Int;
	@:optional var isBgEllipse:Bool;
	@:optional var bgPadding:Float;
	@:optional var bgColor:Color;
	@:optional var letterSpacing:Int;
	@:optional var lineSpacing:Int;
	@:optional var fontName:String;
	@:optional var fontSize:Float;
	@:optional var align:TextAlign;
	@:optional var alignVertical:TextAlignVertical;
}

#if comtor
@:publicFields
#end
class TextItem extends ComicItem {
	var text:h2d.Text;
	var skin:String;
	var background:h2d.Graphics;
	var hasBg:Bool;
	var isHtml:Bool;
	var bgLayer:Int;
	var bgPadding:Float;
	var letterSpacing #if comtor (default, set) #end:Int;
	var lineSpacing #if comtor (default, set) #end:Int;
	var fontName:String;
	var textAlign #if comtor (default, set) #end:h2d.Text.Align;
	var alignVertical:TextAlignVertical;
	var isBgEllipse:Bool;
	var bgColor(default, set):Vector4;
	var isUpperCase(default, set):Bool;
	var glossaryLinkBgColor:h3d.Vector4 = new Vector4(.7, .7, 1);

	public var fontSize(default, set):Float;
	public var overrideFont:Bool;

	public function new(?opt:TextItemOptions) {
		type = TEXT;
		skin = defNull(opt.skin, Defaults.TEXT_SKIN_NAME);
		defNan(opt.layer, CV.GENERAL_VALUES.textLayer);
		defNull(opt.color, CV.GENERAL_VALUES.textColor);
		overrideFont = defNull(opt.overrideFont, Defaults.TEXT_OVERRIDE_FONT);
		hasBg = defNull(opt.hasBg, CV.GENERAL_VALUES.textHasBg);
		fontName = defNull(opt.fontName, CV.GENERAL_VALUES.textFontName);
		isHtml = opt.isHtml;
		size = defNull(opt.size, CV.GENERAL_VALUES.textItemSize.clone());
		bgLayer = defNan(opt.bgLayer, CV.GENERAL_VALUES.textBackgroundLayer);
		bgPadding = defNan(opt.bgPadding, CV.GENERAL_VALUES.textBgPadding);
		isBgEllipse = defNull(opt.isBgEllipse, CV.GENERAL_VALUES.textIsBgEllipse);

		super(opt);

		if (glossaryLink != null)
			color = glossaryLinkBgColor;

		isUpperCase = defNull(opt.isUpperCase, CV.GENERAL_VALUES.textUppercase);

		textAlign = defNull(opt.align, CV.GENERAL_VALUES.textAlign);
		alignVertical = defNull(opt.alignVertical, Center);

		if (size.x == CV.GENERAL_VALUES.textItemSize.x && size.y == CV.GENERAL_VALUES.textItemSize.y) {
			var sx = CV.GENERAL_VALUES.textItemSize.x * CV.GENERAL_VALUES.pageWidth; // - initPos.x;
			if (textAlign == h2d.Text.Align.Left)
				sx -= initPos.x;

			var sy = CV.GENERAL_VALUES.textItemSize.y * CV.GENERAL_VALUES.pageHeight; // - initPos.y;
			if (textAlign == h2d.Text.Align.Left)
				sy -= initPos.y;

			size = new Vector(sx, sy);
		} else {
			size.set(size.x * CV.GENERAL_VALUES.pageWidth, size.y * CV.GENERAL_VALUES.pageHeight);
		}

		letterSpacing = defNan(opt.letterSpacing, CV.GENERAL_VALUES.textLetterSpacing);

		lineSpacing = defNan(opt.lineSpacing, CV.GENERAL_VALUES.textLineSpacing);

		fontSize = defNan(opt.fontSize, CV.GENERAL_VALUES.textFontSize);
		if (fontSize == 0)
			fontSize = CV.GENERAL_VALUES.textFontSize;

		if (hasBg) {
			if (glossaryLink != null) {
				bgColor = glossaryLinkBgColor;
			} else {
				bgColor = defNull(opt.bgColor, CV.GENERAL_VALUES.textBgColor);
			}
		}

		inited = true;
	}

	override public function start() {
		super.start();

		if (glossaryLink != null) {
			var c = Std.instance(getComponent(Clickable.ID), Clickable);
			if (c != null) {
				var i = @:privateAccess c.interactive;
				if (text.textAlign == h2d.Text.Align.Center)
					i.x = -i.width * .5;

				if (alignVertical == Center)
					i.y = -i.height * .5;
				else if (alignVertical == Bottom)
					i.y = -i.height;
			}
		}

		#if comtor
		comtorInteractive.x = -size.x * .5;
		#end
	}

	override public function updateLocalization() {
		if (!inited)
			return;

		if (text == null) {
			var font = getFont();
			if (isHtml)
				drawable = text = new h2d.HtmlText(font, this);
			else
				drawable = text = new Text(font, this);
			text.textColor = color.toColor();
			text.lineSpacing = lineSpacing;
			text.letterSpacing = letterSpacing;
			text.textAlign = textAlign;
			text.maxWidth = size.x;
			text.smooth = true;
		}
		var _retText = getLocalizedText();
		text.text = (isUpperCase) ? _retText.toUpperCase() : _retText;

		if (text.maxWidth == null || text.maxWidth == 0) {
			var rightBorder = initPos.x + text.textWidth;
			if (text.textAlign == h2d.Text.Align.Center)
				rightBorder -= text.textWidth * .5;
			if (rightBorder > CV.GENERAL_VALUES.pageWidth) {
				text.maxWidth = CV.GENERAL_VALUES.pageWidth;
				if (text.textAlign == h2d.Text.Align.Left)
					text.maxWidth -= initPos.x;
			}
		} else if (text.textAlign == h2d.Text.Align.Center) {
			text.x = -text.maxWidth * .5;
		}

		if (alignVertical == Center)
			text.y = -text.textHeight * .5;
		else if (alignVertical == Bottom)
			text.y = -text.textHeight;

		setPosition(x, y);
		updateBg();
	}

	function updateBg() {
		if (!hasBg || bgColor == null || text == null || bgColor.a == 0)
			return;

		return;

		if (background == null) {
			background = new h2d.Graphics(this);
			addChild(text);
		}

		background.clear();
		background.beginFill(bgColor.toColor(), bgColor.a);
		var w = text.textWidth;
		var h = text.textHeight;
		var bgx, bgy;
		if (isBgEllipse) {
			if (w < h * 1.5)
				w = Std.int(h * 1.5);
			bgx = (text.textAlign != Align.Center) ? w * .5 : 0;
			bgx -= bgPadding;
			bgy = h * .5 - bgPadding;
			// background.drawCircle(bgx, -bgPadding + w * .5, (w + bgPadding * 2) * .5);
			background.drawEllipse(bgx, bgy, w * .5 + bgPadding * 2, h * .5 + bgPadding * 2);
		} else {
			if (text.textAlign == Align.Center && h < text.font.lineHeight)
				w *= 2;
			bgx = (text.textAlign == Align.Center) ? -w * .5 : 0;
			bgx -= bgPadding;
			bgy = -bgPadding;
			// background.drawRect(bgx, bgy, w + bgPadding * 2, h + bgPadding * 2);
			background.drawRoundedRect(bgx, bgy, w + bgPadding * 2, h + bgPadding * 2, 10);
		}
		background.endFill();
	}

	function getFont() {
		return FontPool.get(fontName, Std.int(fontSize * CV.GENERAL_VALUES.pageWidth));
	}

	/* GETTER & SETTER */
	override function set_initPos(p:Vector) {
		super.set_initPos(p);
		updateBg();
		return p;
	}

	function set_isUpperCase(_u:Bool) {
		isUpperCase = _u;
		updateLocalization();
		return _u;
	}

	override public function get_width()
		return if (background != null) background.getSize().width; else super.get_width();

	override public function get_height()
		return if (background != null) background.getSize().height; else super.get_height();

	override public function set_size(s:h3d.Vector) {
		if (text != null)
			text.maxWidth = s.x;
		updateBg();
		return size = s;
	}

	override public function set_color(c:h3d.Vector4) {
		if (text != null)
			text.color.load(c);
		return super.set_color(c);
	}

	function set_bgColor(c:Vector4) {
		bgColor = c;

		updateBg();
		return c;
	}

	function set_fontSize(v:Float) {
		if (text != null) {
			text.font = FontPool.get(text.font.name, Std.int(v * CV.GENERAL_VALUES.pageWidth));
			@:privateAccess text.rebuild();
		}
		return fontSize = v;
	}

	#if comtor
	public function set_lineSpacing(v:Int) {
		if (text != null)
			text.lineSpacing = v;
		return lineSpacing = v;
	}

	public function set_letterSpacing(v:Int) {
		if (text != null)
			text.letterSpacing = v;
		return letterSpacing = v;
	}

	public function set_textAlign(v:h2d.Text.Align) {
		if (text != null)
			text.textAlign = v;
		return textAlign = v;
	}

	override public function getDataJson() {
		var ret = super.getDataJson();
		var isMapChar = Std.is(this, comicviewer.display.items.map.Character);
		var type = if (isMapChar) 'mapCharacter' else if (isHtml) 'html' else 'text';
		ret = '{"type":"$type",' + ret;

		if (skin != Defaults.TEXT_SKIN_NAME)
			ret += ',"skin":"$skin"';

		var skin = CV.GENERAL_VALUES.textSkins.get(skin);

		var sx = hxd.Math.fmt(size.x / CV.GENERAL_VALUES.pageWidth);
		var sy = hxd.Math.fmt(size.y / CV.GENERAL_VALUES.pageHeight);
		if (sx != CV.GENERAL_VALUES.textItemSize.x || sy != CV.GENERAL_VALUES.textItemSize.y) {
			ret += ',"size":[$sx,$sy]';
		}

		if (textAlign != (skin.align : TextAlign))
			ret += ',"align":"$textAlign"';

		if ((isMapChar && fontSize != Defaults.MAP_CHARACTER_FONT_SIZE) || (!isMapChar && fontSize != skin.fontSize)) {
			ret += ',"fontSize":${hxd.Math.fmt(fontSize)}';
		}
		if (lineSpacing != skin.lineSpacing) {
			ret += ',"lineSpacing":${lineSpacing}';
		}
		if (letterSpacing != skin.letterSpacing) {
			ret += ',"letterSpacing":${letterSpacing}';
		}
		if (isUpperCase != skin.uppercase) {
			ret += ',"isUpperCase":${!skin.uppercase}';
		}
		if (fontName != skin.fontName) {
			ret += ',"fontName":"${fontName}"';
		}
		if (overrideFont != Defaults.TEXT_OVERRIDE_FONT) {
			ret += ',"overrideFont":"${overrideFont}"';
		}
		if (hasBg != skin.hasBg) {
			ret += ',"hasBg":$hasBg';
		}
		if (color != null && color.toColor() != Std.parseInt(skin.color))
			ret += ',"color":"${color.toColor()}"';

		if (hasBg && bgColor != null && bgColor.toColor() != Std.parseInt(skin.bgColor))
			ret += ',"bgColor":"${bgColor.toColor()}"';

		if (bgLayer != Defaults.TEXT_BACKGROUND_LAYER) {
			ret += ',"bgLayer":${bgLayer}';
		}
		if (bgPadding != skin.bgPadding) {
			ret += ',"bgPadding":${bgPadding}';
		}
		if (isBgEllipse != skin.isBgEllipse) {
			ret += ',"isBgEllipse":${isBgEllipse}';
		}

		ret += '}';

		// if (!isMapChar) trace('textGetJson.return: $ret');

		return ret;
	}
	#end
}
