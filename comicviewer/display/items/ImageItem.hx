package comicviewer.display.items;

import comicviewer.utils.Utils.*;

#if comtor
using haxe.io.Path;

import comicviewer.components.button.Clickable;
import comicviewer.components.*;
import comicviewer.components.shaders.*;
#end

/**
 * ...
 * @author josu
 */
typedef ImageItemOptions = {
	> ComicItem.ComicItemOptions,
	@:optional var texturePath:String;
	@:optional var u:Float;
	@:optional var v:Float;
	@:optional var centered:Bool;
}

#if comtor
@:publicFields
#end
class ImageItem extends ComicItem {
	var bitmap:h2d.Bitmap;
	var texturePath:String;

	public var centered(default, set):Bool;

	public function new(?opt:ImageItemOptions) {
		type = IMAGE;
		defNull(opt.id, '');
		defNull(opt.color, 0xFF333333);
		defNan(opt.layer, CV.GENERAL_VALUES.imageLayer);
		size = defNull(opt.size, CV.GENERAL_VALUES.imageItemSize);

		super(opt);

		centered = defNull(opt.centered, CV.GENERAL_VALUES.imageCentered);
		texturePath = defNull(opt.texturePath, comicviewer.utils.Defaults.GET_BASE_TEXTURE_PATH());

		if (id != null && id.indexOf('thumb') != -1) {
			size.x = .1 * CV.COMIC_SCALE;
			size.y = .1 * CV.COMIC_SCALE;
		}

		inited = true;
	}

	override function updateLocalization() {
		if (!inited)
			return;

		var _retTex = texturePath + getLocalizedText();
		var tile = try {
			hxd.Res.load(_retTex).toTile();
		} catch (e:hxd.res.NotFound) {
			trace('texture $_retTex not found');
			h2d.Tile.fromColor(color.toColor(), color.a);
		}
		if (centered)
			tile = tile.center();
		if (drawable == null)
			drawable = bitmap = new h2d.Bitmap(tile, this);
		else
			bitmap.tile = tile;
		var _w, _h;
		if (absoluteSize != null) {
			_w = absoluteSize.x / tile.width;
			_h = absoluteSize.y / tile.height;
		} else {
			if (size.x == CV.GENERAL_VALUES.imageItemSize.x && size.y == CV.GENERAL_VALUES.imageItemSize.y) {
				_w = 1.;
				_h = 1.;
			} else {
				_w = Std.int(size.x * CV.GENERAL_VALUES.pageWidth) / tile.width;
				_h = Std.int(size.y * CV.GENERAL_VALUES.pageHeight) / tile.height;
			}
		}

		setPosition(initPos.x, initPos.y);
		setScale(hxd.Math.max(_w, _h));
	}

	override function unload(doDispose:Bool = false) {
		super.unload(true);
	}

	override function dispose() {
		if (bitmap != null && bitmap.tile != null)
			bitmap.tile.dispose();
		super.dispose();
	}

	public function set_centered(c:Bool) {
		if (c && bitmap != null)
			bitmap.tile = bitmap.tile.center();
		return centered = c;
	}

	#if comtor
	override public function getDataJson() {
		var ret = super.getDataJson();

		ret = '{"type":"image",' + ret;

		if (centered != CV.GENERAL_VALUES.imageCentered) {
			ret += ',"centered":${centered}';
		}

		if (size.x != CV.GENERAL_VALUES.imageItemSize.x || size.y != CV.GENERAL_VALUES.imageItemSize.y) {
			ret += ',"size":[${size.x},${size.y}]';
		}

		// return haxe.Json.stringify(ret);
		ret += '}';

		return ret;
	}
	#end
}
