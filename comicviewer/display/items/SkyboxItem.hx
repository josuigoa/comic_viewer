package comicviewer.display.items;

import comicviewer.utils.Utils.*;

#if comtor
using haxe.io.Path;

import comicviewer.components.button.Clickable;
import comicviewer.components.*;
import comicviewer.components.shaders.*;
#end

/**
 * ...
 * @author josu
 */
typedef SkyboxItemOptions = {
	> ComicItem.ComicItemOptions,
	@:optional var texturesPaths:Array<String>;
}

#if comtor
@:publicFields
#end
class SkyboxItem extends ComicItem {
	var texturesPaths:Array<String>;
	var cameraController:h3d.scene.CameraController;

	public function new(?opt:SkyboxItemOptions) {
		type = SKYBOX;
		defNull(opt.id, '');
		// defNan(opt.layer, CV.GENERAL_VALUES.imageLayer);
		texturesPaths = defNull(opt.texturesPaths, []);

		super(opt);

		var skySize:Int = 128;
		var skyTexture = new h3d.mat.Texture(skySize, skySize, [Cube, MipMapped]);
		function loadTex(tex:String):hxd.res.Image {
			return hxd.Res.load(comicviewer.utils.Defaults.GET_BASE_TEXTURE_PATH() + 'extra/' + tex).toImage();
		}
		var faceColors = [loadTex('ft.jpg'), loadTex('bk.jpg'), loadTex('rt.jpg'), loadTex('lf.jpg'), loadTex('up.jpg'), loadTex('dn.jpg')];
		for (i in 0...6) {
			skyTexture.uploadPixels(faceColors[i].getPixels(h3d.mat.Texture.nativeFormat), 0, i);
		}
		skyTexture.mipMap = Linear;

		var sky = new h3d.prim.Sphere(30, skySize, skySize);
		sky.addNormals();
		var skyMesh = new h3d.scene.Mesh(sky, CV.s3d);
		skyMesh.material.mainPass.culling = Front;
		skyMesh.material.mainPass.addShader(new h3d.shader.CubeMap(skyTexture));
		skyMesh.material.shadows = false;

		var tex = loadTex('noho_maansi.png').toTexture();
		// creates a new unit cube
		var prim = new h3d.prim.Cube(.01, 1, 1);

		// translate it so its center will be at the center of the cube
		prim.translate( -0.005, -0.5, -0.5);

		// unindex the faces to create hard edges normals
		prim.unindex();

		// add face normals
		prim.addNormals();

		// add texture coordinates
		prim.addUVs();
		var mat = h3d.mat.Material.create(tex);
		// GARRANTZITSUA!!
		// irudiaren alpha kanala aplikatzeko, bertzenaz alpha dena beltza ikusiko da
		mat.blendMode = Alpha;
		var mesh = new h3d.scene.Mesh(prim, mat, CV.s3d);
		mesh.x = -19;
		mesh.y = 0;
		mesh.z = -2;

		inited = true;
	} // new

	override function start() {
		super.start();
		cameraController = new h3d.scene.CameraController(5, CV.s3d);
		cameraController.loadFromCamera();
	}

	override function updateLocalization() {
		if (!inited)
			return;

		// var _retTex = texturePath + getLocalizedText();
		// var tile = try {
		// 				hxd.Res.load(_retTex).toTile();
		// 			} catch (e:hxd.res.NotFound) {
		// 				trace('texture $_retTex not found');
		// 				h2d.Tile.fromColor(color.toColor(), color.a);
		// 			}
		// if (centered) tile = tile.center();
		// if (bitmap == null) drawable = bitmap = new h2d.Bitmap(tile, this);
		// else bitmap.tile = tile;
		// var _w, _h;
		// if (size.x == -1 && size.y == -1) {
		// 	_w = 1.;
		// 	_h = 1.;
		// } else {
		// 	_w = Std.int(size.x * CV.GENERAL_VALUES.pageWidth)/tile.width;
		// 	_h = Std.int(size.y * CV.GENERAL_VALUES.pageHeight)/tile.height;
		// }

		// setScale(hxd.Math.max(_w, _h));
	}

	#if comtor
	override public function getDataJson() {
		var ret = super.getDataJson();

		ret = '{"type":"skybox",' + ret;

		// return haxe.Json.stringify(ret);
		ret += '}';

		return ret;
	} // getDataJson
	#end
}
