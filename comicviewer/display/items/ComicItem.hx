package comicviewer.display.items;

import tink.CoreApi.CallbackLink;
import comicviewer.utils.Utils.defNull;
import comicviewer.utils.parser.Types;
import comicviewer.utils.Defaults;
import comicviewer.components.Component;

typedef ComicItemOptions = {
	var id:String;
	@:optional var parent:h2d.Layers;
	@:optional var drawable:h2d.Drawable;
	@:optional var color:Color;
	@:optional var initPos:CVector;
	@:optional var absoluteSize:CVector;
	@:optional var size:CVector;
	@:optional var layer:Int;
	@:optional var glossaryLink:String;
	@:optional var components:Array<comicviewer.utils.parser.Types.ComponentData>;
}

enum abstract ItemType(UInt) to UInt from UInt {
	var UNKNOWN = 0;
	var IMAGE = value(0);
	var TEXT = value(1);
	var MAP_TEXT = value(2) | TEXT;
	var MAP_CHAR = value(3) | TEXT;
	var SKYBOX = value(4);
	var PARTICLES = value(5);
	var AUDIO = value(6);

	static inline function value(index:Int)
		return 1 << index;

	inline public function is(it:ItemType)
		return it & this > 0;

	inline public function exclusive(it:ItemType)
		return it == this;
}

#if comtor
@:publicFields
#end
class ComicItem extends h2d.Object {
	public var drawable:h2d.Drawable;

	var components:Map<String, Component>;

	public var id(default, set):String = '';
	public var type(default, null):ItemType;
	public var initPos(default, set):h3d.Vector;
	public var width(get, null):Float;
	public var height(get, null):Float;
	public var absoluteSize(default, set):h3d.Vector;
	public var size(default, set):h3d.Vector;
	// public var rotation(default, set):Float;
	public var color(default, set):h3d.Vector4;
	// public var filter(default, set):h2d.filter.Filter;
	// public var alpha(default, set):Float;
	// public var visible(default, set):Bool;
	public var itemParent(default, set):h2d.Layers;
	public var active:Bool;
	public var layer:Int;
	public var glossaryLink:String;

	var loaded:Bool = false;
	var inited:Bool = false;
	var effectHandlerLink:CallbackLink;
	#if comtor
	var comtorInteractive:h2d.Interactive;
	#end

	public function new(opt:ComicItemOptions) {
		super();

		drawable = opt.drawable;
		if (drawable != null) {
			drawable.name = id;
			addChild(drawable);
		}
		itemParent = opt.parent;
		color = defNull(opt.color, new h3d.Vector4(1, 1, 1, 1));
		absoluteSize = opt.absoluteSize;

		layer = opt.layer;

		// var _p = defNull(opt.initPos, new h3d.Vector(CV.GENERAL_VALUES.itemPos.x, CV.GENERAL_VALUES.itemPos.y));
		var _p = defNull(opt.initPos, CV.GENERAL_VALUES.itemPos);
		_p.x *= CV.GENERAL_VALUES.pageWidth;
		_p.y *= CV.GENERAL_VALUES.pageHeight;
		initPos = _p;
		id = opt.id;

		components = new Map();
		if (opt.components != null) {
			var comps = opt.components;
			var compOpt, comp;
			for (c in comps) {
				comp = null;
				compOpt = c.options;
				compOpt.name = '${id}_${c.id.toLowerCase()}';
				if (c.constructor != null)
					comp = c.constructor(compOpt);
				if (comp != null)
					addComponent(comp);
				else
					trace('couldn\'t create ${c.id} component');
			}
		}
		glossaryLink = defNull(opt.glossaryLink, CV.GENERAL_VALUES.glossaryLink);
		if (glossaryLink != CV.GENERAL_VALUES.glossaryLink)
			addComponent(new comicviewer.components.button.Clickable(onGlossaryClick));

		active = false;
	}

	public function addComponent(c:Component) {
		c.item = this;
		components.set(c.id, c);
	}

	public function getComponent(cid:String) {
		return components.get(cid);
	}

	public function hasComponent(cid:String) {
		return getComponent(cid) != null;
	}

	public function removeComponent(cid:String) {
		var c = getComponent(cid);
		if (c == null)
			return;

		c.item = null;
		components.remove(cid);
	}

	function enableEffects(enable:Bool) {}

	function onGlossaryClick(_) {
		#if !comtor
		@:privateAccess comicviewer.CV.signals.triggers.itemClicked.trigger(this);
		#end
	}

	public function load() {
		if (loaded)
			return;

		visible = true;
		if (itemParent != null)
			itemParent.add(this, layer);
		updateLocalization();
		loaded = true;
	}

	public function start() {
		load();
		effectHandlerLink = CV.signals.effectsEnabled.handle(b -> enableEffects(b));
		updateLocalization();

		active = true;

		if (!Lambda.empty(components))
			for (c in components)
				c.start();

		#if comtor
		comtorInteractive = new h2d.Interactive(width, height, this);
		comtorInteractive.onClick = function(_) CV.instance.onItemClicked(this);
		#end
	}

	public function unload(doDispose = false) {
		if (!loaded)
			return;

		if (doDispose)
			dispose();
		loaded = visible = false;
		remove();
	}

	public function dispose() {
		if (drawable != null) {
			drawable.remove();
			drawable = null;
		}
	}

	public function stop() {
		effectHandlerLink.cancel();
		active = false;

		if (!Lambda.empty(components))
			for (c in components)
				c.stop();

		#if comtor
		comtorInteractive.remove();
		#end
	}

	public function setItemScale(s:Float) {}

	public function updateLocalization() {
		if (!inited)
			return;

		getLocalizedText();
	}

	function getLocalizedText() {
		return libgoa.Itzul.getLocalized(id, 'eu_ES');
	}

	public function update(dt:Float) {
		if (!active)
			return;

		for (c in components)
			c.update(dt);
	}

	public function addShader(s:hxsl.Shader)
		if (drawable != null)
			drawable.addShader(s);

	public function removeShader(s:hxsl.Shader)
		if (drawable != null)
			drawable.removeShader(s);

	public function set_id(_id:String) {
		id = _id;
		updateLocalization();
		return _id;
	}

	public function set_initPos(p:h3d.Vector) {
		setPosition(p.x, p.y);
		return initPos = p;
	}

	public function get_width()
		return getSize().width;

	public function get_height()
		return getSize().height;

	public function set_absoluteSize(s:h3d.Vector)
		return absoluteSize = s;

	public function set_size(s:h3d.Vector)
		return size = s;

	// public function setRotation(v:Float) return rotation = v;
	public function set_color(c:h3d.Vector4) {
		// if (drawable != null) drawable.color.set(c.r, c.g, c.b, c.a);
		if (color == null)
			color = new h3d.Vector4();
		color.load(c);
		return c;
	}

	// public function setFilter(f:h2d.filter.Filter) return filter = drawable.filter = f;
	// public function setAlpha(a:Float) return alpha = drawable.alpha = a;
	// public function setVisible(v:Bool) return visible = drawable.visible = v;
	public function set_itemParent(p:h2d.Layers) {
		itemParent = p;
		return p;
	}

	#if comtor
	public function getDataJson() {
		var ret = '"id":"${id}"';

		if (initPos != null && initPos.x != CV.GENERAL_VALUES.itemPos.x || initPos.y != CV.GENERAL_VALUES.itemPos.y) {
			var _x = hxd.Math.fmt(initPos.x / CV.GENERAL_VALUES.pageWidth);
			var _y = hxd.Math.fmt(initPos.y / CV.GENERAL_VALUES.pageHeight);
			ret += ',"pos":[$_x,$_y]';
		}
		if (glossaryLink != CV.GENERAL_VALUES.glossaryLink) {
			ret += ',"glossaryLink":"${glossaryLink}"';
		}

		// var col = color;
		// var defCol = Defaults.TEXT_COLOR;
		// if (col != null && (col.r != defCol.r || col.g != defCol.g || col.b != defCol.b || col.a != defCol.a)) {
		// 	ret += ',"color":"${col.toColor()}"';
		// }

		if (Lambda.count(components) > 0) {
			var compJson = '';
			var comp, cj;
			for (c in components) {
				comp = Std.downcast(c, comicviewer.components.Component);
				if (comp != null) {
					cj = comp.getDataJson();
					if (cj != '')
						compJson += cj + ',';
				}
			}
			if (compJson != '')
				ret += ',"components":[${compJson.substring(0, compJson.length - 1)}]';
		}

		return ret;
	}
	#end
}
