package comicviewer.utils;

import haxe.macro.Expr;
import haxe.macro.Context;

class Utils {
	macro public static function defNull(value:Expr, def:Expr):Expr {
		return macro @:pos(Context.currentPos()) {
			if ($value == null)
				$value = $def;
			$value;
		}
	}

	macro public static function defNan(value:Expr, def:Expr):Expr {
		return macro @:pos(Context.currentPos()) {
			if (Math.isNaN($value))
				$value = $def;
			$value;
		}
	}
	
	#if !macro
	static public function scalePageAround(scalingPage:comicviewer.display.Page, offsetX:Float, offsetY:Float, newScale:Float) { 
		// scaling will be done relatively 
		var relScaleX = newScale / scalingPage.scaleX; 
		var relScaleY = newScale / scalingPage.scaleY; 
		// map vector to centre point within parent scope 
		var ac = scalingPage.parent.globalToLocal( new h2d.col.Point( offsetX, offsetY ) ); 
		// current registered postion ab 
		var ab = new h2d.col.Point( scalingPage.x, scalingPage.y ); 
		// cb = ab - ac, this vector that will scale as it runs from the centre 
		var cb = ab.sub( ac ); 
		cb.x *= relScaleX; 
		cb.y *= relScaleY; 
		// recaulate ab, this will be the adjusted position for the clip 
		ab = ac.add( cb ); 
		// set actual properties 
		scalingPage.setPageScale(newScale);
		scalingPage.x = ab.x; 
		scalingPage.y = ab.y; 
    }
	#end
}
