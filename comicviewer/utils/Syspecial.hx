package comicviewer.utils;

@:forward
enum abstract Key(String) to String {
	var androidInternal = 'android_internal_storage_path';
	var androidExternal = 'android_external_storage_path';
	var androidObbDir = 'android_obb_storage_path';
	var iosDocumentPath = 'ios_document_path';
	var iosResourcePath = 'ios_resource_path';
	var iosRetinaScaleFactor = 'ios_retina_scale_factor';
	var iosDeviceName = 'ios_device_name';
}

@:access(String)
class Syspecial {
	#if hl
	@:hlNative("std", "sys_special")
	static function sys_special(key:hl.Bytes):Null<hl.Bytes> {
		return null;
	}

	static public function get(key:Key) {
		try {
			var sd = sys_special(key.toUtf8());
			return (sd == null) ? key : String.fromUTF8(sd);
		} catch (e:Dynamic) {
			return Std.string(e);
		}
	}

	#if android
	@:hlNative("std", "android_alert_dialog_visit_web")
	static function android_alert_dialog_visit_web(title:hl.Bytes, message:hl.Bytes, buyButtonText:hl.Bytes, websiteUrl:hl.Bytes):Bool {
		return false;
	}

	static public function alertDialogVisitWeb(title:String, message:String, buyButtonText:String, websiteUrl:String) {
		try {
			return android_alert_dialog_visit_web(title.toUtf8(), message.toUtf8(), buyButtonText.toUtf8(), websiteUrl.toUtf8());
		} catch (e:Dynamic) {
			trace(Std.string(e));
		}
		return false;
	}
	#end

	static public function alertDialogVisitWeb(title:String, message:String, buyButtonText:String, websiteUrl:String) {
		var flags = new haxe.EnumFlags<hl.UI.DialogFlags>();
		flags.set(hl.UI.DialogFlags.YesNo);
		hl.UI.dialog(title, message, flags);
	}
	#else
	static public function alertDialogVisitWeb(title:String, message:String, buyButtonText:String, websiteUrl:String) {}
	#end
}
