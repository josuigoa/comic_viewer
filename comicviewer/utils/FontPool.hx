package comicviewer.utils;

import h2d.Font.SDFChannel;

class FontPool {
	// <font_name, <font_size, font>>
	static var fonts:Map<String, Map<Int, h2d.Font>>;

	static inline function init() {
		if (fonts != null)
			return;

		fonts = new Map();
		var textSize = Std.int(CV.GENERAL_VALUES.textFontSize * CV.GENERAL_VALUES.pageWidth);
		var uiSize = Std.int(CV.GENERAL_VALUES.uiFontSize * CV.GENERAL_VALUES.pageWidth);
		FontPool.set(CV.GENERAL_VALUES.textFontName, textSize, Defaults.LOAD_TEXT_FONT(textSize));
		FontPool.set(CV.GENERAL_VALUES.uiFontName, uiSize, Defaults.LOAD_UI_FONT(uiSize));
	}

	static public function get(name:String, size:Int) {
		init();

		inline function resizeFont(name:String, size:Int) {
			var f = if (name == CV.GENERAL_VALUES.uiFontName)
					fonts.get(CV.GENERAL_VALUES.uiFontName).get(Std.int(CV.GENERAL_VALUES.uiFontSize * CV.GENERAL_VALUES.pageWidth)).clone();
				else
					fonts.get(CV.GENERAL_VALUES.textFontName).get(Std.int(CV.GENERAL_VALUES.textFontSize * CV.GENERAL_VALUES.pageWidth)).clone();
			f.resizeTo(size);
			FontPool.set(f.name, size, f);
			return f;
		}

		inline function loadFont(name:String, size:Int) {
			try {
				return hxd.Res.load(CV.ASSETS_BASE_PATH + 'fonts/${name}/${name}.fnt').to(hxd.res.BitmapFont).toSdfFont(size, SDFChannel.Alpha);
			} catch (e:Dynamic) {
				return resizeFont(name, size);
			}
		}

		var fsize = fonts.get(name);
		var font;
		if (fsize == null)
			return set(name, size, resizeFont(name, size));

		var font = fsize.get(size);
		if (font == null)
			return set(name, size, loadFont(name, size));

		return font;
	}

	static public function set(name:String, size:Int, font:h2d.Font) {
		init();

		var size_font = fonts.get(name);
		if (size_font == null) {
			size_font = new Map();
			fonts.set(name, size_font);
		}

		size_font.set(size, font);
		return font;
	}
}
