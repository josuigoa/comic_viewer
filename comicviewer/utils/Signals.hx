package comicviewer.utils;

import comicviewer.display.items.ComicItem;

using tink.CoreApi;

class Signals {
	var triggers:Triggers;

	public var effectsEnabled(default, null):Signal<Bool>;
	public var itemClicked(default, null):Signal<ComicItem>;
	public var languageSelected(default, null):Signal<String>;
	public var extraDataParsed(default, null):Signal<Any>;

	public function new() {
		triggers = new Triggers();
		effectsEnabled = triggers.effectsEnabled.asSignal();
		itemClicked = triggers.itemClicked.asSignal();
		languageSelected = triggers.languageSelected.asSignal();
		extraDataParsed = triggers.extraDataParsed.asSignal();
	}
}

@:allow(comicviewer.utils.Signals)
private class Triggers {
	var effectsEnabled:SignalTrigger<Bool>;
	var itemClicked:SignalTrigger<ComicItem>;
	var languageSelected:SignalTrigger<String>;
	var extraDataParsed:SignalTrigger<Any>;

	public function new() {
		effectsEnabled = Signal.trigger();
		itemClicked = Signal.trigger();
		languageSelected = Signal.trigger();
		extraDataParsed = Signal.trigger();
	}
}
