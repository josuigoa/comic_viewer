package comicviewer.utils;

import h2d.Font.SDFChannel;
import h3d.Vector;
import h3d.Vector4;
import h2d.Text;
import comicviewer.utils.parser.Types;

class Defaults {
	// COMIC
	inline static public var UI_FONT_NAME:String = 'luxe_default_sdf';
	#if !comtor inline #end static public var UI_FONT_SIZE:Float = .03;
	#if !comtor inline #end static public var ASSETS_BASE_PATH:String = '';
	#if !comtor inline #end static var HSCRIPTS_PATH:String = 'hscripts/';
	#if !comtor inline #end static var PARTICLES_PATH:String = 'particles/';
	#if !comtor inline #end static var AUDIO_PATH:String = 'audio/';

	static public inline function GET_HSCRIPTS_PATH()
		return CV.ASSETS_BASE_PATH + HSCRIPTS_PATH;

	static public inline function GET_PARTICLES_PATH()
		return CV.ASSETS_BASE_PATH + PARTICLES_PATH;

	static public inline function GET_AUDIO_PATH()
		return CV.ASSETS_BASE_PATH + AUDIO_PATH;

	#if !comtor inline #end static public var ZOOM_STEP:Float = .02;
	#if !comtor inline #end static public var MIN_ZOOM:Float = 1.;
	#if !comtor inline #end static public var MAX_ZOOM:Float = 1.8;
	#if !comtor inline #end static public var PAGE_FLIPPING_TIME:Float = .2;
	// PAGE
	#if !comtor inline #end static public var PAGE_LAYER:Int = 0;
	#if !comtor inline #end static public var PAGE_WIDTH:Float = 752;
	#if !comtor inline #end static public var PAGE_HEIGHT:Float = 1062;
	static public var ITEM_POS:CVector = new Vector();
	static public var ITEM_PARALLAX:CVector = new Vector();
	// TEXT
	#if !comtor inline #end static public var TEXT_BACKGROUND_LAYER:Int = 1;
	#if !comtor inline #end static public var TEXT_LAYER:Int = 2;
	inline static public var TEXT_FONT_NAME:String = 'wurper_sdf';
	inline static public var TEXT_SKIN_NAME:String = '__default__';
	#if !comtor inline #end static public var TEXT_FONT_SIZE:Float = .023;
	static public var TEXT_ITEM_SIZE:CVector = new Vector(1, 1);
	#if !comtor inline #end static public var TEXT_OVERRIDE_FONT:Bool = true;
	static public var TEXT_COLOR:Color = Vector4.fromColor(0xFF000000);
	#if !comtor inline #end static public var TEXT_HAS_BG:Bool = false;
	#if !comtor inline #end static public var TEXT_IS_BG_ELLIPSE:Bool = false;
	#if !comtor inline #end static public var TEXT_BG_PADDING:Float = 5;
	static public var TEXT_BG_COLOR:Color = Vector4.fromColor(0xCCFFFFFF);
	#if !comtor inline #end static public var TEXT_UPPERCASE:Bool = false;
	#if !comtor inline #end static public var TEXT_LINE_SPACING:Int = -3;
	#if !comtor inline #end static public var TEXT_LETTER_SPACING:Int = 0;
	#if !comtor inline #end static public var TEXT_TRANSLATIONS:Map<String, String> = null;
	static public var TEXT_ALIGN:TextAlign = Align.Center;
	#if !comtor inline #end static var TEXT_FONTS_PATH:String = 'fonts/';

	static public inline function GET_FONTS_PATH()
		return CV.ASSETS_BASE_PATH + TEXT_FONTS_PATH;

	// IMAGE
	static public var IMAGE_ITEM_SIZE:CVector = new Vector(-1, -1);
	#if !comtor inline #end static public var IMAGE_LAYER:Int = 0;
	#if !comtor inline #end static public var IMAGE_CENTERED:Bool = false;
	#if !comtor inline #end static public var IMAGE_HAS_THUMB:Bool = true;
	#if !comtor inline #end static public var IMAGE_SHADER_TIME:Float = -1;
	#if !comtor inline #end static public var IMAGE_SHADER_SINE:Float = -1;
	#if !comtor inline #end static var IMAGE_BASE_TEXTURE_PATH:String = 'textures/';
	#if !comtor inline #end static var IMAGE_PAGE_TEXTURE_PATH:String = 'pages/';
	#if !comtor inline #end static var IMAGE_MAP_TEXTURE_PATH:String = 'map/';
	#if !comtor inline #end static var IMAGE_GLOSSARY_TEXTURE_PATH:String = 'glossary/';

	static public inline function GET_BASE_TEXTURE_PATH()
		return CV.ASSETS_BASE_PATH + IMAGE_BASE_TEXTURE_PATH;

	static public inline function GET_PAGE_TEXTURE_PATH()
		return GET_BASE_TEXTURE_PATH() + IMAGE_PAGE_TEXTURE_PATH;

	static public inline function GET_GLOSSARY_TEXTURE_PATH()
		return GET_BASE_TEXTURE_PATH() + IMAGE_GLOSSARY_TEXTURE_PATH;

	static public inline function GET_MAP_TEXTURE_PATH()
		return GET_BASE_TEXTURE_PATH() + IMAGE_MAP_TEXTURE_PATH;

	// MAP
	#if !comtor inline #end static public var MAP_LAYER:Int = 1;
	#if !comtor inline #end static public var MAP_CHARACTER_FONT_SIZE:Float = .035;
	static public var MAP_CHARACTER_PATH_COLOR:Color = Vector4.fromColor(0xCC88FF88);
	// GLOSSARY
	#if !comtor inline #end static public var SETTINGS_LAYER:Int = 5;
	// #if !comtor inline #end static public var GLOSSARY_LAYER:Int = 2;
	#if !comtor inline #end static public var GLOSSARY_LINK:String = null;
	#if !comtor inline #end static public var GLOSSARY_TITLE_POINT_SIZE:Float = .05;
	#if !comtor inline #end static public var GLOSSARY_BODY_POINT_SIZE:Float = .05;

	// SETTINGS
	static public function LOAD_TEXT_FONT(size:Int) {
		var DESC = hxd.res.Embed.getResource('comicviewer/utils/default_font/wurper_sdf.fnt');
		var BYTES = hxd.res.Embed.getResource('comicviewer/utils/default_font/wurper_sdf.png');
		var fontBmp = new hxd.res.BitmapFont(DESC.entry);
		@:privateAccess fontBmp.loader = BYTES.loader;
		var textFont = fontBmp.toSdfFont(size, SDFChannel.Alpha);
		DESC = null;
		BYTES = null;

		return textFont;
	}

	static public function LOAD_UI_FONT(size:Int) {
		var DESC = hxd.res.Embed.getResource('comicviewer/utils/default_font/luxe_default_sdf.fnt');
		var BYTES = hxd.res.Embed.getResource('comicviewer/utils/default_font/luxe_default_sdf.png');
		var fontBmp = new hxd.res.BitmapFont(DESC.entry);
		@:privateAccess fontBmp.loader = BYTES.loader;
		var uiFont = fontBmp.toSdfFont(size, SDFChannel.Alpha, .45);
		DESC = null;
		BYTES = null;

		return uiFont;
	}

	static public function LOAD_SETTINGS_ICON() {
		var BYTES = hxd.res.Embed.getResource('comicviewer/utils/img/settings.png');
		var loader = @:privateAccess BYTES.loader;
		return new hxd.res.Any(loader, BYTES.entry).toTile();
	}

	static public function LOAD_TUTORIAL_IMAGE() {
		var BYTES = hxd.res.Embed.getResource('comicviewer/utils/img/tutorial.png');
		var loader = @:privateAccess BYTES.loader;
		return new hxd.res.Any(loader, BYTES.entry).toTile();
	}

	@:generic
	static public function fillMissingFieldsFrom<T>(secondary:T, primary:T) {
		for (f in Reflect.fields(primary)) {
			if (Reflect.field(secondary, f) == null)
				Reflect.setField(secondary, f, Reflect.field(primary, f));
		}

		return secondary;
	}

	static public function getDefaultGeneralValues():comicviewer.utils.parser.Types.ComicGeneralValues {
		return {
			comicId: 'comicId',
			comicName: 'comic name',
			pageWidth: 752,
			pageHeight: 1062,
			uiFontName: UI_FONT_NAME,
			uiFontSize: UI_FONT_SIZE,
			zoomStep: ZOOM_STEP,
			minZoom: MIN_ZOOM,
			maxZoom: MAX_ZOOM,
			pageFlippingTime: PAGE_FLIPPING_TIME,
			pageLayer: PAGE_LAYER,
			itemPos: [ITEM_POS.x, ITEM_POS.y],
			textBackgroundLayer: TEXT_BACKGROUND_LAYER,
			textLayer: TEXT_LAYER,
			textFontName: TEXT_FONT_NAME,
			textFontSize: TEXT_FONT_SIZE,
			textItemSize: [TEXT_ITEM_SIZE.x, TEXT_ITEM_SIZE.y],
			textOverrideFont: TEXT_OVERRIDE_FONT,
			textColor: TEXT_COLOR.toColor(),
			textHasBg: TEXT_HAS_BG,
			textIsBgEllipse: TEXT_IS_BG_ELLIPSE,
			textBgPadding: TEXT_BG_PADDING,
			textBgColor: TEXT_BG_COLOR.toColor(),
			textUppercase: TEXT_UPPERCASE,
			textLineSpacing: TEXT_LINE_SPACING,
			textLetterSpacing: TEXT_LETTER_SPACING,
			textAlign: switch (TEXT_ALIGN) {
				case Align.Center: 'center';
				case Align.Right: 'right';
				default: 'left';
			},
			textSkins: null,
			imageItemSize: [IMAGE_ITEM_SIZE.x, IMAGE_ITEM_SIZE.y],
			imageLayer: IMAGE_LAYER,
			imageCentered: IMAGE_CENTERED,
			mapLayer: MAP_LAYER,
			mapCharacterFontSize: MAP_CHARACTER_FONT_SIZE,
			mapCharacterPathColor: MAP_CHARACTER_PATH_COLOR.toColor(),
			settingsLayer: SETTINGS_LAYER,
			glossaryLink: GLOSSARY_LINK,
			glossaryTitlePointSize: GLOSSARY_TITLE_POINT_SIZE,
			glossaryBodyPointSize: GLOSSARY_BODY_POINT_SIZE
		}
	}

	#if comtor
	static public function getGeneralJson():String {
		var fieldVal:Any, json = '';
		var genVal = CV.GENERAL_VALUES;
		for (f in Reflect.fields(genVal)) {
			fieldVal = switch f {
				case 'pageWidth' | 'pageHeight': '${Std.string(Reflect.field(genVal, f))}';
				case 'uiFontName': UI_FONT_NAME != genVal.uiFontName ? '"${genVal.uiFontName}"' : null;
				case 'uiFontSize': UI_FONT_SIZE != genVal.uiFontSize ? genVal.uiFontSize : null;
				case 'pageFlippingTime': PAGE_FLIPPING_TIME != genVal.pageFlippingTime ? genVal.pageFlippingTime : null;
				case 'pageLayer': PAGE_LAYER != genVal.pageLayer ? genVal.pageLayer : null;
				case 'textBackgroundLayer': TEXT_BACKGROUND_LAYER != genVal.textBackgroundLayer ? genVal.textBackgroundLayer : null;
				case 'textLayer': TEXT_LAYER != genVal.textLayer ? genVal.textLayer : null;
				case 'textFontName': TEXT_FONT_NAME != genVal.textFontName ? '"${genVal.textFontName}"' : null;
				case 'textFontSize': TEXT_FONT_SIZE != genVal.textFontSize ? genVal.textFontSize : null;
				case 'textItemSize': TEXT_ITEM_SIZE.toString() != genVal.textItemSize.toString() ? genVal.textItemSize : null;
				case 'textOverrideFont': TEXT_OVERRIDE_FONT != genVal.textOverrideFont ? genVal.textOverrideFont : null;
				case 'textColor': TEXT_COLOR.toString() != genVal.textColor.toString() ? genVal.textColor : null;
				case 'textHasBg': TEXT_HAS_BG != genVal.textHasBg ? genVal.textHasBg : null;
				case 'textIsBgEllipse': TEXT_IS_BG_ELLIPSE != genVal.textIsBgEllipse ? genVal.textIsBgEllipse : null;
				case 'textBgPadding': TEXT_BG_PADDING != genVal.textBgPadding ? genVal.textBgPadding : null;
				case 'textBgColor': TEXT_BG_COLOR.toString() != genVal.textBgColor.toString() ? genVal.textBgColor : null;
				case 'textUppercase': TEXT_UPPERCASE != genVal.textUppercase ? genVal.textUppercase : null;
				case 'textLineSpacing': TEXT_LINE_SPACING != genVal.textLineSpacing ? genVal.textLineSpacing : null;
				case 'textLetterSpacing': TEXT_LETTER_SPACING != genVal.textLetterSpacing ? genVal.textLetterSpacing : null;
				case 'textAlign': TEXT_ALIGN.toString() != genVal.textAlign.toString() ? genVal.textAlign : null;
				case 'imageItemSize': IMAGE_ITEM_SIZE.toString() != genVal.imageItemSize.toString() ? genVal.imageItemSize : null;
				case 'imageLayer': IMAGE_LAYER != genVal.imageLayer ? genVal.imageLayer : null;
				case 'imageCentered': IMAGE_CENTERED != genVal.imageCentered ? genVal.imageCentered : null;
				case 'mapLayer': MAP_LAYER != genVal.mapLayer ? genVal.mapLayer : null;
				case 'mapCharacterFontSize': MAP_CHARACTER_FONT_SIZE != genVal.mapCharacterFontSize ? genVal.mapCharacterFontSize : null;
				case 'mapCharacterPathColor': MAP_CHARACTER_PATH_COLOR.toString() != genVal.mapCharacterPathColor.toString() ? genVal.mapCharacterPathColor : null;
				case 'settingsLayer': SETTINGS_LAYER != genVal.settingsLayer ? genVal.settingsLayer : null;
				case 'glossaryLink': GLOSSARY_LINK != genVal.glossaryLink ? genVal.glossaryLink : null;
				case 'glossaryTitlePointSize': GLOSSARY_TITLE_POINT_SIZE != genVal.glossaryTitlePointSize ? genVal.glossaryTitlePointSize : null;
				case 'glossaryBodyPointSize': GLOSSARY_BODY_POINT_SIZE != genVal.glossaryBodyPointSize ? genVal.glossaryBodyPointSize : null;
				case 'textSkins' | 'zoomStep' | 'minZoom' | 'maxZoom' | 'itemPos': null;
				default: '"${Std.string(Reflect.field(genVal, f))}"';
			}
			if (fieldVal != null) {
				json += '"$f":$fieldVal,';
			}
		}

		if (json != '')
			json = json.substring(0, json.length - 1); // remove the last comma

		trace('retJson: ' + json);
		return json;
	}
	#end
} // Defaults
