package comicviewer.utils;

using haxe.io.Path;

class HScriptHelper {
	static var loadedScripts:Map<String, hscript.Expr>;
	static var interp:hscript.Interp;
	static var parser:hscript.Parser;

	static public function loadScript(scriptName:String) {
		if (interp == null)
			initInterp();
		if (loadedScripts == null)
			loadedScripts = new Map();

		if (!loadedScripts.exists(scriptName)) {
			var hscriptPath = comicviewer.utils.Defaults.GET_HSCRIPTS_PATH() + scriptName + '.hscript';
			var rawScript = '';
			try {
				rawScript = hxd.Res.load(hscriptPath).toText();
			} catch (e:Dynamic) {
				error("Specified file [" + scriptName + "] not found in [" + hscriptPath + "]");
			}
			loadedScripts.set(scriptName, parser.parseString(rawScript));
		}
	}

	static public function removeScript(scriptName:String) {
		return (loadedScripts == null)
			? null
			: loadedScripts.remove(scriptName);
	}

	static public function executeScriptFile(scriptName:String, onComplete:Void->Void = null) {
		if (scriptName == null)
			return;

		var program = loadedScripts.get(scriptName);
		if (program == null) {
			trace('$scriptName script is not loaded!');
			onComplete();
			return;
		}

		interp.variables.set('onComplete', onComplete);
		info('execute: ' + interp.execute(program));
	}

	static function initInterp() {
		parser = new hscript.Parser();
		interp = new hscript.Interp();
		// export some useful classes
		interp.variables.set("Array", Array);
		interp.variables.set("StringTools", StringTools);
		// interp.variables.set("Sys", Sys);
		interp.variables.set("Std", Std);
		interp.variables.set("haxe", {
			"Json": haxe.Json,
			"Timer": haxe.Timer,
			"Serializer": haxe.Serializer,
			"Unserializer": haxe.Unserializer,
			"ds": {
				"ArraySort": haxe.ds.ArraySort
			}
		});
		interp.variables.set("hxd", {
			"File": hxd.File,
			"Res": hxd.Res,
			"Math": hxd.Math
		});
		interp.variables.set("Object", h2d.Object);
		interp.variables.set('CV', comicviewer.CV);
		interp.variables.set('ComicItem', comicviewer.display.items.ComicItem);
		interp.variables.set('ImageItem', comicviewer.display.items.ImageItem);
		interp.variables.set('TextItem', comicviewer.display.items.TextItem);
		interp.variables.set('Shake', comicviewer.display.effects.Shake);
		interp.variables.set('Artean', libgoa.Artean);
		interp.variables.set('Accelerometer', comicviewer.utils.Accelerometer);
	}

	static function info(msg:Dynamic) {
		trace("" + msg + "\n");
	}

	static function error(msg:Dynamic, exit:Bool = false) {
		info("ERROR: " + msg + "!");
		if (exit)
			hxd.System.exit();
	}
}
