package comicviewer.utils;

abstract Dyn<T>(Dynamic) from T to T {}

typedef MotionResult = {
    var x:Float;
    var y:Float;
    var z:Float;
}

typedef RotationAxis = {
    var pitch:Float;
    var yaw:Float;
    var roll:Float;
}

class CoreMotion {
    
    @:hlNative("std", "sys_coremotion_init")
    static function sys_coremotion_init() : Void { return; }
    static public function coreMotionInit() {
        sys_coremotion_init();
    }
    
    // @:hlNative("std", "sys_coremotion_acceleration")
    // static public function sys_coremotion_acceleration() : Dyn<{x:Float, y:Float, z:Float}> { return null; }
    // static public function getAcceleration():MotionResult {
    //     var r:{x:Float, y:Float, z:Float} = sys_coremotion_acceleration();
    //     var result:MotionResult = {x : r.x, y : r.y, z : r.z};
    //     return result;
    // }

    @:hlNative("std", "sys_coremotion_rotation")
    static public function sys_coremotion_rotation() : Dyn<{pitch:Float, yaw:Float, roll:Float}> { return null; }
    static public function getRotation():RotationAxis {
        var r:{pitch:Float, yaw:Float, roll:Float} = sys_coremotion_rotation();
        trace('roll: ' + r.roll);
        var result:RotationAxis = {pitch : r.pitch,
                    yaw : r.yaw,
                    roll : r.roll};
        return result;
    }
}
