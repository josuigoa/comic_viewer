package comicviewer.utils.parser;

import comicviewer.display.items.AudioItem;
import comicviewer.display.items.ImageItem;
import comicviewer.display.items.TextItem;
import comicviewer.display.items.ParticlesItem;
import comicviewer.display.items.SkyboxItem;
import comicviewer.display.items.map.ComicMap;
import comicviewer.display.items.map.Character;
import comicviewer.display.Page;
import comicviewer.display.settings.Settings;
import comicviewer.utils.Defaults;
import comicviewer.utils.Utils.*;
import comicviewer.utils.FontPool;
import comicviewer.components.*;
import comicviewer.components.effects.*;
import comicviewer.components.shaders.*;
import comicviewer.CV;
import libgoa.Itzul;
import h2d.Text;
import h3d.Vector;

using StringTools;

class ComicParser {
	static var jsondata:Types.JSONComicData;
	static var allThumbsTile:h2d.Tile;

	static public function fromJson(_jsondata:Types.JSONComicData):Types.ComicData {
		if (_jsondata == null)
			return null;

		try {
			jsondata = _jsondata;

			var defFontName = jsondata.general.textFontName;
			var mainDefaultFontName;
			if (defFontName == CV.GENERAL_VALUES.textFontName || defFontName == null) {
				mainDefaultFontName = CV.GENERAL_VALUES.textFontName;
			} else {
				mainDefaultFontName = defFontName;
			}

			var pageDefaultFontName;
			var pages = [];
			var tSkins = jsondata.textSkins;
			var gitems = jsondata.glossaryItems;

			var baseDir = '';
			var fs = Std.instance(hxd.Res.loader.fs, hxd.fs.LocalFileSystem);

			var localesDir = baseDir + CV.ASSETS_BASE_PATH + 'locales';
			for (poFile in hxd.Res.loader.dir(localesDir))
				Itzul.processPoContent(poFile.name.replace('.po', ''), poFile.toText());

			var allThumbsPath = Defaults.GET_PAGE_TEXTURE_PATH() + 'thumbs.';
			allThumbsTile = try {
				hxd.Res.load(allThumbsPath + 'jpg').toTile();
			} catch (e:hxd.res.NotFound) {
				try {
					hxd.Res.load(allThumbsPath + 'png').toTile();
				} catch (e:hxd.res.NotFound) {
					null;
				}
			}

			var settings = new Settings({name: "settings"}, (gitems != null && gitems.length > 0));
			for (l in Itzul.locales)
				settings.addLanguage(l, Itzul.getLocalizedForLocale('lang_name', l));

			var page:Page;
			var pageIndex = 0;

			if (tSkins != null) {
				var defaultTextSkin = CV.GENERAL_VALUES.textSkins.get(Defaults.TEXT_SKIN_NAME);
				for (s in tSkins) {
					CV.GENERAL_VALUES.textSkins.set(s.name, Defaults.fillMissingFieldsFrom(s, defaultTextSkin));
				}
			}

			if (gitems != null) {
				for (glossaryItem in gitems) {
					settings.addGlossaryItem({
						link: 'gl_$glossaryItem',
						title: 'gt_$glossaryItem',
						body: 'gb_$glossaryItem',
						image: 'gi_${glossaryItem}.png',
						detailImage: 'gi_${glossaryItem}.png',
						visible: false
					});
				}
			}

			var jsondataMap = jsondata.map;
			var map = null;
			if (jsondataMap != null) {
				pageDefaultFontName = switch jsondataMap.defaultFontName {
					case null: mainDefaultFontName;
					case fontName: fontName;
				}
				if (jsondataMap.items != null) {
					map = new ComicMap({name: "comicMap"});
					parsePageItems(map, Defaults.GET_MAP_TEXTURE_PATH(), -1, jsondataMap.items, pageDefaultFontName);
					map.setScale(CV.COMIC_SCALE);
				}
			}

			if (jsondata.pages != null && jsondata.pages.length != 0) {
				var thumbTile = null, pid;
				var tw = Std.int(CV.GENERAL_VALUES.pageWidth * .1);
				var th = Std.int(CV.GENERAL_VALUES.pageHeight * .1);
				var stx = 0, sty = 0;
				for (pageData in jsondata.pages) {
					if (allThumbsTile != null) {
						thumbTile = allThumbsTile.sub(stx, sty, tw, th);
						stx += tw;
						if (stx + tw > allThumbsTile.width) {
							stx = 0;
							sty += th;
						}
					} else {
						thumbTile = null;
					}
					page = new Page({
						pid: pageData.id,
						thumbTile: thumbTile,
						onEnterScript: pageData.onEnterScript,
						onLeaveScript: pageData.onLeaveScript
					});
					pageDefaultFontName = switch pageData.defaultFontName {
						case null: mainDefaultFontName;
						case fontName: fontName;
					}

					parsePageItems(page, Defaults.GET_PAGE_TEXTURE_PATH(), page.pid, pageData.items, pageDefaultFontName, map);

					page.setPageScale(CV.COMIC_SCALE);
					pageIndex++;
					pages.push(page);
				}
			} else {
				trace('"pages" element is null or empty.');
				page = new Page({pid: 0});
				page.addItem(new TextItem({parent: page, id: 'sampleText'}));
				pages.push(page);
			}

			return {
				pages: pages,
				defaultFontName: mainDefaultFontName,
				settings: settings,
				map: map
			};
		} catch (msg:String) {
			trace('ERROREA komikia parseatzean: $msg');
			trace('exceptionStack: ${haxe.CallStack.toString(haxe.CallStack.exceptionStack())}');
		}

		return null;
	} // fromJson

	static public function parsePageItems(page:Page, pageTexturesFolder:String, pageId:Int, items:Array<Types.ItemData>, pageDefaultFontName:String,
			map:ComicMap = null) {
		var mapchar:Character;
		var imgOptions:comicviewer.display.items.ImageItemOptions;
		var txtOptions:comicviewer.display.items.TextItemOptions;
		var partOptions:comicviewer.display.items.ParticlesItemOptions;
		var audioOptions:comicviewer.display.items.AudioItemOptions;
		var id,
			mapSS:Map<String, String>,
			pos:Types.CVector,
			size:Types.CVector,
			uv:Types.CVector,
			uniforms:Array<Types.ShaderUniformData>;

		for (item in items) {
			// trace('processing "${item.id}" element');
			if (item.type == null) {
				throw 'TYPE not found: $pageId orrian.';
			}

			if (item.pos == null)
				pos = new Types.CVector();
			else
				pos = cast(item.pos, Array<Dynamic>);
			switch (item.type) {
				case "image":
					if (item.id != null) {
						id = item.id;
					} else {
						throw '$pageId orriko irudi-fitxategi bat "null" da.';
					}
					imgOptions = {
						id: id,
						initPos: pos,
						parent: page,
						texturePath: pageTexturesFolder
					};

					if (item.size == null)
						size = CV.GENERAL_VALUES.imageItemSize;
					else
						size = cast(item.size, Array<Dynamic>);
					imgOptions.size = size;

					if (item.components != null) {
						var c = parseComponents(item.components);
						if (c != null) {
							imgOptions.components = c;
						}
					}

					imgOptions.layer = defNan(item.layer, CV.GENERAL_VALUES.imageLayer);
					imgOptions.centered = defNull(item.centered, CV.GENERAL_VALUES.imageCentered);
					if (item.uv == null)
						uv = new h3d.Vector(0, 0, 1);
					else
						uv = cast(item.uv, Array<Dynamic>);
					// imgOptions.size = uv;

					page.addItem(new ImageItem(imgOptions));

				case "text" | "mapCharacter" | "mapText" | "html":
					if (map == null && (item.type == "mapCharacter" || item.type == "mapText")) {
						continue;
					}

					var defaultTextSkin = CV.GENERAL_VALUES.textSkins.get(Defaults.TEXT_SKIN_NAME);

					var textSkin = switch item.skin {
						case null: defaultTextSkin;
						default:
							var ts = CV.GENERAL_VALUES.textSkins.get(item.skin);
							(ts != null) ? ts : defaultTextSkin;
					}

					txtOptions = {
						parent: page,
						id: item.id,
						initPos: pos,
						skin: item.skin
					};
					txtOptions.isUpperCase = defNull(item.isUpperCase, textSkin.uppercase);
					txtOptions.isBgEllipse = defNull(item.isBgEllipse, textSkin.isBgEllipse);
					txtOptions.bgPadding = defNull(item.bgPadding, textSkin.bgPadding);
					txtOptions.isHtml = item.type == 'html';

					if (item.fontName != null)
						txtOptions.fontName = item.fontName;
					else // TODO: Skin-eko fontName-a ere kudeatu behar da hemen
						txtOptions.fontName = pageDefaultFontName;

					if (item.fontSize != null)
						txtOptions.fontSize = item.fontSize;
					else
						txtOptions.fontSize = (item.type == "mapCharacter") ? CV.GENERAL_VALUES.mapCharacterFontSize : textSkin.fontSize;

					txtOptions.layer = defNan(item.layer, textSkin.layer);
					txtOptions.letterSpacing = (item.letterSpacing != null) ? item.letterSpacing : textSkin.letterSpacing;
					txtOptions.lineSpacing = (item.lineSpacing != null) ? item.lineSpacing : textSkin.lineSpacing;

					if (item.size == null)
						size = CV.GENERAL_VALUES.textItemSize;
					else
						size = cast(item.size, Array<Dynamic>);
					txtOptions.size = size;

					txtOptions.bgLayer = defNan(item.bgLayer, textSkin.backgroundLayer);
					txtOptions.hasBg = defNull(item.hasBg, textSkin.hasBg);
					txtOptions.bgColor = (item.bgColor != null) ? item.bgColor : textSkin.bgColor;
					txtOptions.color = (item.color != null) ? item.color : textSkin.color;

					txtOptions.glossaryLink = item.glossaryLink;

					if (item.components != null) {
						var c = parseComponents(item.components);
						if (c != null)
							txtOptions.components = c;
					}

					txtOptions.align = defNull(item.align, textSkin.align);
					txtOptions.alignVertical = defNull(item.alignVertical, (item.type == "mapCharacter") ? "top" : "center");

					if (item.type == "mapCharacter") {
						if (map == null) {
							trace('map element is null, couldn\'t add ${item.id}');
							continue;
						}
						txtOptions.parent = map;
						// if this item is previously in the map, add a new position
						mapchar = Std.instance(map.getItem(item.id), Character);
						// mapchar = Std.downcast(map.getItem(item.id), Character);
						if (mapchar == null) {
							mapchar = new Character(txtOptions);
							map.addItem(mapchar);
						}
						if (item.pathColor != null) {
							mapchar.pathColor = item.pathColor;
						}
						mapchar.addPosition(pageId, pos);
					} else {
						page.addItem(new TextItem(txtOptions));
					}

				case 'particles':
					partOptions = {
						id: item.id,
						initPos: pos,
						parent: page,
						particlesConfPaths: item.particlesConfPaths
					};

					partOptions.delayMs = defNan(item.delayMs, 0);

					page.addItem(new ParticlesItem(partOptions));
				case 'audio':
					audioOptions = {
						id: item.id
					};

					audioOptions.delayMs = defNan(item.delayMs, Std.int(CV.GENERAL_VALUES.pageFlippingTime * 1500));

					page.addItem(new AudioItem(audioOptions));
				case 'skybox':
					page.addItem(new SkyboxItem({id: 'skybox'}));
				default:
					trace('"${item.type}" motako elementua dago $pageId orrian, horrelakorik ez da prozesatzen.');
			} // switch (item.type)
		} // for(i in page.items)
	} // parsePageItems

	static public function parseComponents(components:Array<comicviewer.utils.parser.Types.ComponentData>) {
		var retComps = [];
		var compData:comicviewer.utils.parser.Types.ComponentData;
		var sineDt,
			timeDt,
			uniforms:Array<comicviewer.utils.parser.Types.ShaderUniformData>,
			uniformIterable:Array<comicviewer.utils.parser.Types.ShaderUniformData>;
		for (comp in components) {
			compData = {id: '${comp.id}', constructor: null};
			switch (comp.id) {
				case "Rotation":
					compData.constructor = RotationEffect.new;
					compData.options = {
						step: comp.options.step,
						min: comp.options.min,
						max: comp.options.max
					};
				case "Scale":
					compData.constructor = ScaleEffect.new;
					compData.options = {
						step: comp.options.step,
						min: comp.options.min,
						max: comp.options.max,
						axis: comp.options.axis
					};
				case "UVAccelerometer" | "UV":
					compData.constructor = (comp.id == 'UVAccelerometer') ? UVAccelerometer.new : UVEffect.new;
					compData.options = {
						initialDeltaU: comp.options.initialDeltaU,
						initialDeltaV: comp.options.initialDeltaV,
						deltaU: comp.options.deltaU,
						deltaV: comp.options.deltaV,
						isAbsoluteUV: comp.options.isAbsoluteUV,
						wrap: comp.options.wrap
					};
				case "Grass":
					compData.constructor = GrassEffect.new;
					compData.options = {
						opposite: comp.options.opposite,
						scale: comp.options.scale,
						speed: comp.options.speed,
					};
				case "Offset":
					compData.constructor = OffsetEffect.new;
					compData.options = {
						offsetX: comp.options.offsetX,
						offsetY: comp.options.offsetY,
						limits: comp.options.limits
					};
				case "Parallax":
					compData.constructor = ParallaxEffect.new;
					compData.options = {
						depthMapName: comp.options.depthMapName,
						scale: comp.options.scale,
					};
				case "AlphaByScale" | "AlphaByAccel":
					compData.constructor = (comp.id == 'AlphaByScale') ? AlphaByScaleEffect.new : AlphaByAccelEffect.new;
					compData.options = {
						minAlpha: comp.options.minAlpha,
						maxAlpha: comp.options.maxAlpha,
						minValue: comp.options.minValue,
						maxValue: comp.options.maxValue
					};
				case "Blur":
					compData.constructor = BlurEffect.new;
					compData.options = {
						radius: comp.options.radius,
						gain: comp.options.gain,
						quality: comp.options.quality,
						linear: comp.options.linear,
						delta: comp.options.delta
					};
				case "Displacement":
					compData.constructor = DisplacementEffect.new;
					compData.options = {
						normalMapName: comp.options.normalMapName,
						dispX: comp.options.dispX,
						dispY: comp.options.dispY
					};
				case "Sinus":
					compData.constructor = SinusEffect.new;
					compData.options = {};
				case "CloudNoise":
					compData.constructor = CloudNoiseEffect.new;
					compData.options = {
						dirX: comp.options.dirX,
						dirY: comp.options.dirY,
						cloudVel: comp.options.cloudVel,
						numClouds: comp.options.numClouds,
						minAlpha: comp.options.minAlpha,
						maxAlpha: comp.options.maxAlpha
					};
				case _:
					compData = null;
			}
			if (compData != null) {
				retComps.push(compData);
			}
		}

		return retComps;
	}
} // ComicParser
