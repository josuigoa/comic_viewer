package comicviewer.utils.parser;

import comicviewer.display.items.TextItem.AlignVertical;

/**
 * @author josu
 */
typedef ItemData = {
	var type:String;
	@:optional var pos:Array<Float>;
	@:optional var id:String;
	@:optional var layer:Null<Int>;
	@:optional var shader:String;
	@:optional var color:String;
	@:optional var components:Array<ComponentData>;
	@:optional var delayMs:Null<Int>;
	// TextItem
	@:optional var fontName:String;
	@:optional var overrideFont:Null<Bool>;
	@:optional var fontSize:Null<Float>;
	@:optional var size:Array<Float>;
	@:optional var glossaryLink:String;
	@:optional var align:String;
	@:optional var alignVertical:String;
	@:optional var letterSpacing:Null<Int>;
	@:optional var lineSpacing:Null<Int>;
	@:optional var hasBg:Null<Bool>;
	@:optional var bgLayer:Null<Int>;
	@:optional var bgPadding:Null<Float>;
	@:optional var isBgEllipse:Null<Bool>;
	@:optional var bgColor:String;
	@:optional var isUpperCase:Null<Bool>;
	@:optional var skin:String;
	// Character
	@:optional var pathColor:String;
	// ImageItem
	@:optional var centered:Null<Bool>;
	@:optional var uv:Array<Float>;
	// ParticleItem
	@:optional var particlesConfPaths:Array<String>;
}

// ItemData
typedef ComponentData = {
	var id:String;
	var constructor:Dynamic->comicviewer.components.Component;
	@:optional var options:Dynamic;
}

typedef MinMaxType = {
	var min:Float;
	@:optional var max:Null<Float>;
}

abstract UniformFloat(haxe.ds.Either<MinMaxType, Float>) {
	public inline function new(e)
		this = e;

	@:from static inline function fromMinMax(m:MinMaxType) {
		if (m.max != null)
			return new UniformFloat(Left(m));
		else
			return fromFloat(m.min);
	}

	@:from static inline function fromFloat(f:Float)
		return new UniformFloat(Right(f));

	@:to public inline function toFloat() {
		return switch this {
			case Left(m): hxd.Math.lerp(m.min, m.max, hxd.Math.random());
			case Right(f): f;
		}
	}

	@:to public inline function toString()
		Std.string(toFloat());
}

typedef ShaderUniformData = {
	var id:String;
	var type:String;
	@:optional var min:Null<Float>;
	@:optional var max:Null<Float>;
	@:optional var float:Null<Float>;
	@:optional var texName:String;
	@:optional var vector:CVector;
	@:optional var color:Array<Float>;
}

typedef PageData = {
	var id:Null<Int>;
	@:optional var defaultFontName:String;
	@:optional var items:Array<ItemData>;
	@:optional var onEnterScript:DelayedScript;
	@:optional var onLeaveScript:String;
}

typedef SettingsData = {
	var tabId:String;
}

typedef DelayedScript = {
	var script:String;
	var delayMs:Int;
}

typedef JSONComicData = {
	var general:ComicGeneralValues;
	var textSkins:Array<TextSkin>;
	var pages:Array<PageData>;
	@:optional var glossaryItems:Array<String>;
	@:optional var map:PageData;
}

typedef GlossaryItem = {
	var link:String; // gl_ prefix
	var title:String; // gt_ prefix
	var body:String; // gb_ prefix
	var image:String; // gi_ prefix
	var detailImage:String; // gdi_ prefix
	var visible:Bool;
}

typedef ComicData = {
	var pages:Array<comicviewer.display.Page>;
	var defaultFontName:String;
	var settings:comicviewer.display.settings.Settings;
	@:optional var map:comicviewer.display.items.map.ComicMap;
}

typedef ComicGeneralValues = {
	var comicId:String;
	var comicName:String;
	var pageWidth:Null<UInt>;
	var pageHeight:Null<UInt>;
	@:optional var uiFontName:String;
	@:optional var uiFontSize:Null<Float>;
	@:optional var zoomStep:Null<Float>;
	@:optional var minZoom:Null<Float>;
	@:optional var maxZoom:Null<Float>;
	@:optional var pageFlippingTime:Null<Float>;
	@:optional var pageLayer:Null<Int>;
	@:optional var itemPos:CVector;
	@:optional var textBackgroundLayer:Null<Int>;
	@:optional var textLayer:Null<Int>;
	@:optional var textFontName:String;
	@:optional var textFontSize:Null<Float>;
	@:optional var textItemSize:CVector;
	@:optional var textOverrideFont:Null<Bool>;
	@:optional var textColor:Color;
	@:optional var textHasBg:Null<Bool>;
	@:optional var textIsBgEllipse:Bool;
	@:optional var textBgPadding:Null<Float>;
	@:optional var textBgColor:Color;
	@:optional var textUppercase:Null<Bool>;
	@:optional var textLineSpacing:Null<Int>;
	@:optional var textLetterSpacing:Null<Int>;
	@:optional var textAlign:TextAlign;
	@:optional var imageItemSize:CVector;
	@:optional var imageLayer:Null<Int>;
	@:optional var imageCentered:Null<Bool>;
	@:optional var mapLayer:Null<Int>;
	@:optional var mapCharacterFontSize:Null<Float>;
	@:optional var mapCharacterPathColor:Color;
	@:optional var settingsLayer:Null<Int>;
	@:optional var glossaryLink:String;
	@:optional var glossaryTitlePointSize:Null<Float>;
	@:optional var glossaryBodyPointSize:Null<Float>;
	@:optional var textSkins:Map<String, TextSkin>;
}

typedef TextSkin = {
	var name:String;
	@:optional var backgroundLayer:Null<Int>;
	@:optional var layer:Null<Int>;
	@:optional var fontName:String;
	@:optional var fontSize:Null<Float>;
	@:optional var itemSize:CVector;
	@:optional var color:String;
	@:optional var hasBg:Null<Bool>;
	@:optional var isBgEllipse:Bool;
	@:optional var bgPadding:Null<Float>;
	@:optional var bgColor:String;
	@:optional var uppercase:Null<Bool>;
	@:optional var lineSpacing:Null<Int>;
	@:optional var letterSpacing:Null<Int>;
	@:optional var align:String;
}

typedef Range = {
	var min:Float;
	var max:Float;
}

typedef Limits = {
	var x:Range;
	var y:Range;
}

@:forward
abstract TextAlign(h2d.Text.Align) to h2d.Text.Align from h2d.Text.Align {
	@:from static inline public function fromString(a:String):TextAlign
		return switch a {
			case 'right': h2d.Text.Align.Right;
			case 'center': h2d.Text.Align.Center;
			case 'left': h2d.Text.Align.Left;
			default: CV.GENERAL_VALUES.textAlign;
		}

	@:to public inline function toString()
		return switch this {
			case h2d.Text.Align.Right: 'right';
			case h2d.Text.Align.Center: 'center';
			default: 'left';
		}
}

@:forward
abstract TextAlignVertical(AlignVertical) to AlignVertical from AlignVertical {
	@:from static inline public function fromString(a:String):TextAlignVertical
		return switch a {
			case 'top': AlignVertical.Top;
			case 'bottom': AlignVertical.Bottom;
			default: AlignVertical.Center;
		}

	@:to public inline function toString()
		return switch this {
			case AlignVertical.Top: 'top';
			case AlignVertical.Bottom: 'bottom';
			default: 'left';
		}
}

@:forward
abstract CVector(h3d.Vector) to h3d.Vector from h3d.Vector {
	inline public function new()
		this = new h3d.Vector();

	@:from static inline public function fromDynamicArray(a:Array<Dynamic>):CVector
		return fromAnyArray(cast a);

	@:from static inline public function fromAnyArray(a:Array<Any>):CVector
		return switch a {
			case null: return null;
			default:
				while (a.length < 3)
					a.push(0);
				new h3d.Vector(a[0], a[1], a[2]);
		}

	@:to public inline function toString():String
		return '[${this.x}, ${this.y}]';
}

@:forward
abstract Color(h3d.Vector4) to h3d.Vector4 from h3d.Vector4 {
	@:from static inline public function fromInt(i:Int):Color
		return h3d.Vector4.fromColor(i);

	@:from static inline public function fromString(s:String):Color
		return switch s {
			case null: null;
			default: h3d.Vector4.fromColor(Std.parseInt(s));
		}

	@:to public inline function toInt():Int
		return this.toColor();

	@:to public inline function toString():String
		return Std.string(toInt());
}
