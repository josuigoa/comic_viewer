package comicviewer.utils;

typedef AccelEvent = {
	timestamp:Float,
	value:Float,
	axis:Axis
}

enum abstract Axis(Int) from Int {
	var X = 0;
	var Y = 1;
	var Z = 2;
}

class Accelerometer {
	static public var x:Float = 0;
	static public var y:Float = 0;
	static public var z:Float = 0;
	static var s2d:h2d.Scene;

	static public function init(s2d:h2d.Scene) {
		#if !mobile
		s2d.addEventListener(onS2dEvent);
		Accelerometer.s2d = s2d;
		#end
	}
	
	#if mobile
	static function processSdlEvent(e:sdl.Event) {
		if (e.type == JoystickAxisMotion) {
			// (range: -32768 to 32767)
			var _val:Float = (e.value+32768)/(32767+32768);
			var _normalized_val = (-0.5 + _val) * 2.0;
			#if android
			_normalized_val *= .2;
			#end
			// var v = e.value / 0x7FFF;
			onAccelEvent({timestamp: 0,
						value: _normalized_val,
						axis: e.button});
			
			
			// facing left -> x = -1
			// facing front -> x = 0
			// facing right -> x = 1
			
			// face up -> y = 1
			// flat -> y = 0
			// upside down -> y = -1
		}
		return true;
	}
	#else
	static function onS2dEvent(e:hxd.Event) {
		switch e.kind {
			case EMove:
				onAccelEvent({timestamp: 0,
							value: (e.relX - s2d.width * .5) / (s2d.width * .5),
							axis: X});
				onAccelEvent({timestamp: 0,
							value: (e.relY - s2d.height * .5) / (s2d.height * .5),
							axis: Y});
				// x
				//  left: -1, center: 0, right: 1
				// y
				//  top: -1, center: 0, bottom: 1
			case _:
		}
	}
	#end
	
	static function onAccelEvent(e:AccelEvent) {
		// trace('accelEvent: $e');
		switch(e.axis) {
			case X: x = e.value;
			case Y: y = e.value; 
			case Z: z = e.value;
		}
	}
	static public function dispose() {
		#if !mobile
		s2d.removeEventListener(onS2dEvent);
		#end
	}
}
