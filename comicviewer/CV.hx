package comicviewer;

#if hlsdl
import sdl.Sdl;
#end
import comicviewer.display.settings.Settings;
import comicviewer.display.settings.General;
import comicviewer.display.Page;
import comicviewer.display.items.ComicItem;
import comicviewer.display.items.ComicItem.ItemType;
import comicviewer.display.effects.Foldable;
import comicviewer.display.items.map.ComicMap;
import comicviewer.utils.Defaults;
import comicviewer.utils.Accelerometer;
import comicviewer.utils.Signals;
import comicviewer.utils.Utils;
import libgoa.Itzul;
import libgoa.Agian;
import libgoa.Artean;
import h3d.Vector;
import h3d.Vector4;

using StringTools;

#if !comtor
import gessie.core.Gessie;
import gessie.gesture.Gesture;
import gessie.gesture.SwipeGesture;
import gessie.gesture.TapGesture;
import gessie.gesture.TransformGesture;
import gessie.impl.heaps.HeapsDisplayListAdapter;
import gessie.impl.heaps.HeapsInputAdapter;
import gessie.impl.heaps.HeapsTouchHitTester;
#end
#if mobile
import comicviewer.utils.Syspecial;
import comicviewer.utils.Syspecial.Key;
#end

/**
 * ...
 * @author josu
 */
typedef CVOptions = {
	var s2d:h2d.Scene;
	var ?s3d:h3d.scene.Scene;
	@:optional var size:Vector;
	@:optional var pos:Vector;
	@:optional var comicData:comicviewer.utils.parser.Types.JSONComicData;
}

#if comtor
@:publicFields
#end
class CV extends h2d.Layers {
	var textureMap:h3d.mat.Texture;
	var foldableMap:Foldable;
	#if !comtor
	var transformGest:TransformGesture<h2d.Scene>;
	#end
	var isPageFlipping:Bool;
	var pageIndex:Int;
	var threadPool:comicviewer.utils.ThreadPool;
	var map:ComicMap;
	var settings:Settings;
	var flipPageMinOffset:Float;
	var transformOffset:h3d.Vector;
	var initialZoomLocation:h3d.Vector;
	var previousTransformGestScale:Float;
	var transformIsScaling:Bool;
	var pageNumber:h2d.Text;

	public var pages(default, null):Array<Page>;
	public var previousPage(default, null):Agian<Page>;
	public var currentPage(default, null):Agian<Page>;
	public var nextPage(default, null):Agian<Page>;
	public var waitEvent:hxd.WaitEvent;

	static public var s2d:h2d.Scene;
	static public var s3d:h3d.scene.Scene;
	static public var ASSETS_BASE_PATH:String;
	static public var LANDSCAPE_ORIENTATION:Bool = false;
	static public var signals:Signals;

	var docPath:String = '';

	/* This is the RenderTextures containers position, the scene rendered to texture is in the (0, 0) */
	static public var PAGE_SPRITE_POS:Vector;
	static public var COMIC_SCALE:Float;
	static public var EFFECTS_ENABLED:Bool = true;
	static public var instance:CV;
	static public var IS_SCALED:Bool = false;
	static public var IS_MAP_SHOWN(default, null):Bool;
	static public var IS_SETTINGS_SHOWN:Bool;
	static public var NAV_BUTTONS:comicviewer.display.ui.NavButtons;
	static public var GENERAL_VALUES:comicviewer.utils.parser.Types.ComicGeneralValues;

	public function new(opt:CVOptions) {
		super(opt.s2d);

		s2d = opt.s2d;
		s3d = opt.s3d;

		#if !(mobile || comtor)
		s2d.addEventListener(onEvent);
		#end

		#if mobile
		#if android
		docPath = Syspecial.get(Key.androidInternal) + '/';
		#else
		docPath = Syspecial.get(Key.iosDocumentPath) + '/';
		#end
		#end

		var comicData = opt.comicData;

		if (ASSETS_BASE_PATH == '')
			ASSETS_BASE_PATH = Defaults.ASSETS_BASE_PATH;
		pageIndex = 0;
		isPageFlipping = IS_MAP_SHOWN = IS_SETTINGS_SHOWN = false;

		if (CV.GENERAL_VALUES == null && comicData != null)
			setGeneralValues(comicData.general);

		instance = this;
		waitEvent = new hxd.WaitEvent();
		threadPool = new comicviewer.utils.ThreadPool(2);

		var pSize = (opt != null && opt.size != null) ? opt.size.clone() : new Vector(hxd.Window.getInstance().width, hxd.Window.getInstance().height);
		var pageScaleX = pSize.x / GENERAL_VALUES.pageWidth;
		var pageScaleY = pSize.y / GENERAL_VALUES.pageHeight;
		CV.COMIC_SCALE = (pageScaleX < pageScaleY) ? pageScaleX : pageScaleY;
		PAGE_SPRITE_POS = (opt != null && opt.pos != null) ? opt.pos.clone() : new Vector((pSize.x - GENERAL_VALUES.pageWidth * CV.COMIC_SCALE) * .5,
			(pSize.y - GENERAL_VALUES.pageHeight * CV.COMIC_SCALE) * .5);
		GENERAL_VALUES.zoomStep *= COMIC_SCALE;
		GENERAL_VALUES.minZoom *= COMIC_SCALE;
		GENERAL_VALUES.maxZoom *= COMIC_SCALE;
		flipPageMinOffset = s2d.width * .25;

		signals = new Signals();
		signals.effectsEnabled.handle(b -> {
			CV.EFFECTS_ENABLED = b;
			saveUserValues();
		});

		var font = comicviewer.utils.FontPool.get(GENERAL_VALUES.uiFontName, Std.int(GENERAL_VALUES.uiFontSize * .5 * GENERAL_VALUES.pageWidth));
		pageNumber = new h2d.Text(font);
		pageNumber.textAlign = Center;
		pageNumber.color = new Vector4();
		pageNumber.x = PAGE_SPRITE_POS.x + pSize.x * .5;
		pageNumber.y = PAGE_SPRITE_POS.y + GENERAL_VALUES.pageHeight * .98 * COMIC_SCALE;
		add(pageNumber, Defaults.TEXT_LAYER + 1);

		#if mobile
		Sdl.setHint(SDL_HINT_ACCELEROMETER_AS_JOYSTICK, "1");
		#end

		Accelerometer.init(s2d);

		var index, locale, fx, showTutorial, tutorialTile = null;
		try {
			var userValues = loadUserValues();
			index = userValues.pageIndex;
			locale = userValues.locale;
			fx = userValues.effects;
			showTutorial = userValues.showTutorial;
		} catch (e:Dynamic) {
			trace('error loading user values: $e');
			index = 0;
			locale = 'eu_ES';
			fx = true;
			showTutorial = true;
		}

		if (showTutorial)
			tutorialTile = comicviewer.utils.Defaults.LOAD_TUTORIAL_IMAGE();

		EFFECTS_ENABLED = fx;

		loadParsedComic(comicviewer.utils.parser.ComicParser.fromJson(comicData), index, locale, tutorialTile);
	}

	public function loadParsedComic(pComic:comicviewer.utils.parser.Types.ComicData, index:Int, locale:String, tutorialTile:h2d.Tile) {
		if (pComic == null)
			return;

		pages = pComic.pages;
		settings = pComic.settings;
		if (pComic.map != null) {
			map = pComic.map;
			map.load();
			foldableMap = new Foldable(@:privateAccess map.targetTexture);
		}

		pageIndex = index;
		setLocale(locale);

		if (pages[pageIndex] == null) {
			pageIndex = 0;
		}

		function initGestures() {
			#if !comtor
			// GESTURES
			Gessie.init(s2d, new HeapsInputAdapter(s2d));
			Gessie.addDisplayListAdapter(h2d.Object, new HeapsDisplayListAdapter());
			Gessie.addTouchHitTester(new HeapsTouchHitTester(s2d));
			transformGest = new TransformGesture(s2d);
			transformGest.on(GEBegan, onTransformGesture);
			transformGest.on(GEChanged, onTransformGesture);
			transformGest.on(GEEnded, onTransformGesture);
			#end
		}

		var localeLocalDir = haxe.io.Path.join([haxe.io.Path.directory(Sys.programPath()), 'locales/']);
		if (sys.FileSystem.exists(localeLocalDir))
			for (l in sys.FileSystem.readDirectory(localeLocalDir))
				Itzul.processPoContent(l.replace('.po', ''), sys.io.File.getContent(haxe.io.Path.join([localeLocalDir, l])));

		loadNearPages();
		#if !comtor
		NAV_BUTTONS = new comicviewer.display.ui.NavButtons(s2d);
		#end
		gotoPage(pages[pageIndex].pid);

		#if comtor
		lastPageId = pages[pages.length - 1].pid + 1;
		#end

		if (tutorialTile != null) {
			tutorialTile.scaleToSize(CV.GENERAL_VALUES.pageWidth * CV.COMIC_SCALE, CV.GENERAL_VALUES.pageHeight * CV.COMIC_SCALE);
			var b = new h2d.Bitmap(tutorialTile, s2d);
			b.setPosition(PAGE_SPRITE_POS.x, PAGE_SPRITE_POS.y);
			var i = new h2d.Interactive(tutorialTile.width, tutorialTile.height, b);
			i.onClick = function(_) {
				b.alpha -= .2;
				if (b.alpha <= .4) {
					i.remove();
					b.remove();
					i = null;
					b = null;
					initGestures();
				}
			}
		} else {
			initGestures();
		}
	}

	/**
	 * @param	dir flipping direction: 1.Right, -1.Left
	 * @param	animate flipping animation or not
	 */
	public function flipPage(dir:Int, animate:Bool = false) {
		if (isPageFlipping || IS_MAP_SHOWN || IS_SETTINGS_SHOWN || IS_SCALED) {
			return;
		}

		isPageFlipping = true;

		if ((dir == 1 && pageIndex >= pages.length - 1) || (dir == -1 && pageIndex == 0)) {
			var initX = currentPage.x;
			Artean.mirror(GENERAL_VALUES.pageFlippingTime * .5, currentPage.x, currentPage.x - 10 * dir, function(currX) {
				isPageFlipping = initX != currX;
				currentPage.x = currX;
			});
			isPageFlipping = false;
			return;
		}

		gotoPage(pages[pageIndex + dir].pid, animate);
	}

	function getPageIndex(pid:Int) {
		for (i in 0...pages.length)
			if (pages[i].pid == pid)
				return i;

		return -1;
	}

	public function gotoPage(_newPageId:Int, animate:Bool = false) {
		trace('gotoPage($_newPageId)');
		var pidx = getPageIndex(_newPageId);

		if (pidx == -1) {
			isPageFlipping = false;
			return;
		}

		pageNumber.text = Std.string(_newPageId);
		var dir = (pidx > pageIndex) ? 1 : -1;
		var jmp = Math.abs(pidx - pageIndex);
		pageIndex = pidx;
		var oldCurrPage = currentPage;

		function onPrevLeaveComplete() {
			if (currentPage == null || jmp > 1) {
				currentPage = pages[pageIndex];
				currentPage.load();
				currentPage.start();
				currentPage.setPageScale(GENERAL_VALUES.minZoom);
				currentPage.setPosition(PAGE_SPRITE_POS.x, PAGE_SPRITE_POS.y);
				currentPage.enter(function() {
					isPageFlipping = false;
					if (jmp > 1) {
						previousPage.may((p) -> p.unload());
						oldCurrPage.may((p) -> {
							p.stop();
							p.unload();
						});
						nextPage.may((p) -> p.unload());
					}
					loadNearPages();
					#if comtor
					onPageChanged(_newPageId);
					#end
				});
				return;
			}

			var flipTime = (animate) ? GENERAL_VALUES.pageFlippingTime : 0.0001;
			var currentToX = (dir == 1) ? PAGE_SPRITE_POS.x - GENERAL_VALUES.pageWidth * CV.COMIC_SCALE : PAGE_SPRITE_POS.x
				+ GENERAL_VALUES.pageWidth * CV.COMIC_SCALE;

			var newCurrPage = (dir == 1) ? nextPage : previousPage;

			var newCurrPageX = PAGE_SPRITE_POS.x;
			Artean.simple(flipTime, newCurrPage.x, newCurrPageX, (currFloat:Float) -> {
				newCurrPage.x = currFloat;
				if (currFloat == newCurrPageX)
					newCurrPage.enter(function() {
						isPageFlipping = false;

						oldCurrPage.stop();
						currentPage = newCurrPage;
						if (dir == 1)
							previousPage.may((p) -> p.unload());
						else
							nextPage.may((p) -> p.unload());
						loadNearPages();
						#if comtor
						onPageChanged(_newPageId);
						#end
					});
			});

			Artean.simple(flipTime, oldCurrPage.x, currentToX, (currFloat:Float) -> {
				oldCurrPage.x = currFloat;
			});
		}

		if (oldCurrPage != null && !animate)
			oldCurrPage.leave(onPrevLeaveComplete);
		else
			onPrevLeaveComplete();

		for (pi in 0...pageIndex + 1) {
			settings.visibilizeGlossaryLinks(pages[pi].getGlossaryLinks());
		}

		saveUserValues();
	}

	static inline var loadPageAmount:Int = 1;

	function loadNearPages() {
		// TODO fix asynchronous loading
		// var task = function(_) {
		var previousToX = -GENERAL_VALUES.pageWidth * CV.COMIC_SCALE;
		previousPage = (pageIndex > 0) ? pages[pageIndex - 1] : null;
		previousPage.may((p) -> {
			p.load();
			p.setPosition(previousToX, PAGE_SPRITE_POS.y);
		});

		nextPage = (pageIndex < pages.length - 1) ? pages[pageIndex + 1] : null;
		var nextToX = hxd.Window.getInstance().width;
		nextPage.may((p) -> {
			p.load();
			p.setPosition(nextToX, PAGE_SPRITE_POS.y);
		});
		// 	return true;
		// }
		// threadPool.addTask(task, null, (_) -> {});
		// threadPool.blockRunAllTasks();
	}

	public function zoomIn(_zoomPos:Vector) {
		if (isPageFlipping || IS_SETTINGS_SHOWN)
			return;

		var scalingPage = (IS_MAP_SHOWN) ? map : currentPage;
		var s = scalingPage.scaleX;

		if (s >= GENERAL_VALUES.maxZoom) {
			scalingPage.setPageScale(GENERAL_VALUES.maxZoom);
			// Artean.mirror(scalingPage.CV.COMIC_SCALE, .5, { x: s + GENERAL_VALUES.zoomStep, y: s + GENERAL_VALUES.zoomStep } )
			// 				.reverse()
			// 				.onComplete(function() {
			// 					IS_SCALED = true;
			// 					scalingPage.CV.COMIC_SCALE.setXy(GENERAL_VALUES.maxZoom, GENERAL_VALUES.maxZoom);
			// 				});
			return;
		}

		s += GENERAL_VALUES.zoomStep;

		if (s > GENERAL_VALUES.maxZoom)
			s = GENERAL_VALUES.maxZoom;
		IS_SCALED = s != GENERAL_VALUES.minZoom;

		Utils.scalePageAround(scalingPage, _zoomPos.x, _zoomPos.y, s);
		clampPagePosition(scalingPage);
	}

	public function zoomOut(_zoomPos:Vector) {
		if (isPageFlipping || IS_SETTINGS_SHOWN)
			return;

		var scalingPage = (IS_MAP_SHOWN) ? map : currentPage;
		var s = scalingPage.scaleX;

		if (s <= GENERAL_VALUES.minZoom) {
			scalingPage.setPageScale(GENERAL_VALUES.minZoom);
			// Artean.mirror(scalingPage.CV.COMIC_SCALE, .5, { x: s - GENERAL_VALUES.zoomStep, y: s - GENERAL_VALUES.zoomStep } )
			// 				.reverse()
			// 				.onComplete(function() {
			// 					IS_SCALED = false;
			// 					scalingPage.CV.COMIC_SCALE.setXy(GENERAL_VALUES.minZoom, GENERAL_VALUES.minZoom);
			// 				});
			return;
		}

		s -= GENERAL_VALUES.zoomStep;

		if (s < GENERAL_VALUES.minZoom)
			s = GENERAL_VALUES.minZoom;
		IS_SCALED = s != GENERAL_VALUES.minZoom;

		Utils.scalePageAround(scalingPage, _zoomPos.x, _zoomPos.y, s);
		clampPagePosition(scalingPage);
	}

	public function moveScaled(mx:Float, my:Float) {
		if (!IS_SCALED)
			return;

		var scalingPage = (IS_MAP_SHOWN) ? map : currentPage;
		scalingPage.x += mx;
		scalingPage.y += my;

		clampPagePosition(scalingPage);
	}

	function clampPagePosition(_page:Page) {
		var s = _page.scaleX;
		var pspx = (IS_MAP_SHOWN) ? 0 : PAGE_SPRITE_POS.x;
		var pspy = (IS_MAP_SHOWN) ? 0 : PAGE_SPRITE_POS.y;

		_page.x = hxd.Math.clamp(_page.x, CV.COMIC_SCALE * GENERAL_VALUES.pageWidth - s * GENERAL_VALUES.pageWidth + pspx, pspx);
		_page.y = hxd.Math.clamp(_page.y, CV.COMIC_SCALE * GENERAL_VALUES.pageHeight - s * GENERAL_VALUES.pageHeight + pspy, pspy);
	}

	public function showMap() {
		if (map == null || IS_MAP_SHOWN || IS_SETTINGS_SHOWN || IS_SCALED)
			return;

		IS_MAP_SHOWN = true;
		map.start();
		map.drawCharactersAtPage(pageIndex);
		foldableMap.unfold();
	}

	public function hideMap() {
		if (map == null || !IS_MAP_SHOWN)
			return;

		foldableMap.fold(true, function(_) {
			map.stop();
			IS_MAP_SHOWN = false;
		}, true);
	}

	public function showSettings() {
		if (IS_SETTINGS_SHOWN || IS_MAP_SHOWN || IS_SCALED)
			return;

		settings.show();
	}

	public function hideSettings() {
		if (!IS_SETTINGS_SHOWN)
			return;

		settings.hide();
	}

	public function setLocale(newLang:String) {
		if (newLang == null || Itzul.currentLocale == newLang)
			return;

		Itzul.setLocale(newLang);
		previousPage.may(p -> previousPage.updateLocalization());
		currentPage.may(p -> currentPage.updateLocalization());
		nextPage.may(p -> nextPage.updateLocalization());
		if (settings != null) {
			settings.updateLocalization();
		}
		if (map != null) {
			map.updateLocalization();
		}

		saveUserValues();
	}

	public function update(dt:Float) {
		waitEvent.update(dt);
		Artean.update(dt);
		if (settings != null && IS_SETTINGS_SHOWN)
			settings.update(dt);
		#if !comtor
		Gessie.update();
		#end
		if (currentPage != null)
			currentPage.update(dt);
		if (map.started) {
			if (map != null)
				map.update(dt);
			if (foldableMap != null)
				foldableMap.update(dt);
		}
	}

	#if !comtor
	function onTransformGesture(states:{newState:gessie.core.GestureState, oldState:gessie.core.GestureState}) {
		var ts = transformGest.scale;
		var isVerticalGesture = hxd.Math.abs(transformGest.offsetY) > hxd.Math.abs(transformGest.offsetX);
		if (ts != 1 && transformGest.touchesCount > 1) {
			transformIsScaling = true;
			if (initialZoomLocation == null)
				initialZoomLocation = new h3d.Vector(transformGest.location.x, transformGest.location.y);
			if (ts > previousTransformGestScale)
				zoomIn(initialZoomLocation);
			else
				zoomOut(initialZoomLocation);
			previousTransformGestScale = ts;
		} else if (IS_SCALED) {
			moveScaled(transformGest.offsetX, transformGest.offsetY);
		} else {
			switch states.newState.toGestureEventType() {
				case GEBegan:
					transformOffset = new h3d.Vector();
				case GEChanged:
					if (isPageFlipping || IS_SETTINGS_SHOWN || IS_SCALED || transformIsScaling)
						return;

					transformOffset = transformOffset.sub(new h3d.Vector(transformGest.offsetX, transformGest.offsetY));
					if ((isVerticalGesture && currentPage.x == PAGE_SPRITE_POS.x && transformOffset.y < 0)
						|| foldableMap.foldingValue > 0) {
						if (IS_MAP_SHOWN)
							foldableMap.foldingValue = 1 - hxd.Math.abs(transformOffset.y / CV.GENERAL_VALUES.pageHeight);
						else {
							if (!map.started) {
								map.start();
								map.drawCharactersAtPage(pageIndex);
							}
							foldableMap.foldingValue = hxd.Math.abs(transformOffset.y / CV.GENERAL_VALUES.pageHeight);
						}
					} else {
						var currX = currentPage.x;
						currX += transformGest.offsetX;
						previousPage.may((p) -> {
							previousPage.x += transformGest.offsetX;
							if (transformGest.offsetX > flipPageMinOffset - 1)
								currX -= transformGest.offsetX;
						});
						nextPage.may((p) -> {
							nextPage.x += transformGest.offsetX;
							if (transformGest.offsetX < -flipPageMinOffset + 1)
								currX -= transformGest.offsetX;
						});
						currentPage.x = currX;
					}
				case GEEnded:
					if (isPageFlipping || transformIsScaling) {
						initialZoomLocation = null;
						transformIsScaling = false;
						return;
					}

					if (foldableMap.foldingValue > 0) {
						if (transformOffset.y > 0 && foldableMap.foldingValue < .75)
							hideMap();
						else if (transformOffset.y < 0 && foldableMap.foldingValue > .25)
							foldableMap.unfold((_) -> IS_MAP_SHOWN = true);
						else if (IS_MAP_SHOWN)
							foldableMap.unfold();
						else
							foldableMap.fold();
					} else if (hxd.Math.abs(transformOffset.x) > flipPageMinOffset) {
						flipPage((transformOffset.x > 0) ? 1 : -1, true);
					} else {
						Artean.simple(GENERAL_VALUES.pageFlippingTime, currentPage.x, PAGE_SPRITE_POS.x, (currFloat:Float) -> {
							currentPage.x = currFloat;
						});
						if (previousPage != null)
							Artean.simple(GENERAL_VALUES.pageFlippingTime, previousPage.x, PAGE_SPRITE_POS.x - GENERAL_VALUES.pageWidth * CV.COMIC_SCALE,
								(currFloat:Float) -> {
									previousPage.x = currFloat;
								});
						if (nextPage != null)
							Artean.simple(GENERAL_VALUES.pageFlippingTime, nextPage.x, PAGE_SPRITE_POS.x + GENERAL_VALUES.pageWidth * CV.COMIC_SCALE,
								(currFloat:Float) -> {
									nextPage.x = currFloat;
								});
					}
				default:
			}
		}
	}
	#end

	function saveUserValues() {
		var userVal = {
			pageIndex: pageIndex,
			locale: Itzul.currentLocale,
			effects: EFFECTS_ENABLED,
			showTutorial: false
		};
		hxd.Save.save(userVal, '${docPath}${GENERAL_VALUES.comicId}.userValues');
	}

	function loadUserValues():{
		pageIndex:Int,
		locale:String,
		effects:Bool,
		showTutorial:Bool
	} {
		return cast hxd.Save.load({
			pageIndex: 0,
			locale: 'eu_ES',
			effects: true,
			showTutorial: true
		}, '${docPath}${GENERAL_VALUES.comicId}.userValues');
	}

	#if comtor
	static var lastPageId:UInt = 0;

	static public function getLastPageId()
		return lastPageId++;

	public function addPage(pid:Int = -1) {
		var pid = (pid == -1) ? CV.getLastPageId() : pid;
		var page = new comicviewer.display.Page({pid: pid});
		page.setPageScale(CV.COMIC_SCALE);
		pages.push(page);

		pages.sort((p1:Page, p2:Page) -> p1.pid - p2.pid); // function(p1:Page, p2:Page) return p1.pid - p2.pid;

		gotoPage(page.pid);

		return page;
	}

	public function addItem(item:ComicItem) {
		item.start();
		currentPage.addItem(item);
	}

	public dynamic function onItemClicked(item:ComicItem) {}

	public dynamic function onPageChanged(pid:Int) {}

	public function getDataJson() {
		var json = '{';
		json += '"general":{';

		json += Defaults.getGeneralJson();

		json += '},';
		if (settings.glossary != null) {
			json += settings.glossary.getDataJson() + ',';
		}
		if (CV.GENERAL_VALUES.textSkins != null) {
			json += '"textSkins": [';
			for (name => skin in CV.GENERAL_VALUES.textSkins) {
				json += '{';
				json += '"name":"${skin.name}",';
				json += '"backgroundLayer":${skin.backgroundLayer},';
				json += '"layer":${skin.layer},';
				json += '"fontName":"${skin.fontName}",';
				json += '"fontSize":${skin.fontSize},';
				json += '"color":"${skin.color}",';
				json += '"hasBg":${skin.hasBg},';
				json += '"isBgEllipse":${skin.isBgEllipse},';
				json += '"bgPadding":${skin.bgPadding},';
				json += '"bgColor":"${skin.bgColor}",';
				json += '"uppercase":${skin.uppercase},';
				json += '"lineSpacing":${skin.lineSpacing},';
				json += '"letterSpacing":${skin.letterSpacing},';
				json += '"align":"${skin.align}"';
				json += '},';
			}
			json = json.substring(0, json.length - 1); // remove the last comma
			json += '],';
		}
		if (map != null) {
			json += '"map":' + map.getDataJson() + ',';
		}
		if (pages != null) {
			json += '"pages": [';
			for (p in pages) {
				json += p.getDataJson(map) + ",";
			}
			json = json.substring(0, json.length - 1); // remove the last comma
			json += ']';
		}

		json += '}';

		// trace('******************************');
		// trace('$json');
		// trace('******************************');

		return json;
	}

	public function getTranslations(locale:String) {
		var ret = '';
		inline function getLoc(key:String) {
			return ~/\s*\n\s*/g.replace(libgoa.Itzul.getLocalizedForLocale(key, locale), '\\n');
		}

		if (settings.glossary != null) {
			ret += '\n### glossary ###\n';
			for (i in settings.glossary.items) {
				ret += 'msgid "${i.title}"\n';
				ret += 'msgstr "${getLoc(i.title)}"\n\n';
				ret += 'msgid "${i.body}"\n';
				ret += 'msgstr "${getLoc(i.body)}"\n\n';
			}
		}
		if (map != null) {
			// json += '"map":' + map.getDataJson() + ',';
		}
		if (pages != null) {
			ret += '\n### dialogs ###\n';
			for (p in pages) {
				ret += '## ${p.pid} ##\n';
				if (p.items.length > 0) {
					for (i in p.items) {
						if (i.type.is(ItemType.TEXT)) {
							ret += 'msgid "${i.id}"\n';
							ret += 'msgstr "${getLoc(i.id)}"\n\n';
						}
					}
				}
			}
		}

		return ret;
	}
	#end

	#if !comtor
	function onEvent(e:hxd.Event) {
		if (e.kind == hxd.Event.EventKind.EKeyDown) {
			switch (e.keyCode) {
				case hxd.Key.ESCAPE:
					hxd.System.exit();
				case hxd.Key.RIGHT:
					flipPage(1);
				case hxd.Key.LEFT:
					flipPage(-1);
				case hxd.Key.M:
					if (IS_MAP_SHOWN)
						hideMap();
					else
						showMap();
				case hxd.Key.S:
					if (IS_SETTINGS_SHOWN)
						hideSettings();
					else
						showSettings();
				case hxd.Key.NUMPAD_ADD:
					zoomIn(new Vector(hxd.Window.getInstance().mouseX, hxd.Window.getInstance().mouseY));
				case hxd.Key.NUMPAD_SUB:
					zoomOut(new Vector(hxd.Window.getInstance().mouseX, hxd.Window.getInstance().mouseY));
				default:
			}
		} else if (e.kind == hxd.Event.EventKind.EWheel) {
			if (e.wheelDelta < 0)
				zoomIn(new Vector(hxd.Window.getInstance().mouseX, hxd.Window.getInstance().mouseY));
			else
				zoomOut(new Vector(hxd.Window.getInstance().mouseX, hxd.Window.getInstance().mouseY));
		}
	}
	#end

	static public function setGeneralValues(gv:comicviewer.utils.parser.Types.ComicGeneralValues) {
		CV.GENERAL_VALUES = Defaults.getDefaultGeneralValues();

		if (gv == null)
			return;

		if (gv.comicId != null)
			CV.GENERAL_VALUES.comicId = gv.comicId;
		if (gv.comicName != null)
			CV.GENERAL_VALUES.comicName = gv.comicName;
		if (gv.pageWidth != null)
			CV.GENERAL_VALUES.pageWidth = gv.pageWidth;
		if (gv.pageHeight != null)
			CV.GENERAL_VALUES.pageHeight = gv.pageHeight;
		if (gv.uiFontName != null)
			CV.GENERAL_VALUES.uiFontName = gv.uiFontName;
		if (!Math.isNaN(gv.uiFontSize) && gv.uiFontSize != null)
			CV.GENERAL_VALUES.uiFontSize = gv.uiFontSize;
		if (!Math.isNaN(gv.zoomStep) && gv.zoomStep != null)
			CV.GENERAL_VALUES.zoomStep = gv.zoomStep;
		if (!Math.isNaN(gv.minZoom) && gv.minZoom != null)
			CV.GENERAL_VALUES.minZoom = gv.minZoom;
		if (!Math.isNaN(gv.maxZoom) && gv.maxZoom != null)
			CV.GENERAL_VALUES.maxZoom = gv.maxZoom;
		if (!Math.isNaN(gv.pageFlippingTime) && gv.pageFlippingTime != null)
			CV.GENERAL_VALUES.pageFlippingTime = gv.pageFlippingTime;
		if (!Math.isNaN(gv.pageLayer) && gv.pageLayer != null)
			CV.GENERAL_VALUES.pageLayer = gv.pageLayer;
		if (gv.itemPos != null)
			CV.GENERAL_VALUES.itemPos = new Vector(gv.itemPos.x, gv.itemPos.y);
		if (!Math.isNaN(gv.textBackgroundLayer) && gv.textBackgroundLayer != null)
			CV.GENERAL_VALUES.textBackgroundLayer = gv.textBackgroundLayer;
		if (!Math.isNaN(gv.textLayer) && gv.textLayer != null)
			CV.GENERAL_VALUES.textLayer = gv.textLayer;
		if (gv.textFontName != null)
			CV.GENERAL_VALUES.textFontName = gv.textFontName;
		if (!Math.isNaN(gv.textFontSize) && gv.textFontSize != null)
			CV.GENERAL_VALUES.textFontSize = gv.textFontSize;
		if (gv.textItemSize != null)
			CV.GENERAL_VALUES.textItemSize = new Vector(gv.textItemSize.x, gv.textItemSize.y);
		if (gv.textOverrideFont != null)
			CV.GENERAL_VALUES.textOverrideFont = gv.textOverrideFont;
		if (gv.textColor != null)
			CV.GENERAL_VALUES.textColor = gv.textColor;
		if (gv.textHasBg != null)
			CV.GENERAL_VALUES.textHasBg = gv.textHasBg;
		if (gv.textIsBgEllipse != null)
			CV.GENERAL_VALUES.textIsBgEllipse = gv.textIsBgEllipse;
		if (!Math.isNaN(gv.textBgPadding) && gv.textBgPadding != null)
			CV.GENERAL_VALUES.textBgPadding = gv.textBgPadding;
		if (gv.textBgColor != null)
			CV.GENERAL_VALUES.textBgColor = gv.textBgColor;
		if (gv.textUppercase != null)
			CV.GENERAL_VALUES.textUppercase = gv.textUppercase;
		if (!Math.isNaN(gv.textLineSpacing) && gv.textLineSpacing != null)
			CV.GENERAL_VALUES.textLineSpacing = gv.textLineSpacing;
		if (!Math.isNaN(gv.textLetterSpacing) && gv.textLetterSpacing != null)
			CV.GENERAL_VALUES.textLetterSpacing = gv.textLetterSpacing;
		if (gv.textAlign != null)
			CV.GENERAL_VALUES.textAlign = gv.textAlign;
		if (gv.imageItemSize != null)
			CV.GENERAL_VALUES.imageItemSize = new Vector(gv.imageItemSize.x, gv.imageItemSize.y);
		if (!Math.isNaN(gv.imageLayer) && gv.imageLayer != null)
			CV.GENERAL_VALUES.imageLayer = gv.imageLayer;
		if (gv.imageCentered != null)
			CV.GENERAL_VALUES.imageCentered = gv.imageCentered;
		if (!Math.isNaN(gv.mapLayer) && gv.mapLayer != null)
			CV.GENERAL_VALUES.mapLayer = gv.mapLayer;
		if (!Math.isNaN(gv.mapCharacterFontSize) && gv.mapCharacterFontSize != null)
			CV.GENERAL_VALUES.mapCharacterFontSize = gv.mapCharacterFontSize;
		if (gv.mapCharacterPathColor != null)
			CV.GENERAL_VALUES.mapCharacterPathColor = gv.mapCharacterPathColor;
		if (!Math.isNaN(gv.settingsLayer) && gv.settingsLayer != null)
			CV.GENERAL_VALUES.settingsLayer = gv.settingsLayer;
		if (gv.glossaryLink != null)
			CV.GENERAL_VALUES.glossaryLink = gv.glossaryLink;
		if (!Math.isNaN(gv.glossaryTitlePointSize) && gv.glossaryTitlePointSize != null)
			CV.GENERAL_VALUES.glossaryTitlePointSize = gv.glossaryTitlePointSize;
		if (!Math.isNaN(gv.glossaryBodyPointSize) && gv.glossaryBodyPointSize != null)
			CV.GENERAL_VALUES.glossaryBodyPointSize = gv.glossaryBodyPointSize;

		var textSkinsDef = new Map<String, comicviewer.utils.parser.Types.TextSkin>();
		textSkinsDef.set(Defaults.TEXT_SKIN_NAME, {
			name: Defaults.TEXT_SKIN_NAME,
			backgroundLayer: CV.GENERAL_VALUES.textBackgroundLayer,
			layer: CV.GENERAL_VALUES.textLayer,
			fontName: CV.GENERAL_VALUES.textFontName,
			fontSize: CV.GENERAL_VALUES.textFontSize,
			itemSize: [CV.GENERAL_VALUES.textItemSize.x, CV.GENERAL_VALUES.textItemSize.y],
			color: CV.GENERAL_VALUES.textColor.toString(),
			hasBg: CV.GENERAL_VALUES.textHasBg,
			isBgEllipse: CV.GENERAL_VALUES.textIsBgEllipse,
			bgPadding: CV.GENERAL_VALUES.textBgPadding,
			bgColor: CV.GENERAL_VALUES.textBgColor.toString(),
			uppercase: CV.GENERAL_VALUES.textUppercase,
			lineSpacing: CV.GENERAL_VALUES.textLineSpacing,
			letterSpacing: CV.GENERAL_VALUES.textLetterSpacing,
			align: CV.GENERAL_VALUES.textAlign.toString()
		});
		CV.GENERAL_VALUES.textSkins = textSkinsDef;
	}
}
