package comicviewer.components.effects;

import comicviewer.CV;
import comicviewer.utils.parser.Types;
import comicviewer.utils.Accelerometer;
import comicviewer.utils.Utils.defNan;

/**
 * ...
 * @author josu
 */
typedef OffsetEffectOptions = {
	@:optional var offsetX:Null<Float>;
	@:optional var offsetY:Null<Float>;
	@:optional var limits:Limits;
}

#if comtor
@:publicFields
#end
class OffsetEffect extends Effect {
	var offsetX:Float;
	var offsetY:Float;
	var limits:Limits;

	public function new(opt:OffsetEffectOptions) {
		super();

		offsetX = defNan(opt.offsetX, 0);
		offsetY = defNan(opt.offsetY, 0);
		limits = opt.limits;
	}

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);
        
		if (item != null) {
			if (offsetX != 0 || offsetY != 0) {
				var mx = Accelerometer.x * CV.GENERAL_VALUES.pageWidth * .25;
				var my = Accelerometer.y * CV.GENERAL_VALUES.pageHeight * .25;

				item.x = item.initPos.x + offsetX * mx;
				item.y = item.initPos.y + offsetY * my;
				
				if (limits != null) {
					if (item.x < limits.x.min)
						item.x = limits.x.min;
					else if (item.x > limits.x.max)
						item.x = limits.x.max;
					
					if (item.y < limits.y.min)
						item.y = limits.y.min;
					else if (item.y > limits.y.max)
						item.y = limits.y.max;
				}
			}
		}
	}

	#if comtor
	override public function getDataJson() {
		var json = '{"id" : "${ID}", "options" : {"offsetX" : ${offsetX}, "offsetY" : ${offsetY} ';
		
		if (limits != null) {
			json += ', "limits" : [${limits.x.min}, ${limits.x.max}, ${limits.y.min}, ${limits.y.max}]';
		}
		
		json += '} }';
		return json;
	}
	#end
}
