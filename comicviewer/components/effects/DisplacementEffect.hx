package comicviewer.components.effects;

import comicviewer.utils.Utils.defNan;
import comicviewer.utils.Utils.defNull;
import comicviewer.utils.Accelerometer;

/**
 * ...
 * @author josu
 */
typedef DisplacementEffectOptions = {
	var normalMapName:String;
	@:optional var dispX:Null<Float>;
	@:optional var dispY:Null<Float>;
	@:optional var wrap:Null<Bool>;
}

#if comtor
@:publicFields
#end
class DisplacementEffect extends Effect {
	var disp:h2d.filter.Displacement;
	var normalMap:h2d.Tile;
	var normalMapName:String;
	var dispX:Float;
	var dispY:Float;
	var wrap:Bool;

	public function new(opt:DisplacementEffectOptions) {
		super();

		normalMapName = defNull(opt.normalMapName, '');
		var normapMapPath = comicviewer.utils.Defaults.GET_BASE_TEXTURE_PATH() + normalMapName;
		normalMap = try {
						hxd.Res.load(normapMapPath).toTile();
					} catch (e:hxd.res.NotFound) {
						trace('normalMap $normapMapPath not found');
						h2d.Tile.fromColor(0xff0000);
					}
		dispX = defNan(opt.dispX, 5.);
		dispY = defNan(opt.dispY, 5.);
		wrap = defNull(opt.wrap, true);
	}

	override public function start() {
		super.start();
		if (disp == null)
			disp = new h2d.filter.Displacement(normalMap, dispX, dispY, wrap);
		disp.dispX = dispX;
		disp.dispY = dispY;
		disp.wrap = wrap;
		if (active)
			item.filter = disp;
	}

	override function enableEffects(enable:Bool) {
		super.enableEffects(enable);
		item.filter = (enable) ? disp : null;
	}

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);
		
		if (item != null && disp != null) {
			normalMap.scrollDiscrete(1.2 * dt, 2.4 * dt);
		}
	} // update

	#if comtor
	override public function getDataJson() {
		return '{"id" : "${ID}", "options" : {"normalMapName" : "$normalMapName", "dispX" : ${dispX}, "dispY" : ${dispY}, "wrap" : ${wrap}}}';
	}
	#end
}
