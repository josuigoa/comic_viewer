package comicviewer.components.effects;

import comicviewer.utils.Accelerometer;
import comicviewer.utils.Utils.defNan;

/**
 * ...
 * @author josu
 */
typedef BlurEffectOptions = {
	@:optional var radius:Null<Float>;
	@:optional var gain:Null<Float>;
	@:optional var quality:Null<Float>;
	@:optional var linear:Null<Float>;
	@:optional var delta:Null<Float>;
}

#if comtor
@:publicFields
#end
class BlurEffect extends Effect {
	var blur:h2d.filter.Blur;
	var radius:Float;
	var gain:Float;
	var quality:Float;
	var linear:Float;
	var delta:Float;

	public function new(opt:BlurEffectOptions) {
		super();

		radius = defNan(opt.radius, 1);
		gain = defNan(opt.gain, 1);
		quality = defNan(opt.quality, 1);
		linear = defNan(opt.linear, 0);

		delta = defNan(opt.delta, 1);
	}

	override public function start() {
		super.start();
		if (blur == null)
			blur = new h2d.filter.Blur(radius, gain, quality, linear);
		
		if (active)
			item.filter = blur;
	}

	override function enableEffects(enable:Bool) {
		super.enableEffects(enable);
		item.filter = (enable) ? blur : null;
	}

	override public function stop() {
		super.stop();
		item.filter = null;
	}

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);
		
		if (item != null && blur != null) {
			var newRad = radius + Accelerometer.y * delta;
			blur.radius = (newRad < 0) ? 0 : newRad;
		}
	} // update

	#if comtor
	override public function getDataJson() {
		return '{"id" : "${ID}", "options" : {"radius" : ${radius}, "gain" : ${gain}, "linear" : ${linear}, "delta" : ${delta}}}';
	}
	#end
}
