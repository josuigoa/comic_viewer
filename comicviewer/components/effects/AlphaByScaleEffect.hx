package comicviewer.components.effects;

import comicviewer.components.effects.AlphaByXEffect;
import comicviewer.CV;

#if comtor
@:publicFields
#end
class AlphaByScaleEffect extends AlphaByXEffect {
	var parentScale:Float;

	public function new(opt:AlphaByXEffectOptions) {
		super(opt);
		minValue += CV.GENERAL_VALUES.minZoom;
		maxValue = CV.GENERAL_VALUES.minZoom + maxValue * (CV.GENERAL_VALUES.maxZoom - CV.GENERAL_VALUES.minZoom);
	}

	override function start() {
		super.start();

		currentValue = minValue;
		if (item != null && item.itemParent != null)
			parentScale = item.itemParent.scaleX;
		calculateAlpha();
	} // start

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);
		
		if (item != null && item.itemParent != null) {
			if (item.itemParent.scaleX != parentScale) {
				currentValue = item.itemParent.scaleX;
				parentScale = item.itemParent.scaleX;
				calculateAlpha();
			}
		}
	}

	#if comtor
	override public function getDataJson() {
		var ret:comicviewer.utils.parser.Types.ComponentData = haxe.Json.parse(super.getDataJson());
		ret.id = ID;
		ret.options.minValue -= CV.GENERAL_VALUES.minZoom;
		ret.options.maxValue = (maxValue - CV.GENERAL_VALUES.minZoom) / (CV.GENERAL_VALUES.maxZoom - CV.GENERAL_VALUES.minZoom);
		return haxe.Json.stringify(ret);
	}
	#end
}
