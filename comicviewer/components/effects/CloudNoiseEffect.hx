package comicviewer.components.effects;

import comicviewer.utils.Utils.defNan;

/**
 * ...
 * @author josu
 */
typedef CloudNoiseEffectOptions = {
	@:optional var dirX:Null<Float>;
	@:optional var dirY:Null<Float>;
	@:optional var cloudVel:Null<Float>;
	@:optional var numClouds:Null<Float>;
	@:optional var minAlpha:Null<Float>;
	@:optional var maxAlpha:Null<Float>;
}

#if comtor
@:publicFields
#end
class CloudNoiseEffect extends Effect {
	var dirX:Float;
	var dirY:Float;
	var cloudVel:Float;
	var numClouds:Float;
	var minAlpha:Float;
	var maxAlpha:Float;
	var noise:CloudNoise;

	public function new(opt:CloudNoiseEffectOptions) {
		super();

		dirX = defNan(opt.dirX, 0);
		dirY = defNan(opt.dirY, 0);
		cloudVel = defNan(opt.cloudVel, 50);
		numClouds = defNan(opt.numClouds, 5);
		minAlpha = defNan(opt.minAlpha, 0);
		maxAlpha = defNan(opt.maxAlpha, 1);

		noise = new CloudNoise(dirX, dirY, cloudVel, numClouds, minAlpha, maxAlpha);
	}

	override public function start() {
		super.start();
		noise.dirX = -1 + hxd.Math.random(2);
		noise.dirY = -1 + hxd.Math.random(2);
		noise.cloudVel = 10 + hxd.Math.random(80);
		noise.numClouds = 1 + hxd.Math.random(4);
		trace('${noise.dirX} / ${noise.dirY} / ${noise.cloudVel} / ${noise.numClouds}');
		item.addShader(noise);
	}

	override public function stop() {
		// item.removeShader(noise);
		super.stop();
	}

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);
		
		if (item != null && noise != null) {
			noise.dirX = noise.dirX + .08 % (Math.PI * 2);
			noise.dirY = noise.dirY + .08 % (Math.PI * 2);
			// noise.speed = speed + Accelerometer.y*deltaSpeed;
		}
	} // update

	#if comtor
	override public function getDataJson() {
		return '{"id" : "${ID}", "options" : {"dirX":${dirX},"dirY":${dirY},"cloudVel":${cloudVel},"numClouds":${numClouds},"minAlpha":${minAlpha},"maxAlpha":${maxAlpha}}}';
	}
	#end
}

// https://thebookofshaders.com/11/
private class CloudNoise extends hxsl.Shader {
	static var SRC = {
		@:import h3d.shader.Base2d;
		@param var dirX:Float;
		@param var dirY:Float;
		@param var cloudVel:Float;
		@param var numClouds:Float;
		@param var minAlpha:Float;
		@param var maxAlpha:Float;
		// 2D Random
		function random(st:Vec2):Float {
			return fract(sin(dot(st.xy, vec2(12.9898, 78.233))) * 43758.5453123);
		}
		// 2D Noise based on Morgan McGuire @morgan3d
		// https://www.shadertoy.com/view/4dS3Wd
		function noise(st:Vec2):Float {
			var i:Vec2 = floor(st);
			var f:Vec2 = fract(st);

			// Four corners in 2D of a tile
			var a:Float = random(i);
			var b:Float = random(i + vec2(1.0, 0.0));
			var c:Float = random(i + vec2(0.0, 1.0));
			var d:Float = random(i + vec2(1.0, 1.0));

			// Smooth Interpolation

			// Cubic Hermine Curve.  Same as SmoothStep()
			var u:Vec2 = f * f * (3.0 - 2.0 * f);
			// u = smoothstep(0.,1.,f);

			// Mix 4 corners percentages
			return mix(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
		}
		function fragment() {
			var offset = time / cloudVel;
			var st:Vec2 = vec2(random(vec2(21.6861, 86.5151)) + calculatedUV.x + offset * dirX, random(vec2(86.5151, 21.6861)) + calculatedUV.y +
					offset * dirY);

			// Scale the coordinate system to see
			// some noise in action
			var pos:Vec2 = vec2(st * numClouds);

			// Use the noise function
			var n:Float = noise(pos);

			output.color = vec4(vec3(minAlpha + n), minAlpha + n * maxAlpha);
		}
	}

	public function new(dirX:Float = 0., dirY:Float = 0., cloudVel:Float = 50, numClouds:Float = 5., minAlpha:Float = 0., maxAlpha:Float = 1.) {
		super();

		this.dirX = hxd.Math.clamp(dirX, -1, 1);
		this.dirY = hxd.Math.clamp(dirY, -1, 1);
		this.cloudVel = hxd.Math.clamp(cloudVel, 10, 90);
		this.numClouds = hxd.Math.clamp(numClouds, 1, 7);
		this.minAlpha = minAlpha;
		this.maxAlpha = maxAlpha;
	}
}
