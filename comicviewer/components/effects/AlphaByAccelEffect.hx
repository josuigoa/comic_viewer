package comicviewer.components.effects;

#if comtor
@:publicFields
#end
class AlphaByAccelEffect extends AlphaByXEffect {
	public function new(opt:AlphaByXEffect.AlphaByXEffectOptions) {
		super(opt);
	}

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);
		
		currentValue = comicviewer.utils.Accelerometer.y;
		calculateAlpha();
	}
}
