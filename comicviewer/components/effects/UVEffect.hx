package comicviewer.components.effects;

import comicviewer.utils.Utils.*;
using UVEffect.TileExtend;

/**
 * ...
 * @author josu
 */
typedef UVEffectOptions = {
	@:optional var initialDeltaU:Null<Float>;
	@:optional var initialDeltaV:Null<Float>;
	@:optional var deltaU:Null<Float>;
	@:optional var deltaV:Null<Float>;
	@:optional var isAbsoluteUV:Null<Bool>;
	@:optional var wrap:Null<Bool>;
}

#if comtor
@:publicFields
#end
class UVEffect extends Effect {
	var bitmap:h2d.Bitmap;
	var initialDeltaU:Float;
	var initialDeltaV:Float;
	var deltaU:Float;
	var deltaV:Float;
	var isAbsoluteUV:Bool;
	var wrap:Bool;
	var limitU:Float;
	var limitV:Float;
	var tile:h2d.Tile;

	public function new(opt:UVEffectOptions) {
		super();

		initialDeltaU = defNan(opt.initialDeltaU, 0);
		initialDeltaV = defNan(opt.initialDeltaV, 0);
		deltaU = defNan(opt.deltaU, 0);
		deltaV = defNan(opt.deltaV, 0);
		isAbsoluteUV = defNull(opt.isAbsoluteUV, false);
		wrap = defNull(opt.wrap, true);
	}

	override public function start() {
		super.start();
		
		bitmap = cast(item.drawable, h2d.Bitmap);
		if (bitmap == null)
			throw 'item must be of ImageItem type';

		tile = bitmap.tile;
		if (wrap && !tile.getTexture().flags.has(IsNPOT))
			bitmap.tileWrap = true;
		
		var _w = tile.width;
		if (deltaU != 0) {
			_w = Std.int(item.size.x * CV.GENERAL_VALUES.pageWidth / item.scaleX);
			limitU = _w / tile.width;
		}

		var _h = tile.height;
		if (deltaV != 0) {
			_h = Std.int(item.size.y * CV.GENERAL_VALUES.pageHeight / item.scaleY);
			limitV = _h / tile.height;
		}

		tile.setSize(_w, _h);
		tile.scrollDiscrete(initialDeltaU * tile.width, initialDeltaV * tile.height);
	} // start

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);

		setUv(deltaU * dt, deltaV * dt);
	} // update

	function setUv(u:Float, v:Float) {
		if (u == 0 && v == 0)
			return;
		
		// var tileU = @:privateAccess tile.u;
		// if (hxd.Math.abs(tileU - u) > .1) {
		// 	u = (u > tileU) ? tileU + .1 : tileU - .1;
		// }

		u = initialDeltaU + u;
		v = initialDeltaV + v;
		if (!wrap) @:privateAccess {
			
			if (u < 0 || u > limitU)
				u = (u < 0) ? 0 : limitU;
			if (v < 0 || v > limitV)
				v = (v < 0) ? 0 : limitV;
			
			u *= tile.width;
			v *= tile.height;
		} else {
			u *= tile.fullWidth();
			v *= tile.fullHeight();
		}
		
		if (isAbsoluteUV) 
			tile.scrollAbsolute(u, v);
		else
			tile.scrollDiscrete(u, v);
	}

	#if comtor
	override public function getDataJson() {
		return '{"id" : "${id}", "options" : {"initialDeltaU" : ${initialDeltaU}, "initialDeltaV" : ${initialDeltaV}, "deltaU" : ${deltaU}, "deltaV" : ${deltaV}, "isAbsoluteUV" : ${isAbsoluteUV}, "wrap" : ${wrap} }}';
	}
	#end
}

class TileExtend {
    
    static public function scrollAbsolute(t:h2d.Tile, dx : Float, dy : Float ) @:privateAccess {
        var tex = t.innerTex;
		t.u = dx / tex.width;
		t.v = dy / tex.height;
		t.u2 = t.u + (t.width / tex.width);
		t.v2 = t.v + (t.height / tex.height);
		t.x = t.u * tex.width;
		t.y = t.v * tex.height;
    }
    
    static public inline function fullWidth(t:h2d.Tile)
        return @:privateAccess t.innerTex.width;
    
    static public inline function fullHeight(t:h2d.Tile)
        return @:privateAccess t.innerTex.height;
}