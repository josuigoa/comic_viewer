package comicviewer.components.effects;

import comicviewer.utils.Utils.defNan;

/**
 * ...
 * @author josu
 */
typedef SinusEffectOptions = {
	@:optional var speed:Null<Float>;
	@:optional var deltaSpeed:Null<Float>;
	@:optional var frequency:Null<Float>;
	@:optional var amplitude:Null<Float>;
}

#if comtor
@:publicFields
#end
class SinusEffect extends Effect {
	var sinus:h3d.shader.SinusDeform;
	var frequency:Float;
	var amplitude:Float;
	var speed:Float;

	public function new(opt:SinusEffectOptions) {
		super();

		frequency = defNan(opt.frequency, 50.);
		amplitude = defNan(opt.amplitude, .01);
		speed = defNan(opt.speed, .5);
	}

	override public function start() {
		super.start();
		if (sinus == null)
			sinus = new h3d.shader.SinusDeform(frequency, amplitude, speed);
		sinus.frequency = frequency;
		sinus.amplitude = amplitude;
		sinus.speed = speed;
		item.addShader(sinus);
	}

	override public function stop() {
		// item.removeShader(sinus);
	}

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);
		if (item != null && sinus != null) {
			// sinus.speed = speed + Accelerometer.y*deltaSpeed;
		}
	} // update

	#if comtor
	override public function getDataJson() {
		return '{"id" : "${ID}", "options" : {"frequency" : ${frequency}, "amplitude" : ${amplitude}, "speed" : ${speed}}}';
	}
	#end
}
