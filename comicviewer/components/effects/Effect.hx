package comicviewer.components.effects;

import tink.CoreApi.CallbackLink;
import comicviewer.components.Component;

class Effect extends Component {

	var effectHandlerLink:CallbackLink;

	override function start() {
		super.start();

		active = CV.EFFECTS_ENABLED;
		effectHandlerLink = CV.signals.effectsEnabled.handle(b -> enableEffects(b));
	}

	function enableEffects(enable:Bool) {
		active = enable;
	}

	override function update(dt:Float) {
		if (!active) return;
		
        applyEffect(dt);
	}
	
	override function stop() {
		super.stop();
		effectHandlerLink.cancel();
	}

    function applyEffect(dt:Float) {}
}