package comicviewer.components.effects;

import comicviewer.utils.Utils.defNan;
import comicviewer.utils.Utils.defNull;
import comicviewer.utils.parser.Types;

/**
 * ...
 * @author josu
 */
typedef GrassEffectOptions = {
	@:optional var opposite:Array<Float>;
	@:optional var scale:Array<Float>;
	@:optional var speed:Null<Float>;
}

#if comtor
@:publicFields
#end
class GrassEffect extends Effect {
    
    var grassShader:GrassShader;
    var opposite:CVector;
    var scale:CVector;
    var speed:Float;
    var filter:h2d.filter.Shader<GrassShader>;

	public function new(opt:GrassEffectOptions) {
		super();
        
        opposite = opt.opposite;
        scale = opt.scale;
        speed = defNan(opt.speed, .08);
	}

	override public function start() {
        super.start();
        
        if (grassShader == null) {
            grassShader = new GrassShader();
            grassShader.opposite = opposite;
            grassShader.scale = scale;
            grassShader.count = 0;
            filter = new h2d.filter.Shader(grassShader);
        }
        
        if (active)
			item.filter = filter;
    }

	override function enableEffects(enable:Bool) {
		super.enableEffects(enable);
		item.filter = (enable) ? filter : null;
	}
    
    override public function stop() {
        super.stop();
        
        item.filter = null;
    }

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);
        
		if (item != null && grassShader != null) {
			grassShader.count = (grassShader.count + speed) % (hxd.Math.PI * 2);
		}
	} // update

	#if comtor
	override public function getDataJson() {
		return '{"id" : "${ID}", "options" : {"opposite" : $opposite, "scale" : $scale, "speed" : $speed}}';
	}
	#end
}

private class GrassShader extends h3d.shader.ScreenShader {
    
    static var SRC = {
        @param var texture : Sampler2D;
        
        @param var opposite : Vec2;
        @param var scale : Vec2;
        @param var count : Float;

		function fragment() {
            
            
            var offsetU = ((1 - input.uv.y) * opposite.y +
                            input.uv.y * (1 - opposite.y)) * scale.y;
            
            offsetU = offsetU.pow(2);
            offsetU = sin(count) * offsetU;
            offsetU *= .3;
            
            var offsetV = ((1 - input.uv.x) * opposite.x +
                            input.uv.x * (1 - opposite.x)) * scale.x;
            
            offsetV = offsetV.pow(2);
            offsetV = sin(count) * offsetV;
            offsetV *= .3;
            
            var newU = input.uv.x + offsetU;
            var newV = input.uv.y + offsetV;
            
            var newUV = input.uv + vec2(offsetU, offsetV);
            
            output.color = texture.get(newUV);
        }
    }
}