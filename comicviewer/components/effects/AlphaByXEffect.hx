package comicviewer.components.effects;

import comicviewer.utils.Utils.defNan;

typedef AlphaByXEffectOptions = {
	@:optional var minAlpha:Null<Float>;
	@:optional var maxAlpha:Null<Float>;
	@:optional var minValue:Null<Float>;
	@:optional var maxValue:Null<Float>;
}

#if comtor
@:publicFields
#end
class AlphaByXEffect extends Effect {
	var entityScale:Float;
	var minAlpha:Float;
	var maxAlpha:Float;
	var minValue:Float;
	var maxValue:Float;
	var currentValue:Float;

	public function new(opt:AlphaByXEffectOptions) {
		super();

		minAlpha = defNan(opt.minAlpha, 0);
		maxAlpha = defNan(opt.maxAlpha, 1);
		minValue = defNan(opt.minValue, 0);
		maxValue = defNan(opt.maxValue, 1);
		currentValue = minValue * .9;
	}

	function calculateAlpha() {
		if (currentValue < minValue && item.alpha != minAlpha) {
			item.alpha = minAlpha;
			return;
		}
		if (currentValue > maxValue && item.alpha != maxAlpha) {
			item.alpha = maxAlpha;
			return;
		}
		var percent = (currentValue - minValue) / (maxValue - minValue);
		item.alpha = minAlpha + percent * (maxAlpha - minAlpha);
	}

	#if comtor
	override public function getDataJson() {
		trace('minAlpha: $minAlpha');
		return '{"id" : "${ID}", "options" : {"minAlpha" : ${minAlpha}, "maxAlpha" : ${maxAlpha}, "minValue" : ${minValue}, "maxValue" : ${maxValue}} } ';
	}
	#end
}
