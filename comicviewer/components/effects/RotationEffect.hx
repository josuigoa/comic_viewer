package comicviewer.components.effects;

/**
 * ...
 * @author josu
 */
typedef RotationEffectOptions = {
	var name:String;
	var step:Float;
	@:optional var id:String;
	@:optional var min:Null<Float>;
	@:optional var max:Null<Float>;
}

#if comtor
@:publicFields
#end
class RotationEffect extends Effect {
	var step:Null<Float>;
	var min:Null<Float>;
	var max:Null<Float>;

	public function new(opt:RotationEffectOptions) {
		super();

		step = opt.step;
		// step = Maths.radians(opt.step);
		min = opt.min;
		// min = Maths.radians(min);
		max = opt.max;
		// max = Maths.radians(max);
	}

	override function start() {
		super.start();
		
		item.rotation = min;
	} // init

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);
        
		if (item != null) {
			item.rotation += step * dt;
			if (step != null && min != null && max != null && (item.rotation <= min || item.rotation >= max)) {
				step *= -1;
			}
		}
	} // update

	#if comtor
	override public function getDataJson() {
		return '{"id" : "${ID}", "options" : {"step" : ${step}, "min" : ${min}, "max" : ${max}}}';
	}
	#end
}
