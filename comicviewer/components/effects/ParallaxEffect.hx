package comicviewer.components.effects;

import comicviewer.CV;
import comicviewer.utils.Accelerometer;
import comicviewer.utils.Utils.defNull;
import comicviewer.utils.parser.Types;

/**
 * ...
 * @author josu
 */
typedef ParallaxEffectOptions = {
	@:optional var depthMapName:Null<String>;
	@:optional var scale:Array<Float>;
}

#if comtor
@:publicFields
#end
class ParallaxEffect extends Effect {
	var depthMapName:String;
	var depthMapShader:DepthMap;
	var scale:CVector;
	var filter:h2d.filter.Shader<DepthMap>;

	public function new(opt:ParallaxEffectOptions) {
		super();

		depthMapName = defNull(opt.depthMapName, '');
		scale = opt.scale;
	}

	override public function start() {
		super.start();

		if (depthMapShader == null) {
			depthMapShader = new DepthMap();
			var depthMapPath = comicviewer.utils.Defaults.GET_BASE_TEXTURE_PATH() + depthMapName;
			depthMapShader.depthTex = try {
				hxd.Res.load(depthMapPath).toTexture();
			} catch (e:hxd.res.NotFound) {
				trace('normalMap $depthMapPath not found');
				h3d.mat.Texture.fromColor(0xaaaaaa);
			}
			depthMapShader.scale = new h3d.Vector(1);
			filter = new h2d.filter.Shader(depthMapShader);
		}

		if (active)
			item.filter = filter;
	}

	override function enableEffects(enable:Bool) {
		super.enableEffects(enable);
		item.filter = (enable) ? filter : null;
	}

	override public function stop() {
		super.stop();

		item.filter = null;
	}

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);

		if (item != null && depthMapShader != null) {
			#if mobile
			var mx = Accelerometer.x * scale.x;
			var accelY = ((Accelerometer.y + .2) * .5) * 5;
			accelY = hxd.Math.clamp(accelY);
			accelY = -.2 + accelY * .3;
			var my = accelY * scale.y;
			#else
			var mx = Accelerometer.x / (item.width * scale.x);
			var my = Accelerometer.y / (item.height * scale.y);
			#end

			depthMapShader.scale.set(mx, my);
		}
	} // update

	#if comtor
	override public function getDataJson() {
		return '{"id" : "${ID}", "options" : {"depthMapName" : "$depthMapName", "scale": $scale}}';
	}
	#end
}

private class DepthMap extends h3d.shader.ScreenShader {
	static var SRC = {
		@param var texture:Sampler2D;
		@param var depthTex:Sampler2D;
		@param var scale:Vec2;
		function fragment() {
			var tcol = depthTex.get(input.uv);
			var height = tcol.r;
			var displacement = scale * ((height - .5) * 2) * step(0.00001, tcol.a);
			var clamped = clamp(input.uv - displacement, vec2(0, 0), vec2(1, 1));
			output.color = texture.get(clamped);
		}
	}
}
