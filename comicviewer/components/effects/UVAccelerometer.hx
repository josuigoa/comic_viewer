package comicviewer.components.effects;

import comicviewer.utils.Accelerometer;

/**
 * ...
 * @author josu
 */
#if comtor
@:publicFields
#end
class UVAccelerometer extends UVEffect {
	public function new(opt:UVEffect.UVEffectOptions) {
		super(opt);
	}

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);
        
		var accelX = Std.int(Accelerometer.x * 100) / 100;
		trace('accel.x : ${accelX} / ${Accelerometer.x}');
		setUv(initialDeltaU + accelX * deltaU * 10,
			initialDeltaV + Accelerometer.y * deltaV * dt);
	} // update
}
