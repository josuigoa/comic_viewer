package comicviewer.components.effects;

import comicviewer.utils.Utils.defNull;

/**
 * ...
 * @author josu
 */
@:enum
abstract Axis(String) {
	var x = 'x';
	var y = 'y';
}

typedef ScaleEffectOptions = {
	var name:String;
	var step:Float;
	@:optional var id:String;
	@:optional var min:Null<Float>;
	@:optional var max:Null<Float>;
	@:optional var axis:Null<Axis>;
}

#if comtor
@:publicFields
#end
class ScaleEffect extends Effect {
	var step:Null<Float>;
	var min:Null<Float>;
	var max:Null<Float>;
	var axis:Axis;

	public function new(opt:ScaleEffectOptions) {
		super();

		step = opt.step;
		min = opt.min;
		max = opt.max;
		axis = defNull(opt.axis, x);
	}

	override function start() {
		super.start();
		
		if (axis == x)
			item.scaleX = min;
		else
			item.scaleY = min;
	} // init

	override public function applyEffect(dt:Float) {
		super.applyEffect(dt);
        
		if (item != null) {
			var s = (axis == x) ? item.scaleX : item.scaleY;
			s += step * dt;
			if (step != null && min != null && max != null && (s <= min || s >= max))
				step *= -1;
			if (axis == x)
				item.scaleX = s;
			else
				item.scaleY = s;
		}
	} // update

	#if comtor
	override public function getDataJson() {
		return '{"id" : "${ID}", "options" : {"step" : ${step}, "min" : ${min}, "max" : ${max}, "axis" : "$axis"}}';
	}
	#end
}
