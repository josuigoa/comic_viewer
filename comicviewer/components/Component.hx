package comicviewer.components;

import comicviewer.display.items.ComicItem;

@:autoBuild(comicviewer.utils.Macros.buildComponent())
class Component {
	public var item(default, set):ComicItem;
	public var id:String;
	public var active(default, set):Bool;

	public function new() {}

	public function start() {
		if (item == null)
			throw 'item must be non null';
		
		active = true;
	}

	public function stop() {
		active = false;
	}

	public function update(dt:Float) {}

	public function set_item(it:ComicItem) {
		return item = it;
	}

	public function set_active(a:Bool) {
		return active = a;
	}

	#if comtor
	public function getDataJson():String
		return '';
	#end
}
