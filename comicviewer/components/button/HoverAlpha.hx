package comicviewer.components.button;

#if comtor
@:publicFields
#end
class HoverAlpha extends InteractiveComponent {
	public var amount:Float = 0.3;
	public var startAlpha:Float = 1;
	public var speed:Float = 0.1;

	public function new() {
		super();
	}

	override public function start() {
		super.start();
		interactive.onOver = hover;
		interactive.onOut = unhover;
		unhover(null);
	}

	override public function stop() {
		if (interactive != null)
			interactive.remove();
		unhover(null);
	}

	public function hover(_) {
		item.alpha = startAlpha - amount;
	} // hover

	public function unhover(_) {
		item.alpha = startAlpha;
	} // unhover

	override public function set_active(b:Bool) {
		if (interactive != null) {
			if (b) {
				interactive.onOver = hover;
				interactive.onOut = unhover;
			} else {
				interactive.onOver = interactive.onOut = null;
			}
		}

		return active = b;
	}

	override public function set_item(it:comicviewer.display.items.ComicItem) {
		if (it == null)
			unhover(null);
		return super.set_item(it);
	}
} // HoverAlpha
