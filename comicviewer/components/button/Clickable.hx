package comicviewer.components.button;

class Clickable extends HoverAlpha {
	public var onClick:hxd.Event->Void;

	static public inline var clicked:String = 'clickable.clicked';

	public function new(?onClick:hxd.Event->Void) {
		super();
		this.onClick = onClick;
	} // new

	override public function start() {
		super.start();
		interactive.onClick = _onClick;
	} // start

	function _onClick(e:hxd.Event) {
		if (onClick != null)
			onClick(e);
	} // onClick

	override public function set_active(b:Bool) {
		if (interactive != null)
			interactive.onClick = (b) ? onClick : null;
		return super.set_active(b);
	}
} // Clickable
