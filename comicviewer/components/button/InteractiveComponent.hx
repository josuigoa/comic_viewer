package comicviewer.components.button;

#if comtor
@:publicFields
#end
class InteractiveComponent extends comicviewer.components.Component {

	var interactive:h2d.Interactive;

	public function new() {
		super();
		active = false;
	}

	override public function start() {
		super.start();
		var w = item.width;
		var h = item.height;
		interactive = new h2d.Interactive(w / item.scaleX, h / item.scaleY, item);
	}

	override public function stop() {
		super.stop();
		if (interactive != null)
			interactive.remove();
	}

	override public function set_active(a:Bool) {
		super.set_active(a);
		if (interactive != null) {
			if (!a)
				interactive.onOver = interactive.onOut = null;
		}

		return a;
	}
} // InteractiveComponent
