package comicviewer.components.button;

import h3d.Vector4;
import comicviewer.utils.Utils.defNull;

class Radio extends Clickable {
	public var isSelected(default, set):Bool;
	public var selectedColor:Vector4;
	public var unselectedColor:Vector4;

	var group:String;

	static var groups:Map<String, Array<Radio>>;
	static public inline var selected:String = 'radio.selected';

	public function new(_groupName:String = 'default', _unselCol:Vector4 = null, _selCol:Vector4 = null) {
		super();

		unselectedColor = defNull(_unselCol, new Vector4(1, 1, 1, .6));
		selectedColor = defNull(_selCol, new Vector4(1, 1, 1, 1));
		defNull(groups, new Map<String, Array<Radio>>());

		if (!groups.exists(_groupName)) {
			groups.set(_groupName, []);
		}

		groups.get(_groupName).push(this);
		group = _groupName;
	} // new

	override public function start() {
		super.start();
		isSelected = false;
		unhover(null);
	} // start

	override function unhover(_) {
		if (!isSelected)
			super.unhover(_);
	}

	override function _onClick(_) {
		if (isSelected)
			return;

		var arr = groups.get(group);
		for (r in arr) {
			if (r != this) {
				r.isSelected = false;
			}
		}

		isSelected = true;

		super._onClick(_);
	} // onClick

	public dynamic function onSelectionChanged(isSelected:Bool) {}

	// GETTER & SETTER
	function set_isSelected(_s:Bool):Bool {
		var col = (_s) ? selectedColor : unselectedColor;
		if (item != null && item.color != null) {
			item.color.set(col.r, col.g, col.b, 1);
			item.alpha = col.a;
		}
		item.alpha = col.a;
		onSelectionChanged(_s);
		return isSelected = _s;
	}
} // Toggle
