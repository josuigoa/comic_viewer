# COMIC VIEWER

## EFEKTUAK

* Blur
* UV
* Hair / Grass
* Water movement
* Parallax
* Offset
* On page enter / leave
* Accelerometer
* Set alpha (by zoom or by accel)
* Particles
* Clouds?
* Rotation

## KARPETA EGITURA

* komikia
  * comicData.json
  * fonts
    * font_izena
      * font_izena.fnt
      * font_izena.png
  * hscripts
  * locales
    * locale_izena.po
  * shaders
  * textures
    * glossary
      * glossary_img.png
      * ...
    * map
      * map_shore.png
      * map_seas.jpg
      * ...
    * pages
      * page0_bg.jpg
      * particleTex.png
      * page1_bg.eu.jpg
      * page1_bg.en.jpg
      * page1_arrow.png
      * ...
