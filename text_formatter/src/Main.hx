using StringTools;
import libgoa.AE;

class Main {
    static function main() {
        var lines = AE.lerro_saltoa.split(sys.io.File.getContent('testuak.txt'));

        var hiz = '', edukia = '';
        var map:Map<String, Array<String>> = new Map();
        for (l in lines) {
            if (l.startsWith('----')) {
                if (edukia != '') {
                    map.get(hiz).push(edukia);
                    edukia = '';
                }
                hiz = if (l.indexOf('eus') != -1) 'eu_ES';
                        else if (l.indexOf('eng') != -1) 'en_GB';
                        else if (l.indexOf('esp') != -1) 'es_ES';
                        else 'ezezaguna';
                if (!map.exists(hiz)) map.set(hiz, []);
            } else {
                edukia += l.replace('"', '\\"');
            }
        }

        var id, page_num = 1, str, arr, eu_arr = map.get('eu_ES'), eu_id, cont, check_map;
        for (h in map.keys()) {
            cont = '';
            check_map = new Map();
            arr = map.get(h);
            for (s_ind in 0...arr.length) {
                str = arr[s_ind];
                eu_id = eu_arr[s_ind];
                id = eu_id.substr(0, 20);
                if (check_map.exists(id)) {
                    if (check_map.get(id) == str) continue
                    else id += eu_id.substr(eu_id.length-5, 5);
                }
                cont += 'msgid "$id"\n';
                cont += 'msgstr "$str"\n\n';
                check_map.set(id, str);
            }
            sys.io.File.saveContent('$h.po', cont);
        }
    }
}
